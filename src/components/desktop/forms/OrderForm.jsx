import React, {useMemo} from 'react';
import uuid from "uuid";
import {measuresList} from "root/static";

// Components
import Form from "core/desktop/view/form/Form";
import FormGroup from "core/desktop/view/form/FormGroup";
import Field from "core/desktop/view/form/Field";
import TwoColumnField from "core/desktop/view/form/TwoColumnField";
import SimpleSelect from "core/desktop/view/form/fields/select/SimpleSelect";
import DealSelect from "components/desktop/ui/DealSelect";
import Input from "core/desktop/view/form/fields/Input";
import TextArea from "core/desktop/view/form/fields/TextArea";
import FileLoader from "core/desktop/view/form/fields/file/FileLoader";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Button from "core/desktop/view/Button";
import FormHelperText from "@material-ui/core/FormHelperText";
import Icon from "core/desktop/view/Icon";
import Bitrix24 from "classes/Bitrix24";

// Actions
import {setPage} from "actions/app";

function OrderForm(props) {
    const {id, onSubmit, onSuccess, onError, initialValues = {}, type} = props;
    const [placement] = Bitrix24.getPlacementInfo();

    const submitTitle = type === 'create' ? 'СОздать заявку' : 'Сохранить заявку';

    return (
        <Form
            id={id}
            settings={{
                labelWidth: 175,
                fieldGrid: 3,
                fieldWidth: 250
            }}
        >
            {(form, settings, isSubmitting) => {
                const {labelWidth, fieldWidth} = settings;
                return (
                    <>
                        <Grid container spacing={2}>
                            {placement !== "CRM_DEAL_DETAIL_TAB" && (
                                <TwoColumnField
                                    name={'object'}
                                    isRequired={true}
                                    defaultValue={initialValues.dealId || ''}
                                    label={'Объект'}
                                    labelWidth={labelWidth}
                                    fieldWidth={fieldWidth}
                                >
                                    {(inputProps, fieldProps) =>
                                        <DealSelect
                                            {...inputProps}
                                            {...fieldProps}
                                            settings={{search: true}}
                                        />
                                    }
                                </TwoColumnField>
                            )}
                            <TwoColumnField
                                name={'desc'}
                                isRequired={true}
                                defaultValue={initialValues.desc || ''}
                                label={'Описание заявки'}
                                labelWidth={labelWidth}
                                fieldWidth={fieldWidth}
                            >
                                {inputProps => <TextArea {...inputProps}/>}
                            </TwoColumnField>
                            <TwoColumnField
                                name={'attachments'}
                                defaultValue={initialValues.attachments || []}
                                label={'Файл-приложение к заявке'}
                                labelWidth={labelWidth}
                                fieldWidth={fieldWidth}
                            >
                                {(inputProps, fieldProps) =>
                                    <FileLoader
                                        {...inputProps}
                                        {...fieldProps}
                                        settings={{
                                            multiple: true
                                        }}
                                        disabled={isSubmitting}
                                    />
                                }
                            </TwoColumnField>
                            <TwoColumnField
                                name={'comment'}
                                defaultValue={initialValues.comment || []}
                                label={'Комментарий'}
                                labelWidth={labelWidth}
                                fieldWidth={fieldWidth}
                            >
                                {inputProps => <TextArea {...inputProps}/>}
                            </TwoColumnField>
                        </Grid>
                        <Box mt={4}>
                            <FormGroup title={'Товары по заявке'} isRequired={true}>
                                <Field
                                    name={'products'}
                                    isRequired={true}
                                    defaultValue={initialValues.products || []}
                                >
                                    {(inputProps, {onMetaChange}) => {
                                        const {value: products = [], onChange, error, meta} = inputProps;
                                        const {deleted = []} = meta;

                                        return (
                                            <>
                                                {error && products.length === 0 && <FormHelperText error={!!error}>{error}</FormHelperText>}
                                                <div className={'products-container'} style={{width: 585}}>
                                                    {products.map( (p, i) => {
                                                        return (
                                                            <Product
                                                                key={p.ID}
                                                                {...p}
                                                                length={products.length}
                                                                onChange={(key, value) => {
                                                                    const products = form.getField('products').value;
                                                                    const updatedItem = Object.assign({}, p, {[key]: value});
                                                                    const updatedProducts = products.slice(0, i).concat(updatedItem).concat(products.slice(i + 1));

                                                                    onChange(updatedProducts);
                                                                }}
                                                                onDelete={() => {
                                                                    const products = form.getField('products').value;
                                                                    onChange(products.del(i));

                                                                    if (!p.IS_NEW) {
                                                                        onMetaChange({deleted: deleted.concat(p.ID)});
                                                                    }
                                                                }}
                                                            />
                                                        )
                                                    })}
                                                    <Box mt={2}>
                                                        <Button
                                                            color={'secondary'}
                                                            onClick={() => {
                                                                onChange(products.concat({
                                                                    ID: uuid(),
                                                                    NAME: "",
                                                                    COUNT: 1,
                                                                    MEASURE_ID: 1,
                                                                    COMMENT: "",
                                                                    IS_NEW: true
                                                                }));
                                                            }}
                                                            isSubmitting={isSubmitting}
                                                        >{products.length > 0 ? 'Добавить еще' : 'Добавить'}</Button>
                                                    </Box>
                                                </div>
                                            </>

                                        )
                                    }}
                                </Field>
                            </FormGroup>
                        </Box>
                        <Box mt={4}>
                            <div className={'form-buttons'}>
                                <Button
                                    type={'reset'}
                                    variant={'outlined'}
                                    color={'primary'}
                                    disabled={isSubmitting}
                                >Сбросить</Button>
                                <Button
                                    isLoading={isSubmitting}
                                    className={'btn-success'}
                                    onClick={() => {
                                        form.submit(onSubmit, onSuccess, onError);
                                    }}
                                >{submitTitle}</Button>
                                {type === 'create' && (
                                    <Button
                                        color={'secondary'}
                                        isLoading={isSubmitting}
                                        className={'btn-link'}
                                        onClick={() => {
                                            form.submit(
                                                onSubmit,
                                                orderId => {
                                                    onSuccess();
                                                    setPage(7, {invoiceType: 1, orderId});
                                                },
                                                onError
                                            );
                                        }}
                                    >{submitTitle} + счет</Button>
                                )}
                            </div>
                        </Box>
                    </>
                )
            }}
        </Form>
    )
}
export default OrderForm;

function Product(props) {
    const {onChange, onDelete, length, NAME, COUNT, MEASURE_ID, COMMENT} = props;
    return useMemo(() =>
            <Grid container item spacing={2} className={'product-item'}>
                <Grid item style={{width: 200}}>
                    <Input
                        placeholder={'название товара'}
                        value={NAME}
                        onChange={ value => onChange('NAME', value) }
                    />
                </Grid>
                <Grid item style={{width: 75}}>
                    <Input
                        value={COUNT}
                        type={'number'}
                        onChange={ value => onChange('COUNT', value) }
                    />
                </Grid>
                <Grid item style={{width: 75}}>
                    <SimpleSelect
                        value={MEASURE_ID}
                        items={measuresList}
                        getText={ item => item.TITLE }
                        onChange={ value => onChange('MEASURE_ID', value) }
                    />
                </Grid>
                <Grid item style={{width: 250}}>
                    <Input
                        placeholder={'комментарий'}
                        value={COMMENT}
                        onChange={ value => onChange('COMMENT', value) }
                    />
                </Grid>
                <Button
                    className={'btn-delete'}
                    onClick={onDelete}
                >
                    <Icon>close</Icon>
                </Button>
            </Grid>
        , [NAME, COUNT, MEASURE_ID, COMMENT, length])
}