import React, {useState} from 'react';
import {orderStatusesList} from "root/static";
import moment from "moment";

// Components
import Form from "core/desktop/view/form/Form";
import Field from "core/desktop/view/form/Field";
import Input from "core/desktop/view/form/fields/Input";
import UserSelect from "core/desktop/view/form/fields/select/UserSelect";
import SimpleSelect from "core/desktop/view/form/fields/select/SimpleSelect";
import DealSelect from "components/desktop/ui/DealSelect";
import Switch from '@material-ui/core/Switch';
import Grid from "@material-ui/core/Grid";
import FormHelperText from "@material-ui/core/FormHelperText";
import Typography from "@material-ui/core/Typography";
import Button from "core/desktop/view/Button";
import Icon from "core/desktop/view/Icon";

// Constants
import {INPUT_DATE_FORMAT} from "root/constants";

// Actions
import {saveUI} from "actions/ui";


function OrderFilter(props) {
    const {onSubmit, placement, initialValues = {}} = props;

    return (
        <div>
            <Form
                id={'order-filter'}
                onSubmit={onSubmit}
                onDismount={ form => {
                    if (form.isFormSubmitted()) {
                        saveUI('orderFilter', {
                            showAll: form.getFormMeta('showAll'),
                            ...form.getValues()
                        });
                    }
                }}
                settings={{
                    labelWidth: 200,
                    fieldGrid: 3,
                    resetAfterSubmit: false
                }}
                onInitialize={ form => {
                    const fields = [
                        {name: 'object', value: initialValues['object'] || []},
                        {name: 'status', value: initialValues['status'] || []},
                        {name: 'creator', value: initialValues['creator'] || []},
                        {name: 'responsible', value: initialValues['responsible'] || []},
                    ];

                    for (let field of fields) {
                        form.addField(field.name, {
                            value: field.value,
                            initialValue: field.value,
                            meta: field.meta || {required: false},
                            validate: field.validate,
                        });
                    }
                    form.setFormMeta('showAll', initialValues.showAll || false);
                }}
                onReset={ form => {
                    form.reset({
                        'fromDate': moment().startOf('month').format(INPUT_DATE_FORMAT),
                        'toDate': moment().endOf('month').format(INPUT_DATE_FORMAT),
                        'object': [],
                        'status': [],
                        'creator': [],
                        'responsible': []
                    });
                }}
            >
                {(form, settings, isSubmitting) => {
                    const showAll = form.getFormMeta('showAll');
                    return (
                        <div>
                            <div className={'title-with-button'}>
                                <Typography variant={'h2'}>Фильтр</Typography>
                                <Switch
                                    color={"secondary"}
                                    checked={showAll}
                                    onChange={e => {
                                        form.setFormMeta('showAll', e.target.checked);
                                        form.render();
                                    }}
                                />
                            </div>
                            <Grid container spacing={2}>
                                <Grid item>
                                    <Field
                                        name={'fromDate'}
                                        isRequired={true}
                                        label={'Заявки от'}
                                        defaultValue={initialValues['fromDate'] || moment().startOf('month').format(INPUT_DATE_FORMAT)}
                                    >
                                        {inputProps => {
                                            const {error} = inputProps;
                                            return (
                                                <>
                                                    <Input {...inputProps} type={'date'}/>
                                                    {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
                                                </>
                                            )
                                        }}
                                    </Field>
                                </Grid>
                                <Grid item>
                                    <Field
                                        name={'toDate'}
                                        isRequired={true}
                                        label={'Заявки по'}
                                        defaultValue={initialValues['toDate'] || moment().endOf('month').format(INPUT_DATE_FORMAT)}
                                    >
                                        {(inputProps) => {
                                            const {error} = inputProps;
                                            return (
                                                <>
                                                    <Input {...inputProps} type={'date'} error={error}/>
                                                    {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
                                                </>
                                            )
                                        }}
                                    </Field>
                                </Grid>
                                {placement !== "CRM_DEAL_DETAIL_TAB" && (
                                    <Grid item xs={2}>
                                        <Field name={'object'} label={'Объект'}>
                                            {(inputProps, fieldProps) =>
                                                <DealSelect
                                                    {...inputProps}
                                                    {...fieldProps}
                                                    settings={{search: true, multiple: true}}
                                                />
                                            }
                                        </Field>
                                    </Grid>
                                )}
                                {showAll && (
                                    <>
                                        <Grid item xs={2}>
                                            <Field name={'status'} label={'Статус'}>
                                                {(inputProps, fieldProps) => {
                                                    return (
                                                        <SimpleSelect
                                                            {...inputProps}
                                                            {...fieldProps}
                                                            settings={{
                                                                multiple: true
                                                            }}
                                                            items={orderStatusesList}
                                                            getText={ item => item.TITLE }
                                                        />
                                                    )
                                                }}
                                            </Field>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <Field name={'creator'} label={'Создатель'}>
                                                {(inputProps, fieldProps) => {
                                                    return (
                                                        <UserSelect
                                                            {...inputProps}
                                                            {...fieldProps}
                                                            settings={{
                                                                multiple: true,
                                                                search: true,
                                                                startAdornment: <Icon>account_circle</Icon>
                                                            }}
                                                        />
                                                    )
                                                }}
                                            </Field>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <Field name={'responsible'} label={'Ответственный'}>
                                                {(inputProps, fieldProps) => {
                                                    return (
                                                        <UserSelect
                                                            {...inputProps}
                                                            {...fieldProps}
                                                            settings={{
                                                                multiple: true,
                                                                search: true,
                                                                startAdornment: <Icon>account_circle</Icon>
                                                            }}
                                                        />
                                                    )
                                                }}
                                            </Field>
                                        </Grid>
                                    </>
                                )}
                                <Grid item>
                                    <div className={'form-buttons'}>
                                        <Button
                                            type={'reset'}
                                            variant={'outlined'}
                                            color={'primary'}
                                            disabled={isSubmitting}
                                        >Сбросить</Button>
                                        <Button
                                            type={'submit'}
                                            isLoading={isSubmitting}
                                            className={'btn-success'}
                                        >Поиск</Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                    )
                }}
            </Form>
        </div>
    )
}
export default OrderFilter;