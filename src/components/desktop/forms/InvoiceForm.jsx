import React from 'react';

// Classes
import Database from "classes/Database";
import Bitrix24 from "classes/Bitrix24";

// Components
import Form from "core/desktop/view/form/Form";
import TwoColumnField from "core/desktop/view/form/TwoColumnField";
import DynamicSelect from "core/desktop/view/form/fields/select/DynamicSelect";
import DealSelect from "components/desktop/ui/DealSelect";
import Input from "core/desktop/view/form/fields/Input";
import FileLoader from "core/desktop/view/form/fields/file/FileLoader";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Button from "core/desktop/view/Button";

// Constants
import {E_ORDERS} from "root/constants";

// Api functions
import {formatMoney} from "core/Api";
import {setPage} from "actions/app";


function InvoiceForm(props) {
    const {id, onSubmit, onSuccess, onError, initialValues = {}, invoiceType = 1, type} = props;
    const [placement, {ID: dealId}] = Bitrix24.getPlacementInfo();

    const submitTitle = type === 'create' ? 'Создать счет' : 'Сохранить счет';

    return (
        <Form
            id={id}
            settings={{
                labelWidth: 175,
                fieldGrid: 3,
                fieldWidth: 250
            }}
            onSubmit={onSubmit}
            onSuccess={onSuccess}
            onError={onError}
        >
            {(form, settings, isSubmitting) => {
                const {labelWidth, fieldWidth} = settings;
                return (
                    <>
                        <Grid container spacing={2}>
                            {invoiceType === 1 ?
                                <TwoColumnField
                                    name={'order'}
                                    label={'Заявка'}
                                    isRequired={true}
                                    defaultValue={initialValues.order || ''}
                                    fieldWidth={fieldWidth}
                                    labelWidth={labelWidth}
                                >
                                    {(inputProps, fieldProps) =>
                                        <DynamicSelect
                                            {...inputProps}
                                            {...fieldProps}
                                            loadPackage={(filterParams = {}, packageNumber) => {
                                                return Bitrix24.loadPackage('entity.item.get', {
                                                    ENTITY: E_ORDERS,
                                                    filter: {
                                                        PROPERTY_DEAL_ID: placement === 'CRM_DEAL_DETAIL_TAB' ? dealId : null,
                                                        ...filterParams
                                                    }
                                                }, packageNumber);
                                            }}
                                            getSearchItems={phrase => {
                                                return Database.load(E_ORDERS, {filter: {"%NAME": phrase}});
                                            }}
                                            getText={ order => `Заявка № ${order.ID}` }
                                            settings={{search: true}}
                                        />
                                    }
                                </TwoColumnField> :
                                (placement !== "CRM_DEAL_DETAIL_TAB" && (
                                    <TwoColumnField
                                        name={'object'}
                                        label={'Объект'}
                                        isRequired={true}
                                        defaultValue={initialValues.dealId || ''}
                                        fieldWidth={fieldWidth}
                                        labelWidth={labelWidth}
                                    >
                                        {(inputProps, fieldProps) =>
                                            <DealSelect
                                                {...inputProps}
                                                {...fieldProps}
                                                settings={{search: true}}
                                            />
                                        }
                                    </TwoColumnField>
                                ))
                            }
                            <TwoColumnField
                                name={'files'}
                                label={'Файлы счета'}
                                defaultValue={initialValues.files || []}
                                fieldWidth={fieldWidth}
                                labelWidth={labelWidth}
                            >
                                {(inputProps, fieldProps) =>
                                    <FileLoader
                                        {...inputProps}
                                        {...fieldProps}
                                        settings={{
                                            multiple: true
                                        }}
                                        disabled={isSubmitting}
                                    />
                                }
                            </TwoColumnField>
                            <TwoColumnField
                                name={'date'}
                                label={'Дата счета'}
                                defaultValue={initialValues.date || ''}
                                fieldWidth={fieldWidth}
                                labelWidth={labelWidth}
                            >
                                {inputProps => <Input {...inputProps} type={'date'}/>}
                            </TwoColumnField>
                            <TwoColumnField
                                name={'invoiceNumber'}
                                label={'Номер счета'}
                                defaultValue={initialValues.invoiceNumber || ''}
                                fieldWidth={fieldWidth}
                                labelWidth={labelWidth}
                            >
                                {inputProps => <Input {...inputProps}/>}
                            </TwoColumnField>
                            <TwoColumnField
                                name={'invoiceSum'}
                                label={'Сумма'}
                                isRequired={true}
                                defaultValue={initialValues.invoiceSum || ''}
                                fieldWidth={fieldWidth}
                                labelWidth={labelWidth}
                                format={formatMoney}
                            >
                                {inputProps => <Input {...inputProps}/>}
                            </TwoColumnField>
                            <TwoColumnField
                                name={'paymentPurpose'}
                                label={'Назначение платежа'}
                                defaultValue={initialValues.paymentPurpose || ''}
                                fieldWidth={fieldWidth}
                                labelWidth={labelWidth}
                            >
                                {inputProps => <Input {...inputProps}/>}
                            </TwoColumnField>
                        </Grid>
                        <Box mt={4}>
                            <div className={'form-buttons'}>
                                <Button
                                    type={'reset'}
                                    variant={'outlined'}
                                    color={'primary'}
                                    disabled={isSubmitting}
                                >Сбросить</Button>
                                <Button
                                    type={'submit'}
                                    isLoading={isSubmitting}
                                    className={'btn-success'}
                                >{submitTitle}</Button>
                                {type === 'create' && (
                                    <Button
                                        color={'secondary'}
                                        isLoading={isSubmitting}
                                        className={'btn-link'}
                                        onClick={() => {
                                            form.submit(
                                                fields => onSubmit(fields, 2),
                                                onSuccess,
                                                onError
                                            );
                                        }}
                                    >{submitTitle} + согласовать</Button>
                                )}
                            </div>
                        </Box>
                    </>
                )
            }}
        </Form>
    )
}
export default InvoiceForm;

export function getDealId(fields, invoiceType) {
    const [placement, {ID: dealId}] = Bitrix24.getPlacementInfo();

    if (placement === 'CRM_DEAL_DETAIL_TAB') {
        return Promise.resolve(dealId);
    }

    if (invoiceType === 1) {
        const orderId = fields['order'].value;
        return Database.loadOrder(orderId)
            .then( order => Promise.resolve(order.PROPERTY_VALUES.DEAL_ID) )
    }

    return Promise.resolve(fields['object'].value);
}