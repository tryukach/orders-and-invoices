import React, {useState, useContext} from 'react';
import {SnackContext} from "root/contexts";
import moment from "moment";
import {deliveryStatusList, documentStatusesList, paymentList, paymentTypes} from "root/static";

// Constants
import {INPUT_DATE_FORMAT} from "root/constants";

// Classes
import Invoice from "classes/Invoice";
import Data from "classes/Data";

// Actions
import {setPage} from "actions/app";

// Api functions
import {formatMoney, numberToMoney} from "core/Api";

// Components
import Form from "core/desktop/view/form/Form";
import TwoColumnField from "core/desktop/view/form/TwoColumnField";
import SimpleSelect from "core/desktop/view/form/fields/select/SimpleSelect";
import Input from "core/desktop/view/form/fields/Input";
import TextArea from "core/desktop/view/form/fields/TextArea";
import FileLoader from "core/desktop/view/form/fields/file/FileLoader";
import Button from "core/desktop/view/Button";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import StaticField from "components/desktop/ui/StaticField";
import Popup from "core/desktop/view/Popup";
import DeclineInvoiceForm from "components/desktop/forms/DeclineInvoiceForm";

export default function ApproveInvoiceForm(props) {
    const {invoice} = props;
    const snackManager = useContext(SnackContext);

    const statusId = +invoice.PROPERTY_VALUES.STATUS_ID;
    const labels = Invoice.getLabelsByStage(invoice, statusId);

    return (
        <Form
            id={'invoice-approve'}
            settings={{
                fieldGrid: 3,
                fieldWidth: 300,
                labelWidth: 225
            }}
            onSubmit={ fields => Invoice.approve(fields, invoice) }
            onSuccess={ nextStatusId => {
                const currentStatusId = +invoice.PROPERTY_VALUES.STATUS_ID;

                nextStatusId === currentStatusId ?
                    snackManager.showMessage(`Счет № ${invoice.ID} успешно сохранен`, {variant: 'success'}):
                    nextStatusId === 9 ?
                        snackManager.showMessage(`Счет № ${invoice.ID} завершен`, {variant: 'success'}):
                        snackManager.showMessage(`Счет № ${invoice.ID} переведен на следующий статус`, {variant: 'success'});

                setPage(2);
            }}
            onError={ error => {
                console.log(error);
                snackManager.showMessage('Произошла ошибка', {variant: 'error'});
            }}
        >
            {(form, settings, isSubmitting) => {
                return (
                    <div>
                        <Grid container spacing={2}>
                            {getDynamicFields(invoice, form, settings, isSubmitting)}
                        </Grid>
                        <Box mt={4}>
                            <div className={'form-buttons'}>
                                <Button
                                    type={'submit'}
                                    isLoading={isSubmitting}
                                    className={'btn-success'}
                                >{labels['actionButtonTitle']}</Button>
                                {[2,3,4].indexOf(statusId) !== -1 && (
                                    <DeclineButton
                                        className={'btn-error'}
                                        disabled={isSubmitting}
                                        invoiceId={invoice.ID}
                                    >Отклонить</DeclineButton>
                                )}
                            </div>
                        </Box>
                    </div>
                )
            }}
        </Form>
    )
}

function DeclineButton(props) {
    const {invoiceId, ...buttonProps} = props;
    const [open, setOpen] = useState(false);
    return (
        <>
            <div data-popup={'decline-invoice'}>
                <Button
                    {...buttonProps}
                    onClick={() => setOpen(true)}
                >Отклонить</Button>
            </div>
            <Popup
                id={'decline-invoice'}
                open={open}
                onClose={() => setOpen(false)}
                fixedWidth={450}
            >
                <DeclineInvoiceForm invoiceId={invoiceId}/>
            </Popup>
        </>
    )
}

export function getStaticFields(statusId, invoice) {
    switch (statusId) {
        case 3: return <StaticField label={'Комментарий инженера'}>{invoice.PROPERTY_VALUES.E_COMMENT || '-'}</StaticField>;
        case 4:
            return (
                <>
                    <StaticField label={'Тип оплаты'}><span className={'highlight'} style={{background: "#535c69"}}>{paymentTypes.byId[invoice.PROPERTY_VALUES.PAYMENT_TYPE_ID].TITLE}</span></StaticField>
                    <StaticField label={'Сумма оплаты'}><span className={'highlight'} style={{background: "#535c69"}}>{numberToMoney(invoice.PROPERTY_VALUES.CURRENT_PAYMENT_SUM)}</span></StaticField>
                    <StaticField label={'Комментарий руководителя'}>{invoice.PROPERTY_VALUES.D_COMMENT || '-'}</StaticField>
                </>
            );
        case 6: return <StaticField label={'Дата планируемой поставки'}>{Data.extractDate(invoice.PROPERTY_VALUES.DELIVERY_DATE) || '-'}</StaticField>;
        case 7: return <StaticField label={'Дата поставки'}>{Data.extractDate(invoice.PROPERTY_VALUES.DELIVERY_DATE) || '-'}</StaticField>;
        default: return;
    }
}

export function getDynamicData(statusId, invoice) {
    switch (statusId) {
        case 4: return getStaticFields(3, invoice);
        case 5: return (
            <React.Fragment>
                {getDynamicData(4, invoice)}
                <StaticField label={'Комментарий руководителя'}>{invoice.PROPERTY_VALUES.D_COMMENT || '-'}</StaticField>
            </React.Fragment>
        );
        case 6: case 7: return getDynamicData(5, invoice);
    }
}

export function getDynamicFields(invoice, form, settings, isSubmitting) {
    const {fieldWidth, labelWidth} = settings;
    const fields = form.getFields();

    const statusId = +invoice.PROPERTY_VALUES.STATUS_ID;
    const toDay = moment();

    switch (statusId) {
        case 2: return (
            <TwoColumnField
                name={'commentE'}
                defaultValue={''}
                label={'Комментарий инженера'}
                labelWidth={labelWidth}
                fieldWidth={fieldWidth}
            >
                {inputProps => <TextArea {...inputProps}/>}
            </TwoColumnField>
        );
        case 3:
            const restPaymentSum = Invoice.getRestPaymentSum(invoice);
            return (
                <>
                    <TwoColumnField
                        name={'paymentType'}
                        defaultValue={1}
                        label={'Тип оплаты'}
                        labelWidth={labelWidth}
                        fieldWidth={fieldWidth}
                    >
                        {inputProps =>
                            <SimpleSelect
                                {...inputProps}
                                items={paymentList}
                                getText={ item => item.TITLE }
                            />
                        }
                    </TwoColumnField>
                    {fields['paymentType'] && fields['paymentType'].value === 2 && (
                        <TwoColumnField
                            name={'paymentSum'}
                            isRequired={true}
                            defaultValue={numberToMoney(restPaymentSum)}
                            label={'Сумма частичной оплаты'}
                            labelWidth={labelWidth}
                            fieldWidth={fieldWidth}
                            format={formatMoney}
                        >
                            {inputProps => <Input {...inputProps}/>}
                        </TwoColumnField>
                    )}
                    <TwoColumnField
                        name={'commentD'}
                        defaultValue={''}
                        label={'Комментарий руководителя'}
                        labelWidth={labelWidth}
                        fieldWidth={fieldWidth}
                    >
                        {inputProps => <TextArea {...inputProps}/>}
                    </TwoColumnField>
                </>
            );
        case 4:
            return (
                <>
                    <TwoColumnField
                        name={'paymentDate'}
                        isRequired={true}
                        defaultValue={toDay.format(INPUT_DATE_FORMAT)}
                        label={'Дата оплаты счета'}
                        labelWidth={labelWidth}
                        fieldWidth={fieldWidth}
                    >
                        {inputProps => <Input {...inputProps} type={'date'}/>}
                    </TwoColumnField>
                    <TwoColumnField
                        name={'ppFiles'}
                        isRequired={true}
                        defaultValue={[]}
                        label={'Файлы п/п'}
                        labelWidth={labelWidth}
                        fieldWidth={fieldWidth}
                    >
                        {(inputProps, fieldProps) =>
                            <FileLoader
                                {...inputProps}
                                {...fieldProps}
                                settings={{
                                    multiple: true
                                }}
                                disabled={isSubmitting}
                            />
                        }
                    </TwoColumnField>
                    <TwoColumnField
                        name={'ppSum'}
                        isRequired={true}
                        defaultValue={numberToMoney(invoice.PROPERTY_VALUES.CURRENT_PAYMENT_SUM)}
                        label={'Сумма п/п'}
                        labelWidth={labelWidth}
                        fieldWidth={fieldWidth}
                    >
                        {inputProps => <Input {...inputProps}/>}
                    </TwoColumnField>
                </>
            );
        case 5: return (
            <TwoColumnField
                name={'deliveryDate'}
                defaultValue={toDay.format(INPUT_DATE_FORMAT)}
                isRequired={true}
                label={'Дата планируемой поставки'}
                labelWidth={labelWidth}
                fieldWidth={fieldWidth}
            >
                {inputProps => <Input {...inputProps} type={'date'}/>}
            </TwoColumnField>
        );
        case 6:
            const prevDeliveryDate = moment(invoice.PROPERTY_VALUES.DELIVERY_DATE);
            const initialDeliveryDate = prevDeliveryDate.isBefore(toDay) ? toDay : prevDeliveryDate;

            return (
                <>
                    <TwoColumnField
                        name={'deliveryStatus'}
                        defaultValue={1}
                        label={'Статус поставки'}
                        labelWidth={labelWidth}
                        fieldWidth={fieldWidth}
                    >
                        {inputProps =>
                            <SimpleSelect
                                {...inputProps}
                                items={deliveryStatusList}
                                getText={ item => item.TITLE }
                            />
                        }
                    </TwoColumnField>
                    {fields['deliveryStatus'] && fields['deliveryStatus'].value === 2 && (
                        <TwoColumnField
                            name={'deliveryDate'}
                            isRequired={true}
                            defaultValue={initialDeliveryDate.format(INPUT_DATE_FORMAT)}
                            label={'Новая дата планируемой поставки'}
                            labelWidth={labelWidth}
                            fieldWidth={fieldWidth}
                        >
                            {inputProps => <Input {...inputProps} type={'date'}/>}
                        </TwoColumnField>
                    )}
                </>
            );
        case 7:
            const deliveryDate = moment(invoice.PROPERTY_VALUES.DELIVERY_DATE);
            const initialDocumentsReceiveDate = deliveryDate.isBefore(toDay) ? toDay : deliveryDate;
            return (
                <>
                    <TwoColumnField
                        name={'closedDocuments'}
                        defaultValue={[]}
                        label={'Закрывающие документы'}
                        labelWidth={labelWidth}
                        fieldWidth={fieldWidth}
                    >
                        {(inputProps, fieldProps) =>
                            <FileLoader
                                {...inputProps}
                                {...fieldProps}
                                settings={{
                                    multiple: true
                                }}
                                disabled={isSubmitting}
                            />
                        }
                    </TwoColumnField>
                    <TwoColumnField
                        name={'documentsReceiveDate'}
                        isRequired={true}
                        defaultValue={initialDocumentsReceiveDate.format(INPUT_DATE_FORMAT)}
                        label={'Дата получения оригинальных документов'}
                        labelWidth={labelWidth}
                        fieldWidth={fieldWidth}
                    >
                        {inputProps => <Input {...inputProps} type={'date'}/>}
                    </TwoColumnField>
                    <TwoColumnField
                        name={'documentsReceiveStatus'}
                        defaultValue={1}
                        label={'Статус получения оригинальных документов'}
                        labelWidth={labelWidth}
                        fieldWidth={fieldWidth}
                    >
                        {inputProps =>
                            <SimpleSelect
                                {...inputProps}
                                items={documentStatusesList}
                                getText={ item => item.TITLE }
                            />
                        }
                    </TwoColumnField>
                </>
            );
        default: return <div>no case for this</div>;
    }
}