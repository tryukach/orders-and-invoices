import React, {useContext} from 'react';
import {connect} from "react-redux";
import {SnackContext} from "root/contexts";

// Classes
import Invoice from "classes/Invoice";

// Actions
import {setPage} from "actions/app";

// Components
import Form from "core/desktop/view/form/Form";
import Field from "core/desktop/view/form/Field";
import TextArea from "core/desktop/view/form/fields/TextArea";
import Button from "core/desktop/view/Button";
import Grid from "@material-ui/core/Grid";
import FormHelperText from "@material-ui/core/FormHelperText";

function DeclineInvoiceForm(props) {
    const {invoiceId, currentUser} = props;
    const snackManager = useContext(SnackContext);

    return (
        <Form id={'decline-invoice'}>
            {(form, settings, isSubmitting) => {
                return (
                    <Grid container spacing={2}>
                        <Grid item style={{width: 250}}>
                                <Field
                                    name={'reason'}
                                    label={'Причина отклонения'}
                                    defaultValue={''}
                                    isRequired={true}
                                >
                                    {inputProps => {
                                        const {error} = inputProps;
                                        return (
                                            <>
                                                <TextArea {...inputProps}/>
                                                {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
                                            </>
                                        )
                                    }}
                                </Field>
                        </Grid>
                        <Grid item>
                            <div className={'form-buttons'}>
                                    <Button
                                        onClick={() => {
                                            const onSuccess = () => {
                                                snackManager.showMessage(`Счет № ${invoiceId} отклонен`, {variant: 'success'});
                                                setPage(2);
                                            };
                                            const onError = () => {
                                                snackManager.showMessage(`Произошла ошибка`, {variant: 'error'});
                                            };

                                            form.submit( fields => {
                                                return Invoice.update(invoiceId, {
                                                    PROPERTY_VALUES: {
                                                        DECLINE_REASON: fields['reason'].value,
                                                        DECLINE_BY: currentUser,
                                                        STATUS_ID: 10
                                                    }
                                                });
                                            }, onSuccess, onError);
                                        }}
                                        isLoading={isSubmitting}
                                        className={'btn-success'}
                                    >Отклонить</Button>
                            </div>
                        </Grid>
                    </Grid>
                )
            }}
        </Form>
    )
}
export default connect(state => ({
    currentUser: state.app.currentUser
}))(DeclineInvoiceForm);