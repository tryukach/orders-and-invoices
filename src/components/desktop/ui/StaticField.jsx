import React from 'react';

// Components
import Grid from "@material-ui/core/Grid";
import Label from "core/desktop/view/form/Label";

export default function StaticField(props) {
    const {label = '', children = '', width = 250} = props;
    return (
        <Grid container item>
            <Grid item style={{width: 225}}><Label>{label}</Label></Grid>
            <Grid item style={{width}}>{children}</Grid>
        </Grid>
    )
}