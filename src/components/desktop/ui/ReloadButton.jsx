import React from 'react';
import {connect} from "react-redux";

// Actions
import {initStaticData} from "actions/staticData";
import {initApp, lock} from "actions/app";

// Classes
import App from "classes/App";

// Components
import Icon from "core/desktop/view/Icon";
import Tooltip from "core/desktop/view/Tooltip";

function ReloadButton(props) {
    const {constAppState} = props;
    return (
        <Tooltip title={'Обновить данные'} placement={'left'}>
            <Icon
                size={'small'}
                color={'white'}
                onClick={() => {
                    lock('Обновляем приложение...');
                    App.reloadData()
                        .then( state => {
                            initApp(Object.assign({}, state.app, constAppState));
                            initStaticData(state.staticData);
                            lock(false);
                        })
                }}
            >refresh</Icon>
        </Tooltip>
    )
}
export default connect(state => ({
    constAppState: {
        currentPage: state.app.currentPage,
        pageOptions: state.app.pageOptions,
        prevPage: state.app.prevPage
    }
}))(ReloadButton);