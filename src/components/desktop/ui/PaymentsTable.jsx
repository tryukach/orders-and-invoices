import React, {useState, useEffect} from 'react';
import moment from "moment";

// Api functions
import {numberToMoney} from "core/Api";

// Constants
import {DATE_FORMAT} from "root/constants";

// Components
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import {renderFiles} from "core/desktop/view/form/fields/file/FileLoader";
import SimpleTable from "core/desktop/view/table/SimpleTable";
import PageLoader from "core/desktop/view/PageLoader";

// Classes
import Database from "classes/Database";

export default function PaymentsTable(props) {
    const {payments = []} = props;

    const [status, setStatus] = useState('loading');
    const [files = [], setFiles] = useState(null);

    useEffect(() => {
        const ids = payments.map( p => p.ID );
        Database.getFilesBy(ids)
            .then( files => {
                setFiles(files);
                setStatus('ready');
            })
    }, []);

    if (status === 'loading') {
        return <PageLoader/>;
    }

    return (
        <>
            <Typography variant={'h4'}>Оплаты по счету</Typography>
            <Box mt={1} width={530}>
                <SimpleTable
                    id={'payments'}
                    items={payments}
                    settings={{
                        useSelect: false,
                        orderBy: 1,
                        usePagination: false,
                        notFound: 'Оплат нет'
                    }}
                    columns={[
                        {id: 1, text: 'Дата оплаты'},
                        {id: 2, text: 'Сумма', measure: 'руб.'},
                        {id: 3, text: 'Файлы', sortable: false},
                    ]}
                    getCellValue={(id, invoice) => getCellValue(id, invoice, {files})}
                />
            </Box>
        </>
    )
}

function getCellValue(id, payment, {files}) {
    switch (id) {
        case 1:
            const date = moment(payment.PROPERTY_VALUES.PAYMENT_DATE);
            return date ? {
                source: date.valueOf(),
                display: date.format(DATE_FORMAT)
            } : '-';
        case 2: return {
            display: numberToMoney(payment.PROPERTY_VALUES.PP_SUM),
            source: parseFloat(payment.PROPERTY_VALUES.PP_SUM + "")
        };
        case 3:
            const paymentFiles = files.filter( f => f.PROPERTY_VALUES.ENTITY_ID === payment.ID );
            return paymentFiles.length > 0 ? renderFiles(paymentFiles) : '-';
    }
}