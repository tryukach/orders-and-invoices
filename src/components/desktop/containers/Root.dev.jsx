import React from 'react';
import { Provider } from 'react-redux';
import DevTools from 'core/DevTools';

// Styles
import "assets/css/dev.less";

export default function(props) {
    const {initHeight, store, useTools, renderContainer} = props;

    return (
        <Provider store={store}>
            {useTools ?
                <div className={'development'} style={{minHeight: initHeight}}>
                    {renderContainer()}
                    <DevTools/>
                </div>:
                renderContainer()
            }
        </Provider>
    );
}