import React from "react";
import {connect} from "react-redux";

// Classes
import Bitrix24 from "classes/Bitrix24";

// Components
import AppBar from "core/desktop/view/AppBar";
import Contacts from "core/desktop/view/Contacts";
import Hidden from "@material-ui/core/Hidden";
import Menu from "core/desktop/view/menu";
import Box from "@material-ui/core/Box";
import ReloadButton from "components/desktop/ui/ReloadButton";

function AppHeader(props) {
    const {showContacts = true, pages, currentPage} = props;
    const filteredPages = filterPages(pages, {});

    return (
        <div className={'app-header'}>
            {showContacts && Bitrix24.isAdmin() && <Hidden smDown={true}><Contacts/></Hidden>}
            <AppBar>
                <Menu currentPage={currentPage} pages={filteredPages}/>
                <Box display={'flex'} alignItems={'center'} mr={2}><ReloadButton/></Box>
            </AppBar>
        </div>
    );
}
export default connect( state => ({
    pages: state.app.pages,
    currentPage: state.app.currentPage
}) )(AppHeader);

function filterPages(pages, state = {}) {
    return pages.filter( page => {
        if (typeof page['isHidden'] === "function") {
            return !page['isHidden'](state);
        }
        if (typeof page['isShow'] === "function") {
            return page['isShow'](state);
        }
        return page.hasOwnProperty('title');
    })
}