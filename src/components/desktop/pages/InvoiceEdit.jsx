import React, {useContext, useState, useEffect} from 'react';
import {connect} from "react-redux";
import {SnackContext} from "root/contexts";
import moment from "moment";

// Components
import InvoiceForm, {getDealId} from "components/desktop/forms/InvoiceForm";
import Back from "core/desktop/view/Back";
import Box from "@material-ui/core/Box";
import PageLoader from "core/desktop/view/PageLoader";

// Api functions
import {callMany, numberToMoney} from "core/Api";

// Classes
import Invoice from "classes/Invoice";
import Database from "classes/Database";
import Data from "classes/Data";

// Actions
import {setPage} from "actions/app";

// Constants
import {INPUT_DATE_FORMAT} from "root/constants";

function InvoiceEdit(props) {
    const {invoiceId, prevPage} = props;
    const snackManager = useContext(SnackContext);

    const [status, setStatus] = useState('loading');
    const [initialValues, setInitialValues] = useState({});
    const [invoice, setInvoice] = useState({});

    useEffect(() => {
        callMany({
            invoice: {handler: () => Database.loadInvoice(invoiceId)},
            files: {handler: () => Database.getInvoiceFiles(invoiceId)},
        }).then( response => {
            const {invoice, files = []} = response;

            const initialValues = {
                order: invoice.PROPERTY_VALUES.ORDER_ID,
                dealId: invoice.PROPERTY_VALUES.DEAL_ID,
                files: Data.extractFiles(files),
                invoiceSum: numberToMoney(invoice.PROPERTY_VALUES.SUM),
                paymentPurpose: invoice.PROPERTY_VALUES.PAYMENT_PURPOSE,
                invoiceNumber: invoice.PROPERTY_VALUES.INVOICE_NUMBER,
                date: invoice.PROPERTY_VALUES.INVOICE_DATE ?
                    moment(invoice.PROPERTY_VALUES.INVOICE_DATE).format(INPUT_DATE_FORMAT):
                    ""
            };

            setInitialValues(initialValues);
            setInvoice(invoice);
            setStatus('ready');
        })
    }, []);

    if (status === 'loading') {
        return <PageLoader/>;
    }

    const invoiceType = +invoice.PROPERTY_VALUES.INVOICE_TYPE_ID;

    return (
        <div>
            <Box mb={2}>
                <Back to={prevPage}>Создание нового счета</Back>
            </Box>
            <InvoiceForm
                id={'invoice-create'}
                type={'edit'}
                onSubmit={fields => {
                    return getDealId(fields, invoiceType)
                        .then( dealId => Invoice.edit(fields, invoice.ID, {invoiceType, dealId}) );
                }}
                onSuccess={() => {
                    snackManager.showMessage('Счет сохранен', {variant: 'success'});
                    setPage(prevPage);
                }}
                onError={() => {
                    snackManager.showMessage('Произошла ошибка', {variant: 'error'});
                }}
                invoiceType={invoiceType}
                initialValues={initialValues}
            />
        </div>
    )
}
export default connect(state => ({
    prevPage: state.app.prevPage,
    invoiceId: state.app.pageOptions.invoiceId
}))(InvoiceEdit);