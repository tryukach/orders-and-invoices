import React from 'react';
import {connect} from "react-redux";

// Components
import Box from "@material-ui/core/Box";
import Back from "core/desktop/view/Back";
import OrderDetailContent from "components/desktop/OrderDetailContent";

function OrderDetail(props) {
    const {orderId, prevPage} = props;
    return (
        <div>
            <Box mb={2}>
                <Back to={prevPage}>Заявка № {orderId}</Back>
            </Box>
            <OrderDetailContent orderId={orderId}/>
        </div>
    )
}
export default connect( state => ({
    orderId: state.app.pageOptions.orderId,
    prevPage: state.app['prevPage']
}))(OrderDetail);