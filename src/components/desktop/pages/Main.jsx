import React, {useEffect, useState, useContext} from 'react';
import moment from "moment";
import {connect} from "react-redux";
import {orderStatuses} from "root/static";
import {SnackContext} from "root/contexts";
import langPhrases from "root/languages";

// Classes
import Order from "classes/Order";
import Bitrix24 from "classes/Bitrix24";
import Data from "classes/Data";
import Database from "classes/Database";

// Constants
import {DATE_TIME_FORMAT} from "root/constants";

// Api functions
import {numberToMoney, callMany, callBatch} from "core/Api";

// Components
import Typography from "@material-ui/core/Typography";
import Button, {LoadButton} from "core/desktop/view/Button";
import Table from "core/desktop/view/table/SimpleTable";
import Box from "@material-ui/core/Box";
import PageLoader from "core/desktop/view/PageLoader";
import OrderFilter from "components/desktop/forms/OrderFilter";

// Actions
import {setPage} from "root/actions/app";

function Main(props) {
    const {users, currentUser, currentRole, initialValues = {}} = props;
    const [placement, {ID: dealId}] = Bitrix24.getPlacementInfo();

    const snackManager = useContext(SnackContext);

    const [data, setData] = useState({});
    const {orders, invoices = [], deals} = data;

    const [status, setStatus] = useState('loading');

    useEffect(() => {
        loadData({
            ">=DATE_ACTIVE_FROM": initialValues['fromDate'] ?
                moment(initialValues['fromDate']).startOf("day").format():
                moment().startOf("month").startOf('day').format(),
            "<=DATE_ACTIVE_FROM": initialValues['toDate'] ?
                moment(initialValues['toDate']).endOf("day").format():
                moment().endOf("month").endOf('day').format(),
            "PROPERTY_DEAL_ID": placement === "CRM_DEAL_DETAIL_TAB" ? dealId : (initialValues['object'] ||  null),
            "PROPERTY_STATUS_ID": initialValues['status'] || null,
            "PROPERTY_CREATOR_ID": initialValues['creator'] || null,
            "PROPERTY_RESPONSIBLE_ID": initialValues['responsible'] || null,
        }).then( data => {
            setData(data);
            setStatus('ready');
        })
    }, []);

    if (status === 'loading') {
        return <PageLoader/>;
    }

    return (
        <>
            <OrderFilter
                placement={placement}
                initialValues={initialValues}
                onSubmit={fields => {
                    const params = {
                        ">=DATE_ACTIVE_FROM": moment(fields['fromDate'].value).startOf("day").format(),
                        "<=DATE_ACTIVE_FROM": moment(fields['toDate'].value).endOf("day").format(),
                        "PROPERTY_DEAL_ID": placement === "CRM_DEAL_DETAIL_TAB" ? dealId : fields['object'].value,
                        "PROPERTY_STATUS_ID": fields['status'] ? fields['status'].value : null,
                        "PROPERTY_CREATOR_ID": fields['creator'] ? fields['creator'].value : null,
                        "PROPERTY_RESPONSIBLE_ID": fields['responsible'] ? fields['responsible'].value: null,
                    };

                    return loadData(params).then( data => setData(data) );
                }}
            />
            <Box mt={4}>
                <div className={'title-with-button'}>
                    <Typography variant={'h2'}>Заявки на закупку</Typography>
                    {([3,4].indexOf(currentRole) === -1 || Bitrix24.isAdmin()) && (
                        <Button
                            className={"btn-link"}
                            onClick={() => setPage(4)}
                            color={'secondary'}
                        >Создать заявку</Button>
                    )}
                </div>
                <Table
                    id={'orders'}
                    items={orders}
                    settings={{
                        useSelect: Bitrix24.isAdmin() || currentRole === 1,
                        useContextMenu: true,
                        useRowContextMenu: (order) => {
                            return +order.PROPERTY_VALUES.CREATOR_ID === currentUser || Bitrix24.isAdmin()
                        },
                        orderBy: 2,
                        perPage: 20,
                        perPageOptions: [20,50,100, langPhrases['all']],
                        notFound: 'Заявок нет'
                    }}
                    columns={[
                        {id: 1, text: '№ заявки'},
                        {id: 2, text: 'Дата создания'},
                        {id: 3, text: 'Объект', isHidden: placement === "CRM_DEAL_DETAIL_TAB"},
                        {id: 4, text: 'Описание заявки', sortable: false, width: 200},
                        {id: 5, text: 'Создатель', width: 150},
                        {id: 6, text: 'Ответственный', width: 150},
                        {id: 7, text: 'Статус', measure: 'руб.'},
                        {id: 8, text: 'Счетов', measure: 'шт.'},
                        {id: 9, text: 'Сумма заявки', measure: 'руб.', width: 200},
                        {id: 10, text: 'Действие'},
                    ]}
                    getCellValue={(columnId, order, updateRow) => {
                        return getCellValue(columnId, order, updateRow, {
                            users,
                            deals,
                            invoices,
                            currentUser,
                            currentRole
                        })
                    }}
                    actions={[
                        {
                            id: 1,
                            title: 'удалить',
                            type: 'delete',
                            handler: ids => {
                                return Order.deleteMany(
                                    ids,
                                    message => snackManager.showMessage(message, {variant: 'success'}),
                                    message => snackManager.showMessage(message, {variant: 'error'})
                                );
                            }
                        }
                    ]}
                    contextMenuActions={[
                        {
                            id: 1,
                            title: 'редактировать',
                            isShow: order => {
                                return currentUser === +order.PROPERTY_VALUES.CREATOR_ID && +order.PROPERTY_VALUES.STATUS_ID !== 2
                            },
                            handler: order => {
                                setPage(5, {orderId: order.ID});
                                return Promise.resolve();
                            }
                        },
                        {
                            id: 2,
                            title: 'удалить',
                            type: 'delete',
                            isShow: order => {
                                return currentUser === +order.PROPERTY_VALUES.CREATOR_ID || Bitrix24.isAdmin()
                            },
                            handler: order => {
                                return Order.delete(order.ID)
                                    .then(
                                        () => snackManager.showMessage('Заявка удалена', {variant: 'success'}),
                                        () => snackManager.showMessage('Произошла ошибка', {variant: 'error'})
                                    );
                            }
                        }
                    ]}
                />
            </Box>
            {orders.length > 0 && (
                <Box mt={2}>
                    <Box mb={1}><Typography variant={'h2'}>Суммарно</Typography></Box>
                    <div><Typography variant={'body2'}>Заявок: {orders.length}</Typography></div>
                    <div><Typography variant={'body2'}>Счетов по заявкам: {invoices.length}</Typography></div>
                    <div><Typography variant={'body2'}>Сумма по всем счетам: {Data.calcInvoiceSum(invoices)}</Typography></div>
                </Box>
            )}
        </>
    );
}
export default connect( state => ({
    users: state.staticData.users,
    currentUser: state.app.currentUser,
    currentRole: state.app.currentRole,
    initialValues: state.ui['orderFilter'] || {}
}) )(Main);

function getCellValue(id, order, updateRow, data) {
    const {invoices = [], users, currentUser, currentRole, deals} = data;

    const invoicesByOrder = invoices.filter( invoice => +invoice.PROPERTY_VALUES.ORDER_ID === +order.ID);
    const sum = invoicesByOrder.reduce((acc, invoice) => {
        return acc + parseFloat(invoice.PROPERTY_VALUES.SUM + "");
    }, 0);

    switch (id) {
        case 1: return {
            display: <a onClick={() => setPage(6, {orderId: order.ID})} title={'просмотр заявки'}>{order.ID}</a>,
            source: +order.ID
        };
        case 2:
            const date = moment(order['DATE_ACTIVE_FROM']);
            return {display: date.format(DATE_TIME_FORMAT), source: date.valueOf()};
        case 3:
            const deal = deals.byId[order.PROPERTY_VALUES.DEAL_ID];
            return {
                display: Bitrix24.getDealLink( deal ),
                source: Bitrix24.getDealName( deal ),
            };
        case 5: return Data.getUserDisplayName(users.byId[order.PROPERTY_VALUES.CREATOR_ID]);
        case 6: return Data.getUserDisplayName(users.byId[order.PROPERTY_VALUES.RESPONSIBLE_ID]);
        case 7:
            let color = "#54a3d8";
            const statusTitle = orderStatuses.byId[order.PROPERTY_VALUES.STATUS_ID].TITLE;

            switch (+order.PROPERTY_VALUES.STATUS_ID) {
                case 1: color = "#a99d42"; break;
                case 2: color = "#5ca968"; break;
                case 3: color = "#d85353"; break;
            }
            return {
                display: <span className={'highlight'} style={{background: color}}>{statusTitle}</span>,
                source: statusTitle
            };
        case 8: return invoicesByOrder.length;
        case 9: return {display: numberToMoney(sum), source: sum};
        case 4: return order.DETAIL_TEXT;
        case 10:
            if ((+order.PROPERTY_VALUES.CREATOR_ID === +currentUser || currentRole === 1) &&
                +order.PROPERTY_VALUES.STATUS_ID !== 2 && Order.isComplete(order.ID, invoicesByOrder)) {
                return (
                    <CompleteOrderButton
                        orderId={order.ID}
                        updateRow={updateRow}
                    />
                );
            }
            return '-';
    }
}

function CompleteOrderButton(props) {
    const {orderId, updateRow} = props;
    const snackManager = useContext(SnackContext);

    return (
        <LoadButton
            size={'small'}
            className={'btn-success'}
            onClick={() => {
                return Order.complete(orderId)
                    .then( () => {
                        updateRow(orderId, {PROPERTY_VALUES: {STATUS_ID: 2}});
                        snackManager.showMessage(`Заявка № ${orderId} завершена`, {variant: 'success'});
                        return Promise.resolve();
                    })
            }}
        >Завершить</LoadButton>
    );
}

function loadData(params) {
    return callMany({
        orders: {handler: () => Database.loadOrders(params)},
        deals: {
            handler: ({orders}) => {
                const dealIds = orders.map( o => o.PROPERTY_VALUES.DEAL_ID );
                return Bitrix24.getDealsByIds(dealIds);
            }
        },
        invoices: {
            handler: ({orders}) => {
                const ids = orders.map( o => o.ID );
                return Database.loadInvoicesByOrder(ids);
            }
        }
    })
}