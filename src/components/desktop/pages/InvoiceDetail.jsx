import React, {useState, useEffect} from 'react';
import {invoiceStatuses, paymentStatuses, deliveryStatuses, documentStatuses} from "root/static";
import {connect} from "react-redux";

// Classes
import Data from "classes/Data";
import Database from "classes/Database";

// Api functions
import {callMany, numberToMoney} from "core/Api";

// Components
import StaticField from "components/desktop/ui/StaticField";
import Box from "@material-ui/core/Box";
import Back from "core/desktop/view/Back";
import PageLoader from "core/desktop/view/PageLoader";
import Grid from "@material-ui/core/Grid";
import {renderFiles} from "core/desktop/view/form/fields/file/FileLoader";
import PaymentsTable from "components/desktop/ui/PaymentsTable";
import Message from "core/desktop/view/Message";

function InvoiceDetail(props) {
    const {invoiceId, prevPage, users} = props;

    const [status, setStatus] = useState('loading');
    const [invoice, setInvoice] = useState(null);
    const [data, setData] = useState({});

    useEffect(() => {
        callMany({
            invoice: {handler: () => Database.loadInvoice(invoiceId)},
            files: {handler: () => Database.getInvoiceFiles(invoiceId)},
            payments: {handler: () => Database.getInvoicePayments(invoiceId)},
            documentFiles: {handler: () => Database.getFilesBy(invoiceId, 'closedDocuments')}
        }).then( response => {
            const {invoice, ...data} = response;

            setInvoice(invoice);
            setData(data);
            setStatus('ready');
        })
    }, []);

    if (status === 'loading') {
        return <PageLoader/>;
    }

    if (!invoice) {
        return <Message type={'warning'}>Счета № {invoiceId} не существует</Message>
    }

    const {files = [], payments = [], documentFiles = []} = data;
    const deliveryStatusId = +invoice.PROPERTY_VALUES.DELIVERY_STATUS_ID;
    const invoiceStatusId = +invoice.PROPERTY_VALUES.STATUS_ID;
    const documentStatusId = +invoice.PROPERTY_VALUES.DOCUMENTS_RECEIVE_STATUS_ID;

    return (
        <div>
            <Box mb={2}>
                <Back to={prevPage}>Счет № {invoiceId}</Back>
            </Box>
            <Grid container spacing={1}>
                <StaticField label={'Статус'}>{invoiceStatuses.byId[invoiceStatusId].TITLE}</StaticField>
                <StaticField label={'Файлы счета'}>{files.length === 0 ? '-' : renderFiles(files)}</StaticField>
                <StaticField label={'Дата счета'}>{Data.extractDate(invoice.PROPERTY_VALUES.INVOICE_DATE) || '-'}</StaticField>
                <StaticField label={'Номер счета'}>{invoice.PROPERTY_VALUES.INVOICE_NUMBER || '-'}</StaticField>
                <StaticField label={'Сумма счета'}>{numberToMoney(invoice.PROPERTY_VALUES.SUM)}</StaticField>
                <StaticField label={'Сумма оплаты'}>{numberToMoney(invoice.PROPERTY_VALUES.PAYED_SUM)}</StaticField>
                <StaticField label={'Статус оплаты'}>{paymentStatuses.byId[invoice.PROPERTY_VALUES.PAYED_STATUS].TITLE}</StaticField>
                <StaticField label={'Назначение платежа'} width={500}>{invoice.PROPERTY_VALUES.PAYMENT_PURPOSE || '-'}</StaticField>
            </Grid>
            <Box my={4}><PaymentsTable payments={payments}/></Box>
            <Box my={4}>
                <Grid container spacing={1}>
                    {deliveryStatusId !== 1 ?
                        <StaticField label={'Дата планируемой поставки'}>{Data.extractDate(invoice.PROPERTY_VALUES.DELIVERY_DATE) || '-'}</StaticField>:
                        <StaticField label={'Дата поставки'}>{Data.extractDate(invoice.PROPERTY_VALUES.DELIVERY_DATE, true) || '-'}</StaticField>
                    }
                    <StaticField label={'Статус поставки'}>{deliveryStatusId ? deliveryStatuses.byId[deliveryStatusId].TITLE: '-'}</StaticField>
                </Grid>
            </Box>
            <Box my={4}>
                <Grid container spacing={1}>
                    <StaticField label={'Закрывающие документы'}>{documentFiles.length === 0 ? '-' : renderFiles(documentFiles)}</StaticField>
                    <StaticField label={'Дата получения оригиналов документов'}>{Data.extractDate(invoice.PROPERTY_VALUES.DOCUMENTS_RECEIVE_DATE) || '-'}</StaticField>
                    <StaticField label={'Статус получения оригиналов документов'}>{documentStatusId ? documentStatuses.byId[documentStatusId].TITLE: '-'}</StaticField>
                </Grid>
            </Box>
            <Box my={4}>
                <Grid container spacing={1}>
                    <StaticField label={'Комментарий инженера'}>{invoice.PROPERTY_VALUES.E_COMMENT || '-'}</StaticField>
                    <StaticField label={'Комментарий руководителя'}>{invoice.PROPERTY_VALUES.D_COMMENT || '-'}</StaticField>
                </Grid>
            </Box>
            {invoiceStatusId === 10 && (
                <Box my={4}>
                    <Grid container spacing={1}>
                        <StaticField label={'Причина отклонения'}>{invoice.PROPERTY_VALUES.DECLINE_REASON || '-'}</StaticField>
                        <StaticField label={'Отклонил'}>{Data.getUserDisplayName(users.byId[invoice.PROPERTY_VALUES.DECLINE_BY])}</StaticField>
                    </Grid>
                </Box>
            )}
        </div>

    )
}
export default connect(state => ({
    invoiceId: state.app.pageOptions.invoiceId,
    prevPage: state.app.prevPage,
    users: state.staticData.users,
}))(InvoiceDetail);