import React, {useContext} from 'react';
import {connect} from "react-redux";
import {SnackContext} from "root/contexts";

// Components
import InvoiceForm, {getDealId} from "components/desktop/forms/InvoiceForm";
import Back from "core/desktop/view/Back";
import Box from "@material-ui/core/Box";

// Actions
import {setPage} from "actions/app";

// Classes
import Invoice from "classes/Invoice";

function CreateInvoice(props) {
    const {invoiceType, orderId = '', prevPage, currentUser} = props;
    const snackManager = useContext(SnackContext);

    return (
        <div>
            <Box mb={2}>
                <Back to={prevPage}>Создание нового счета</Back>
            </Box>
            <InvoiceForm
                id={'invoice-create'}
                type={'create'}
                onSubmit={ (fields, statusId = 1) => {
                    return getDealId(fields, invoiceType)
                        .then( dealId => Invoice.create(fields, {invoiceType, statusId, currentUser, dealId}) );
                }}
                onSuccess={ statusId => {
                    const message =  statusId === 1 ? 'Счет добавлен' : 'Счет добавлен и согласован';
                    snackManager.showMessage(message, {variant: 'success'});
                    setPage(prevPage);
                }}
                onError={() => {
                    snackManager.showMessage('Произошла ошибка', {variant: 'error'});
                }}
                invoiceType={invoiceType}
                initialValues={{order: orderId}}
            />
        </div>
    )
}
export default connect(state => ({
    invoiceType: state.app.pageOptions.invoiceType,
    orderId: state.app.pageOptions.orderId,
    prevPage: state.app.prevPage,
    currentUser: state.app.currentUser
}))(CreateInvoice);