import React, {useContext, useState, useEffect} from 'react';
import {connect} from "react-redux";
import {invoiceStatuses, paymentStatuses} from "root/static";
import moment from "moment";
import {SnackContext} from "root/contexts";

// Components
import Typography from "@material-ui/core/Typography";
import MultiButton from "core/desktop/view/MultiButton";
import SimpleTable from "core/desktop/view/table/SimpleTable";
import Button from "core/desktop/view/Button";
import InvoiceFilter from "components/desktop/forms/InvoiceFilter";
import Box from "@material-ui/core/Box";
import PageLoader from "core/desktop/view/PageLoader";

// Classes
import Bitrix24 from "classes/Bitrix24";
import Data from "classes/Data";
import Database from "classes/Database";
import Invoice from "classes/Invoice";

// Actions
import {setPage} from "actions/app";

// Api functions
import {callMany, numberToMoney} from "core/Api";

// Constants
import {DATE_TIME_FORMAT, E_INVOICES} from "root/constants";
import langPhrases from "root/languages";


function InvoiceList(props) {
    const {currentRole, users, currentUser, initialValues = {}} = props;
    const snackManager = useContext(SnackContext);
    const [placement, {ID: dealId}] = Bitrix24.getPlacementInfo();

    const [data, setData] = useState({});
    const [status, setStatus] = useState('loading');

    const {invoices, deals} = data;

    useEffect(() => {
        loadData({
            ">=DATE_ACTIVE_FROM": initialValues['fromDate'] ?
                moment(initialValues['fromDate']).startOf("day").format():
                moment().startOf("month").startOf('day').format(),
            "<=DATE_ACTIVE_FROM": initialValues['toDate'] ?
                moment(initialValues['toDate']).endOf("day").format():
                moment().endOf("month").endOf('day').format(),
            "PROPERTY_DEAL_ID": placement === "CRM_DEAL_DETAIL_TAB" ? dealId : (initialValues['object'] ||  null),
            "PROPERTY_STATUS_ID": initialValues['status'] || null,
            "PROPERTY_CREATOR_ID": initialValues['creator'] || null,
        }).then( data => {
            setData(data);
            setStatus('ready');
        })
    }, []);

    if (status === 'loading') {
        return <PageLoader/>;
    }

    return (
        <>
            <InvoiceFilter
                initialValues={initialValues}
                onSubmit={ fields => {
                    const params = {
                        ">=DATE_ACTIVE_FROM": moment(fields['fromDate'].value).startOf("day").format(),
                        "<=DATE_ACTIVE_FROM": moment(fields['toDate'].value).endOf("day").format(),
                        "PROPERTY_DEAL_ID": placement === "CRM_DEAL_DETAIL_TAB" ? dealId : fields['object'].value,
                        "PROPERTY_STATUS_ID": fields['status'] ? fields['status'].value : null,
                        "PROPERTY_CREATOR_ID": fields['creator'] ? fields['creator'].value : null
                    };
                    return loadData(params).then( data => setData(data) );
                }}
            />
            <Box mt={4}>
                <div className={'title-with-button'}>
                    <Typography variant={'h2'}>Счета</Typography>
                    <MultiButton
                        actions={[
                            {title: 'с заявкой', onClick: () => setPage(7, {invoiceType: 1})},
                            {title: 'без заявки', onClick: () => setPage(7, {invoiceType: 2})},
                        ]}
                        color={'secondary'}
                    >Создать счет</MultiButton>
                </div>
                <SimpleTable
                    id={'invoices'}
                    items={invoices}
                    settings={{
                        useSelect: Bitrix24.isAdmin() || currentRole === 1,
                        orderBy: 2,
                        notFound: 'Счетов нет',
                        useContextMenu: true,
                        perPage: 20,
                        perPageOptions: [20,50,100, langPhrases['all']],
                        useRowContextMenu: (invoice) => {
                            return +invoice.PROPERTY_VALUES.CREATOR_ID === currentUser || Bitrix24.isAdmin();
                        }
                    }}
                    columns={[
                        {id: 1, text: '№ счета'},
                        {id: 2, text: '№ заявки'},
                        {id: 3, text: 'Дата создания', width: 150},
                        {id: 4, text: 'Объект', isHidden: placement === 'CRM_DEAL_DETAIL_TAB'},
                        {id: 5, text: 'Создатель', width: 150},
                        {id: 6, text: 'Назначение платежа', sortable: false, width: 200},
                        {id: 7, text: 'Статус счета'},
                        {id: 8, text: 'Сумма счета', measure: 'руб.', subText: 'Оплачено / Всего', width: 200},
                        {id: 9, text: 'Статус оплаты'},
                        {id: 10, text: 'Действие', sortable: false},
                    ]}
                    getCellValue={(id, invoice, updateRow) => {
                        const data = {
                            users,
                            currentUser,
                            currentRole,
                            deals,
                        };
                        return getCellValue(id, invoice, updateRow, data)
                    }}
                    contextMenuActions={[
                        {
                            id: 1,
                            title: 'редактировать',
                            isHidden: (item) => {
                                return [1,10].indexOf(+item.PROPERTY_VALUES.STATUS_ID) === -1
                            },
                            handler: item => {
                                setPage(8, {invoiceId: item.ID});
                                return Promise.resolve();
                            }
                        },
                        {
                            id: 2,
                            title: 'удалить',
                            type: 'delete',
                            handler: item => {
                                return Invoice.delete(item.ID)
                                    .then(
                                        () => snackManager.showMessage('Счет удален', {variant: 'success'}),
                                        () => snackManager.showMessage('Произошла ошибка', {variant: 'error'})
                                    );
                            }
                        }
                    ]}
                    actions={[
                        {
                            id: 1,
                            title: 'удалить',
                            type: 'delete',
                            handler: ids => Invoice.deleteMany(
                                ids,
                                message => snackManager.showMessage(message, {variant: 'success'}),
                                message => snackManager.showMessage(message, {variant: 'error'})
                            )
                        }
                    ]}
                />
            </Box>
            {invoices.length > 0 && (
                <Box mt={2}>
                    <Box mb={1}><Typography variant={'h2'}>Суммарно</Typography></Box>
                    <div><Typography variant={'body2'}>Счетов: {invoices.length}</Typography></div>
                    <div><Typography variant={'body2'}>Общая сумма счетов: {Data.calcInvoiceSum(invoices)}</Typography></div>
                </Box>
            )}
        </>

    )
}
export default connect(state => ({
    users: state.staticData.users,
    currentRole: state.app.currentRole,
    currentUser: state.app.currentUser,
    initialValues: state.ui['invoiceFilter'] || {}
}))(InvoiceList);

function getCellValue(id, invoice, updateRow, data) {
    const {users, currentUser, currentRole, deals} = data;

    const statusId = +invoice.PROPERTY_VALUES.STATUS_ID;
    const orderId = +invoice.PROPERTY_VALUES.ORDER_ID;
    const invoiceId = +invoice.ID;

    switch (id) {
        case 1: return {
            display: <a onClick={() => setPage(9, {invoiceId})} title={'просмотр счета'}>{invoiceId}</a>,
            source: invoiceId
        };
        case 2: return +invoice.PROPERTY_VALUES.INVOICE_TYPE_ID === 1 ? {
            display: <a onClick={() => setPage(6, {orderId})} title={'просмотр заявки'}>{orderId}</a>,
            source: orderId
        } : '-';
        case 3:
            const date = moment(invoice['DATE_ACTIVE_FROM']);
            return {display: date.format(DATE_TIME_FORMAT), source: date.valueOf()};
        case 4:
            const deal = deals.byId[invoice.PROPERTY_VALUES.DEAL_ID];
            return {
                display: Bitrix24.getDealLink( deal ),
                source: Bitrix24.getDealName( deal ),
            };
        case 5: return Data.getUserDisplayName(users.byId[invoice.PROPERTY_VALUES.CREATOR_ID]);
        case 6: return invoice.PROPERTY_VALUES.PAYMENT_PURPOSE;
        case 7:
            let color = "#54a3d8";
            switch (statusId) {
                case 2: color = "#4ac9dc"; break;
                case 3: color = "#5dd0d4"; break;
                case 4: color = "#4cbabd"; break;
                case 5: color = "#58b99d"; break;
                case 6: color = "#5578d6"; break;
                case 7: color = "#a99d42"; break;
                case 8: color = "#b9904f"; break;
                case 9: color = "#5ca968"; break;
                case 10: color = "#d85353"; break;
            }
            return {
                display: <span className={'highlight'} style={{background: color}}>{invoiceStatuses.byId[statusId].TITLE}</span>,
                source: invoiceStatuses.byId[statusId].TITLE
            };
        case 8:
            return {
                display: `${numberToMoney(invoice.PROPERTY_VALUES.PAYED_SUM)} / ${numberToMoney(invoice.PROPERTY_VALUES.SUM)}`,
                source: parseFloat(invoice.PROPERTY_VALUES.SUM + "")
            };
        case 9: return paymentStatuses.byId[invoice.PROPERTY_VALUES.PAYED_STATUS].TITLE;
        case 10:
            const roleId = Data.getRoleIdByInvoiceStatus(statusId);
            const isAuthorAction = currentUser === +invoice.PROPERTY_VALUES.CREATOR_ID && (statusId < 2 || statusId > 4 && statusId !== 7);

            return (roleId === currentRole || isAuthorAction) && statusId !== 9 ?
                <InvoiceApproveButton
                    invoice={invoice}
                    currentRole={currentRole}
                    updateTableRow={updateRow}
                />: "-";
    }
}

function InvoiceApproveButton(props) {
    const {invoice, currentRole, updateTableRow} = props;
    const snackManager = useContext(SnackContext);

    const statusId = +invoice.PROPERTY_VALUES.STATUS_ID;
    const invoiceType = +invoice.PROPERTY_VALUES.INVOICE_TYPE_ID;

    const [isLoading, setLoading] = useState(false);

    if (statusId === 1 || statusId === 10) {
        const highRole = [1,2].indexOf(currentRole) !== -1;
        const nextStatus = (invoiceType === 2 && highRole) || highRole ? 3 : 2;

        return (
            <Button
                size={"small"}
                color={'secondary'}
                isLoading={isLoading}
                onClick={ () => {
                    setLoading(true);
                    return Invoice.createTaskByStage(invoice, nextStatus)
                        .then( taskId => {
                            return Database.updateItem(E_INVOICES, {
                                ID: invoice.ID,
                                PROPERTY_VALUES: {
                                    STATUS_ID: nextStatus,
                                    TASK_ID: taskId
                                }
                            })
                        }).then( () => {
                            updateTableRow(invoice.ID, {
                                PROPERTY_VALUES: {
                                    STATUS_ID: nextStatus
                                }
                            });
                            snackManager.showMessage(`Счет № ${invoice.ID} отправлен на согласование`, {variant: 'success'});
                        }).catch( error => {
                            console.log(error);
                            snackManager.showMessage('Произошла ошибка', {variant: 'error'})
                        }).finally( () => {
                            setLoading(false);
                        });
                }}
            >Согласовать</Button>
        );
    }

    let buttonTitle = "Утвердить";
    switch (statusId) {
        case 4: buttonTitle = "Оплатить"; break;
        case 5: buttonTitle = "Запланировать"; break;
        case 6: buttonTitle = "Проконтролировать"; break;
        case 7: buttonTitle = "Собрать документы"; break;
        case 8: buttonTitle = "Получить документы"; break;
    }

    return (
        <Button
            color={'secondary'}
            size={"small"}
            onClick={() => setPage(10, {invoiceId: invoice.ID})}
        >{buttonTitle}</Button>
    );
}

function loadData(params) {
    return callMany({
        invoices: {handler: () => Database.loadInvoices(params) },
        deals: {
            handler: ({invoices}) => {
                const dealIds = invoices.map( i => i.PROPERTY_VALUES.DEAL_ID );
                return Bitrix24.getDealsByIds(dealIds);
            }
        }
    })
}