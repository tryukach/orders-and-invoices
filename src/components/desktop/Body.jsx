import React from "react";
import {connect} from "react-redux";

// Pages
import Main from "components/desktop/pages/Main";
import CreateOrder from "components/desktop/pages/CreateOrder";
import EditOrder from "components/desktop/pages/EditOrder";
import OrderDetail from "components/desktop/pages/OrderDetail";
import InvoiceList from "components/desktop/pages/InvoiceList";
import CreateInvoice from "components/desktop/pages/CreateInvoice";
import InvoiceEdit from "components/desktop/pages/InvoiceEdit";
import InvoiceDetail from "components/desktop/pages/InvoiceDetail";
import ApproveInvoice from "components/desktop/pages/ApproveInvoice";
import Roles from "components/desktop/pages/Roles";

// Components
import LockLoader from "core/desktop/view/LockLoader";
import Fade from '@material-ui/core/Fade';
import Message from "core/desktop/view/Message";
import Header from "./Header";

function AppBody(props) {
    const {currentPage, message, lock} = props;
    const PageComponent = getPageComponent(currentPage);
    
    return (
        <Fade in={true} timeout={1000}>
            {lock ?
                <LockLoader size={'medium'}>{lock}</LockLoader>:
                <div className={'app'}>
                    <Header showContacts={false}/>
                    <main>
                        {message.text && <Message {...message.options}>{message.text}</Message>}
                        <PageComponent/>
                    </main>
                </div>
            }
        </Fade>
    );
}
export default connect( state => {
    return {
        currentPage: state.app.currentPage,
        message: state.app.message,
        lock: state.app.lock
    }
})(AppBody);

function getPageComponent(page) {
    switch (page) {
        case 1: return Main;
        case 2: return InvoiceList;
        case 3: return Roles;
        case 4: return CreateOrder;
        case 5: return EditOrder;
        case 6: return OrderDetail;
        case 7: return CreateInvoice;
        case 8: return InvoiceEdit;
        case 9: return InvoiceDetail;
        case 10: return ApproveInvoice;
        default: throw new Error('page is not exist');
    }
}