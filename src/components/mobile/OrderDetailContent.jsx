import React, {useState, useEffect} from 'react';
import {connect} from "react-redux";
import {orderStatuses, measures, invoiceStatuses, paymentStatuses} from "root/static";
import moment from "moment";

// Components
import Box from "@material-ui/core/Box";
import PageLoader from "core/desktop/view/PageLoader";
import Grid from "@material-ui/core/Grid";
import {renderFiles} from "core/mobile/view/form/fields/file/FileLoader";
import TableCard from "components/mobile/TableCard";
import Typography from "@material-ui/core/Typography";
import Button from "core/desktop/view/Button";
import StaticField from "components/mobile/ui/StaticField";

// Classes
import Database from "classes/Database";
import Data from "classes/Data";
import Bitrix24 from "classes/Bitrix24";

// Api functions
import {callMany, numberToMoney} from "core/Api";

// Constants
import {DATE_TIME_FORMAT, OBJECT_RESPONSIBLE_KEY} from "root/constants";

// Actions
import {setPage} from "actions/app";

function OrderDetailContent(props) {
    const {orderId, users} = props;
    const [placement] = Bitrix24.getPlacementInfo();

    const [status, setStatus] = useState('loading');
    const [order, setOrder] = useState(null);
    const [deal, setDeal] = useState({});
    const [data, setData] = useState({});

    useEffect(() => {
        callMany({
            order: {handler: () => Database.loadOrder(orderId)},
            orderData: {handler: () => Database.getOrderRelatedData(orderId)},
            deal: {handler: ({order}) => Bitrix24.getDeal(order.PROPERTY_VALUES.DEAL_ID) }
        }).then( response => {
            const {order, orderData, deal} = response;

            setOrder(order);
            setData(orderData);
            setDeal(deal);
            setStatus('ready');
        })
    }, []);

    if (status === 'loading') {
        return <PageLoader/>;
    }

    if (order === null) {
        return <div>Заявка № {orderId} отсутствует</div>;
    }

    const {files = [], products = [], invoices = []} = data;
    const responsibleId = deal ? deal[OBJECT_RESPONSIBLE_KEY] : null;
    const responsibleUser = deal ? users.byId[responsibleId] : null;

    return (
        <>
            <Grid container spacing={1}>
                <StaticField label={'Создатель'}>{Data.getUserDisplayName(users.byId[order.PROPERTY_VALUES.CREATOR_ID])}</StaticField>
                <StaticField label={'Статус'}>{orderStatuses.byId[order.PROPERTY_VALUES.STATUS_ID].TITLE}</StaticField>
                {placement !== "CRM_DEAL_DETAIL_TAB" && <StaticField label={'Объект'}>{Bitrix24.getDealName(deal)}</StaticField>}
                <StaticField label={'Ответственный за объект'}>{responsibleUser ? Data.getUserDisplayName(responsibleUser) : '-'}</StaticField>
                <StaticField label={'Описание заявки'}>{order.DETAIL_TEXT}</StaticField>
                <StaticField label={'Файл-приложение к заявке'}>{files.length === 0 ? '-' : renderFiles(files)}</StaticField>
                <StaticField label={'Комментарий'}>{order.PROPERTY_VALUES.COMMENT || '-'}</StaticField>
                <StaticField label={'Товары по заявке'} width={500}>
                    {products.length > 0 ?
                        <TableCard
                            id={'order-products'}
                            items={products}
                            settings={{
                                orderBy: 1,
                                usePagination: false,
                                gridSize: {
                                    xs: 12,
                                    sm: 6
                                }
                            }}
                            columns={[
                                {id: 1, text: 'Название товара'},
                                {id: 2, text: 'Количество', measure: 'шт.'},
                                {id: 3, text: 'Мера'},
                                {id: 4, text: 'Комментарий', sortable: false},
                            ]}
                            getCellValue={getProductCellValue}
                        />:
                        "-"
                    }
                </StaticField>
            </Grid>
            <Box mt={4}>
                <div className={'title-with-button'}>
                    <Typography variant={'h2'}>Счета</Typography>
                    <Button
                        onClick={() => setPage(7, {invoiceType: 1, orderId})}
                        color={'secondary'}
                    >Создать счет</Button>
                </div>
                <TableCard
                    id={'order-invoices'}
                    items={invoices}
                    settings={{
                        orderBy: 1,
                        usePagination: false
                    }}
                    columns={[
                        {id: 1, text: '№ счета'},
                        {id: 2, text: 'Дата создания'},
                        {id: 3, text: 'Создатель'},
                        {id: 4, text: 'Назначение платежа', sortable: false},
                        {id: 5, text: 'Статус счета'},
                        {id: 6, text: 'Сумма счета', measure: 'руб.', subText: 'Оплачено / Всего'},
                        {id: 7, text: 'Статус оплаты'},
                    ]}
                    getCellValue={(id, invoice) => getInvoiceCellValue(id, invoice, {users})}
                />
            </Box>
        </>

    )
}
export default connect( state => ({
    users: state.staticData.users,
    currentRole: state.app.currentRole
}))(OrderDetailContent);

function getProductCellValue(id, product) {
    switch (id) {
        case 1: return product.NAME;
        case 2: return +product.PROPERTY_VALUES.COUNT;
        case 3: return measures.byId[product.PROPERTY_VALUES.MEASURE_ID].TITLE;
        case 4: return product.PROPERTY_VALUES.COMMENT;
    }
}

function getInvoiceCellValue(id, invoice, data) {
    const {users} = data;
    const statusId = +invoice.PROPERTY_VALUES.STATUS_ID;

    switch (id) {
        case 1: return {
            display: <a onClick={() => setPage(9, {invoiceId: invoice.ID})} title={'просмотр счета'}>{invoice.ID}</a>,
            source: +invoice.ID
        };
        case 2:
            const date = moment(invoice['DATE_CREATE']);
            return {display: date.format(DATE_TIME_FORMAT), source: date.valueOf()};
        case 3: return Data.getUserDisplayName(users.byId[invoice.PROPERTY_VALUES.CREATOR_ID]);
        case 4: return invoice.PROPERTY_VALUES.PAYMENT_PURPOSE;
        case 5:
            let color = "#54a3d8";
            switch (statusId) {
                case 2: color = "#4ac9dc"; break;
                case 3: color = "#5dd0d4"; break;
                case 4: color = "#4cbabd"; break;
                case 5: color = "#58b99d"; break;
                case 6: color = "#5578d6"; break;
                case 7: color = "#a99d42"; break;
                case 8: color = "#b9904f"; break;
                case 9: color = "#5ca968"; break;
                case 10: color = "#d85353"; break;
            }
            return {
                display: <span className={'highlight'} style={{background: color}}>{invoiceStatuses.byId[statusId].TITLE}</span>,
                source: invoiceStatuses.byId[statusId].TITLE
            };
        case 6: return {
            display: `${numberToMoney(invoice.PROPERTY_VALUES.PAYED_SUM)} / ${numberToMoney(invoice.PROPERTY_VALUES.SUM)}`,
            source: parseFloat(invoice.PROPERTY_VALUES.SUM + "")
        };
        case 7: return paymentStatuses.byId[invoice.PROPERTY_VALUES.PAYED_STATUS].TITLE;
    }
}