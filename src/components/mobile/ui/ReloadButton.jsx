import React from 'react';
import {connect} from "react-redux";

// Actions
import {initStaticData} from "actions/staticData";
import {initApp, lock} from "actions/app";

// Classes
import App from "classes/App";

// Components
import Button from "core/desktop/view/Button";
import Icon from "core/desktop/view/Icon";
import Box from "@material-ui/core/Box";

function ReloadButton(props) {
    const {constAppState, disabled} = props;
    return (
        <Box mb={2}>
            <Button
                disabled={disabled}
                size={'small'}
                variant={'outlined'}
                onClick={() => {
                    lock('Обновляем приложение...');
                    App.reloadData()
                        .then( state => {
                            initApp(Object.assign({}, state.app, constAppState));
                            initStaticData(state.staticData);
                            lock(false);
                        })
                }}
            >
                <Box display={'flex'} alignItems={'center'}>
                    <Box mr={1} display={'flex'}><Icon fontSize={20}>refresh</Icon></Box>
                    <span>Обновить данные</span>
                </Box>
            </Button>
        </Box>
    )
}
export default connect(state => ({
    constAppState: {
        currentPage: state.app.currentPage,
        pageOptions: state.app.pageOptions,
        prevPage: state.app.prevPage
    }
}))(ReloadButton);