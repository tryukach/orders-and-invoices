import React from 'react';

// Classes
import Bitrix24 from "classes/Bitrix24";

// Components
import DynamicSelect from "core/desktop/view/form/fields/select/DynamicSelect";

export default function DealSelect(props) {
    const {settings = {}, ...selectProps} = props;

    return (
        <DynamicSelect
            {...selectProps}
            settings={settings}
            loadPackage={(filterParams = {}, packageNumber) => {
                return Bitrix24.loadPackage('crm.deal.list', {filter: filterParams, select: ["*", "UF_*"]}, packageNumber);
            }}
            getSearchItems={Bitrix24.searchDeals}
            getText={ item => item.TITLE }
        />
    )
}