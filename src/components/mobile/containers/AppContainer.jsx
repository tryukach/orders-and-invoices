import React from "react";
import Theme from "core/mobile/Theme";
import CssBaseline from '@material-ui/core/CssBaseline';
import ReactResizeDetector from 'react-resize-detector';

// Api functions
import {resizeWindow} from "core/Api";

// Components
import ErrorBoundary from "core/ErrorBoundary";
import WithSnack from "core/desktop/view/WithSnack";
import BodyContainer from "components/mobile/containers/BodyContainer";

function AppContainer(props) {
    const {renderBody} = props;

    return (
        <Theme>
            <CssBaseline />
                <div className={'app-container'}>
                    <ReactResizeDetector
                        handleWidth
                        handleHeight
                        refreshMode={'throttle'}
                        refreshRate={500}
                        onResize={() => {
                            resizeWindow();
                        }}
                    />
                    <WithSnack>
                        <ErrorBoundary>
                            <BodyContainer>{renderBody()}</BodyContainer>
                        </ErrorBoundary>
                    </WithSnack>
                </div>
        </Theme>
    )
}
export default AppContainer;