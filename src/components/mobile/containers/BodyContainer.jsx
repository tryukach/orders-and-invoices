import React, {useState, useEffect} from 'react';
import {connect} from "react-redux";

// Classes
import Database from "classes/Database";
import Bitrix24 from "classes/Bitrix24";

// Actions
import {setPage} from "actions/app";

// Api functions
import {callMany} from "core/Api";

// Constants
import {U_INVOICE_STATUS_ID_LC} from "root/constants";

// Components
import Message from "core/desktop/view/Message";

function BodyContainer(props) {
    const {currentUser} = props;
    const [message, setMessage] = useState(null);
    const [status, setStatus] = useState(null);

    useEffect(() => {
        const [, options] = Bitrix24.getPlacementInfo();
        const {action, id, orderId} = options;

        if (action) {
            callMany({
                invoice: {handler: () => Database.loadInvoice(id)},
                task: {handler: ({invoice}) => Bitrix24.getTask(invoice.PROPERTY_VALUES.TASK_ID)}
            }).then( response => {
                const {invoice, task} = response;

                const owners = Array.isArray(task['accomplices']) ?
                    task['accomplices'].concat(task['responsibleId']): [task['responsibleId']];

                if (owners.indexOf(currentUser.toString()) === -1) {
                    setMessage('Вы не являетесь ответственным по этому делу');
                }
                else if (task[`uf${U_INVOICE_STATUS_ID_LC}`] !== invoice.PROPERTY_VALUES.STATUS_ID) {
                    setMessage('Некорректная стадия утверждения');
                }
                else {
                    switch (action) {
                        case "invoice_create": setPage(7, {invoiceType: 1, orderId}); break;
                        case "invoice_show": setPage(9, {invoiceId: id}); break;
                        case "invoice_approve": setPage(10, {invoiceId: id}); break;
                    }
                }
                setStatus('ready');
            });
        } else {
            setStatus('ready');
        }
    }, []);

    if (status !== 'ready') {
        return null;
    }
    return message ? <Message type={'warning'}>{message}</Message> : props.children;
}
export default connect(state => ({currentUser: state.app.currentUser}))(BodyContainer);