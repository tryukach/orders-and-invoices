import React from 'react';
import { Provider } from 'react-redux';

export default function (props) {
    const {store, renderContainer} = props;
    return (
        <Provider store={store}>
            {renderContainer()}
        </Provider>
    );
}