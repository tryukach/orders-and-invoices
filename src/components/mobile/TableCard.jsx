import React, {useContext, useMemo} from 'react';
import {TableContext} from "root/contexts";
import langPhrases from "core/languages";

// Components
import TableWrapper from "core/desktop/view/table/TableWrapper";
import Paper from "@material-ui/core/Paper";
import MuiTable from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableRow from "@material-ui/core/TableRow";
import Grid from '@material-ui/core/Grid';
import Typography from "@material-ui/core/Typography";
import SimpleSelect from "core/desktop/view/form/fields/select/SimpleSelect";
import Tooltip from "core/desktop/view/Tooltip";
import Button from "core/desktop/view/Button";
import Icon from "core/desktop/view/Icon";
import Box from "@material-ui/core/Box";

// Api functions
import {
    filterColumns,
    sortRows,
    getCellHeadText,
    getCellText,
    getPageItems
} from "core/desktop/view/table/TableWrapper";
import {getGridSize} from "core/Api";


function TableCardContainer(props) {
    const {items = [], settings = {}, getCellValue} = props;

    const table = useContext(TableContext);
    const state = table.getState();

    useMemo(() => {
        const sortedItems = sortRows(items, state.order, item => getCellValue(state.orderBy, item));
        table.setRows(sortedItems);
        table.render();
    }, [items.length]);

    const cards = table.getRows();

    if (cards.length === 0) {
        return <Typography variant={'body1'} color={'textSecondary'}>{settings.notFound}</Typography>
    }

    return (
        <TableCard
            cards={cards}
            settings={settings}
            columns={props.columns}
            getCellValue={getCellValue}
            getPageCards={ items => {
                return settings.usePagination && typeof state.perPage !== "string" ?
                    items.slice( (state.currentPage - 1) * state.perPage, state.currentPage * state.perPage):
                    items;
            }}
            setPage={ page => table.setPage(page) }
            getAll={() => Promise.resolve()}
            cardsTotal={cards.length}
        />
    )
}
export default TableWrapper(TableCardContainer);


function TableCard(props) {
    const {cards, columns = [], settings, getPageCards, getCellValue, cardsTotal} = props;
    const cardsByPage = getPageCards(cards);

    const filteredColumns = filterColumns(columns);
    const size = getGridSize(settings.gridSize);

    return (
        <div className={'table-card-container'} >
            <Grid container spacing={2}>
                {cardsByPage.map( card => {
                    const ID = card.ID || card.id;
                    return (
                        <Grid item {...size} key={ID}>
                            <Paper elevation={2} square={false}>
                                <MuiTable>
                                    <Head
                                        card={card}
                                        column={filteredColumns[0]}
                                        getCellValue={getCellValue}
                                    />
                                    <Body
                                        card={card}
                                        columns={filteredColumns.slice(1)}
                                        getCellValue={getCellValue}
                                    />
                                </MuiTable>
                            </Paper>
                        </Grid>
                    )
                })}
            </Grid>
            <Footer
                setPage={props['setPage']}
                usePagination={settings.usePagination}
                perPageOptions={settings.perPageOptions}
                count={cardsTotal}
                getAll={props['getAll']}
            />
        </div>
    )
}

function Head(props) {
    const {column, getCellValue, card} = props;
    const cellValue = getCellValue(column.id, card);
    const cellText = getCellText(cellValue);

    return (
        <TableHead>
            <TableRow>
                <TableCell variant={'head'}>{getCellHeadText(column)}</TableCell>
                <TableCell variant={'head'}>{cellText}</TableCell>
            </TableRow>
        </TableHead>
    )
}

function Body(props) {
    const {columns, getCellValue, card} = props;
    const ID = card.ID || card.id;

    return (
        <TableBody>
            {columns.map( column => {
                const cellValue = getCellValue(column.id, card);
                const cellText = getCellText(cellValue);
                const key = `${ID}-${column.id}`;

                return (
                    <TableRow key={key}>
                        <TableCell variant={'body'}>{getCellHeadText(column)}</TableCell>
                        <TableCell variant={'body'}>{cellText}</TableCell>
                    </TableRow>
                )
            })}
        </TableBody>
    )
}

function Footer(props) {
    const {
        count,
        getAll,
        usePagination,
        perPageOptions,
        setPage
    } = props;


    const table = useContext(TableContext);
    const {currentPage, perPage} = table.getState();

    const pagesCount = Math.ceil(count / perPage);
    const to = currentPage * perPage;
    const from = to - perPage + 1;
    const pageItems = useMemo(() => getPageItems(perPageOptions), [perPageOptions.length]);

    return usePagination && (
        <Box display={'flex'} justifyContent={'flex-end'} mt={2}>
            <div className={'pagination d-flex align-items-center'}>
                <div className={'page-switcher d-flex align-items-center'}>
                    <SimpleSelect
                        value={perPage}
                        items={pageItems}
                        settings={{
                            useUnderline: true,
                            asPlainText: true
                        }}
                        getText={item => item.TITLE}
                        onChange={ ID => {
                            const item = pageItems.find( i => i.ID === ID );
                            if (item.TITLE === langPhrases['all']) {
                                getAll().then( () => table.setPerPage(item.TITLE) )
                            }
                            else {
                                table.setPerPage(item.TITLE);
                            }
                        }}
                    />
                </div>
                {typeof perPage !== "string" && (
                    <div className={'info'}>
                        <Typography variant={'body2'}>{from}-{to} из {count}</Typography>
                    </div>
                )}
                <div className={'page-links d-flex'}>
                    <div  className={'page-link page-prev'}>
                        <Tooltip title={langPhrases['prev']}>
                            <Button
                                size={'small'}
                                color={'primary'}
                                disabled={currentPage === 1 || perPage === langPhrases['all']}
                                onClick={() => {
                                    if (currentPage > 1) {
                                        setPage(currentPage - 1);
                                    }
                                }}
                            ><Icon>navigate_before</Icon></Button>
                        </Tooltip>
                    </div>
                    <div className={'page-link page-next'}>
                        <Tooltip title={langPhrases['next']}>
                            <Button
                                size={'small'}
                                color={'primary'}
                                disabled={currentPage >= pagesCount || perPage === langPhrases['all']}
                                onClick={() => {
                                    if (currentPage < pagesCount) {
                                        setPage(currentPage + 1);
                                    }
                                }}
                            ><Icon>navigate_next</Icon></Button>
                        </Tooltip>
                    </div>
                </div>
            </div>
        </Box>
    )
}