import React, {useState, useContext} from 'react';
import {SnackContext} from "root/contexts";
import moment from "moment";
import {deliveryStatusList, documentStatusesList, paymentList, paymentTypes} from "root/static";

// Constants
import {INPUT_DATE_FORMAT} from "root/constants";

// Classes
import Invoice from "classes/Invoice";
import Data from "classes/Data";

// Actions
import {setPage} from "actions/app";

// Api functions
import {formatMoney, numberToMoney} from "core/Api";

// Components
import Form from "core/desktop/view/form/Form";
import Field from "core/desktop/view/form/Field";
import SimpleSelect from "core/desktop/view/form/fields/select/SimpleSelect";
import Input from "core/desktop/view/form/fields/Input";
import TextArea from "core/desktop/view/form/fields/TextArea";
import FileLoader from "core/mobile/view/form/fields/file/FileLoader";
import Button from "core/desktop/view/Button";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import StaticField from "components/mobile/ui/StaticField";
import Popup from "core/desktop/view/Popup";
import DeclineInvoiceForm from "components/mobile/forms/DeclineInvoiceForm";
import FormHelperText from "@material-ui/core/FormHelperText";

export default function ApproveInvoiceForm(props) {
    const {invoice} = props;
    const snackManager = useContext(SnackContext);

    const statusId = +invoice.PROPERTY_VALUES.STATUS_ID;
    const labels = Invoice.getLabelsByStage(invoice, statusId);

    return (
        <Form
            id={'invoice-approve'}
            settings={{
                fieldGrid: 3,
                fieldWidth: 300,
                labelWidth: 225
            }}
            onSubmit={ fields => Invoice.approve(fields, invoice) }
            onSuccess={ nextStatusId => {
                const currentStatusId = +invoice.PROPERTY_VALUES.STATUS_ID;

                nextStatusId === currentStatusId ?
                    snackManager.showMessage(`Счет № ${invoice.ID} успешно сохранен`, {variant: 'success'}):
                    nextStatusId === 9 ?
                        snackManager.showMessage(`Счет № ${invoice.ID} завершен`, {variant: 'success'}):
                        snackManager.showMessage(`Счет № ${invoice.ID} переведен на следующий статус`, {variant: 'success'});

                setPage(2);
            }}
            onError={ error => {
                console.log(error);
                snackManager.showMessage('Произошла ошибка', {variant: 'error'});
            }}
        >
            {(form, settings, isSubmitting) => {
                return (
                    <div>
                        <Grid container spacing={2}>
                            {getDynamicFields(invoice, form, isSubmitting)}
                        </Grid>
                        <Box mt={4}>
                            <div className={'form-buttons'}>
                                <Button
                                    type={'submit'}
                                    isLoading={isSubmitting}
                                    className={'btn-success'}
                                >{labels['actionButtonTitle']}</Button>
                                {[2,3,4].indexOf(statusId) !== -1 && (
                                    <DeclineButton
                                        className={'btn-error'}
                                        disabled={isSubmitting}
                                        invoiceId={invoice.ID}
                                    >Отклонить</DeclineButton>
                                )}
                            </div>
                        </Box>
                    </div>
                )
            }}
        </Form>
    )
}

function DeclineButton(props) {
    const {invoiceId, ...buttonProps} = props;
    const [open, setOpen] = useState(false);
    return (
        <>
            <div data-popup={'decline-invoice'}>
                <Button
                    {...buttonProps}
                    onClick={() => setOpen(true)}
                >Отклонить</Button>
            </div>
            <Popup
                id={'decline-invoice'}
                open={open}
                onClose={() => setOpen(false)}
            >
                <DeclineInvoiceForm invoiceId={invoiceId}/>
            </Popup>
        </>
    )
}

export function getStaticFields(statusId, invoice) {
    switch (statusId) {
        case 3: return <StaticField label={'Комментарий инженера'}>{invoice.PROPERTY_VALUES.E_COMMENT || '-'}</StaticField>;
        case 4:
            return (
                <>
                    <StaticField label={'Тип оплаты'}><span className={'highlight'} style={{background: "#535c69"}}>{paymentTypes.byId[invoice.PROPERTY_VALUES.PAYMENT_TYPE_ID].TITLE}</span></StaticField>
                    <StaticField label={'Сумма оплаты'}><span className={'highlight'} style={{background: "#535c69"}}>{formatMoney(invoice.PROPERTY_VALUES.CURRENT_PAYMENT_SUM)}</span></StaticField>
                    <StaticField label={'Комментарий руководителя'}>{invoice.PROPERTY_VALUES.D_COMMENT || '-'}</StaticField>
                </>
            );
        case 6: return <StaticField label={'Дата планируемой поставки'}>{Data.extractDate(invoice.PROPERTY_VALUES.DELIVERY_DATE) || '-'}</StaticField>;
        case 7: return <StaticField label={'Дата поставки'}>{Data.extractDate(invoice.PROPERTY_VALUES.DELIVERY_DATE) || '-'}</StaticField>;
        default: return;
    }
}

export function getDynamicData(statusId, invoice) {
    switch (statusId) {
        case 4: return getStaticFields(3, invoice);
        case 5: return (
            <React.Fragment>
                {getDynamicData(4, invoice)}
                <StaticField label={'Комментарий руководителя'}>{invoice.PROPERTY_VALUES.D_COMMENT || '-'}</StaticField>
            </React.Fragment>
        );
        case 6: case 7: return getDynamicData(5, invoice);
    }
}

export function getDynamicFields(invoice, form, isSubmitting) {
    const fields = form.getFields();

    const statusId = +invoice.PROPERTY_VALUES.STATUS_ID;
    const toDay = moment();

    switch (statusId) {
        case 2: return (
            <Grid item xs={12}>
                <Field
                    name={'commentE'}
                    defaultValue={''}
                    label={'Комментарий инженера'}
                >
                    {inputProps => <TextArea {...inputProps}/>}
                </Field>
            </Grid>
        );
        case 3:
            const restPaymentSum = Invoice.getRestPaymentSum(invoice);
            return (
                <>
                    <Grid item xs={12}>
                        <Field
                            name={'paymentType'}
                            defaultValue={1}
                            label={'Тип оплаты'}
                        >
                            {inputProps =>
                                <SimpleSelect
                                    {...inputProps}
                                    items={paymentList}
                                    getText={ item => item.TITLE }
                                />
                            }
                        </Field>
                    </Grid>
                    {fields['paymentType'] && fields['paymentType'].value === 2 && (
                        <Grid item xs={12}>
                            <Field
                                name={'paymentSum'}
                                isRequired={true}
                                defaultValue={numberToMoney(restPaymentSum)}
                                label={'Сумма частичной оплаты'}
                                format={formatMoney}
                            >
                                {inputProps => {
                                    const {error} = inputProps;
                                    return (
                                        <>
                                            <Input {...inputProps}/>
                                            {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
                                        </>
                                    )
                                }}
                            </Field>
                        </Grid>
                    )}
                    <Grid item xs={12}>
                        <Field
                            name={'commentD'}
                            defaultValue={''}
                            label={'Комментарий руководителя'}
                        >
                            {inputProps => <TextArea {...inputProps}/>}
                        </Field>
                    </Grid>
                </>
            );
        case 4:
            return (
                <>
                    <Grid item xs={12}>
                        <Field
                            name={'paymentDate'}
                            isRequired={true}
                            defaultValue={toDay.format(INPUT_DATE_FORMAT)}
                            label={'Дата оплаты счета'}
                        >
                            {inputProps => {
                                const {error} = inputProps;
                                return (
                                    <>
                                        <Input {...inputProps} type={'date'}/>
                                        {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
                                    </>
                                )
                            }}
                        </Field>
                    </Grid>
                    <Grid item xs={12}>
                        <Field
                            name={'ppFiles'}
                            isRequired={true}
                            defaultValue={[]}
                            label={'Файлы п/п'}
                        >
                            {(inputProps, fieldProps) => {
                                const {error} = inputProps;
                                return (
                                    <>
                                        <FileLoader
                                            {...inputProps}
                                            {...fieldProps}
                                            settings={{
                                                multiple: true
                                            }}
                                            disabled={isSubmitting}
                                        />
                                        {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
                                    </>
                                )
                            }}
                        </Field>
                    </Grid>
                    <Grid item xs={12}>
                        <Field
                            name={'ppSum'}
                            isRequired={true}
                            defaultValue={formatMoney(invoice.PROPERTY_VALUES.CURRENT_PAYMENT_SUM)}
                            label={'Сумма п/п'}
                        >
                            {inputProps => {
                                const {error} = inputProps;
                                return (
                                    <>
                                        <Input {...inputProps}/>
                                        {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
                                    </>
                                )
                            }}
                        </Field>
                    </Grid>
                </>
            );
        case 5: return (
            <Grid item xs={12}>
                <Field
                    name={'deliveryDate'}
                    defaultValue={toDay.format(INPUT_DATE_FORMAT)}
                    isRequired={true}
                    label={'Дата планируемой поставки'}
                >
                    {inputProps => {
                        const {error} = inputProps;
                        return (
                            <>
                                <Input {...inputProps} type={'date'}/>
                                {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
                            </>
                        )
                    }}
                </Field>
            </Grid>
        );
        case 6:
            const prevDeliveryDate = moment(invoice.PROPERTY_VALUES.DELIVERY_DATE);
            const initialDeliveryDate = prevDeliveryDate.isBefore(toDay) ? toDay : prevDeliveryDate;

            return (
                <>
                    <Grid item xs={12}>
                        <Field
                            name={'deliveryStatus'}
                            defaultValue={1}
                            label={'Статус поставки'}
                        >
                            {inputProps =>
                                <SimpleSelect
                                    {...inputProps}
                                    items={deliveryStatusList}
                                    getText={ item => item.TITLE }
                                />
                            }
                        </Field>
                    </Grid>
                    {fields['deliveryStatus'] && fields['deliveryStatus'].value === 2 && (
                        <Grid item xs={12}>
                            <Field
                                name={'deliveryDate'}
                                isRequired={true}
                                defaultValue={initialDeliveryDate.format(INPUT_DATE_FORMAT)}
                                label={'Новая дата планируемой поставки'}
                            >
                                {inputProps => {
                                    const {error} = inputProps;
                                    return (
                                        <>
                                            <Input {...inputProps} type={'date'}/>
                                            {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
                                        </>
                                    )
                                }}
                            </Field>
                        </Grid>
                    )}
                </>
            );
        case 7:
            const deliveryDate = moment(invoice.PROPERTY_VALUES.DELIVERY_DATE);
            const initialDocumentsReceiveDate = deliveryDate.isBefore(toDay) ? toDay : deliveryDate;
            return (
                <>
                    <Grid item xs={12}>
                        <Field
                            name={'closedDocuments'}
                            defaultValue={[]}
                            label={'Закрывающие документы'}
                        >
                            {(inputProps, fieldProps) =>
                                <FileLoader
                                    {...inputProps}
                                    {...fieldProps}
                                    settings={{
                                        multiple: true
                                    }}
                                    disabled={isSubmitting}
                                />
                            }
                        </Field>
                    </Grid>
                    <Grid item xs={12}>
                        <Field
                            name={'documentsReceiveDate'}
                            isRequired={true}
                            defaultValue={initialDocumentsReceiveDate.format(INPUT_DATE_FORMAT)}
                            label={'Дата получения оригинальных документов'}
                        >
                            {inputProps => {
                                const {error} = inputProps;
                                return (
                                    <>
                                        <Input {...inputProps} type={'date'}/>
                                        {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
                                    </>
                                )
                            }}
                        </Field>
                    </Grid>
                    <Grid item xs={12}>
                        <Field
                            name={'documentsReceiveStatus'}
                            defaultValue={1}
                            label={'Статус получения оригинальных документов'}
                        >
                            {inputProps =>
                                <SimpleSelect
                                    {...inputProps}
                                    items={documentStatusesList}
                                    getText={ item => item.TITLE }
                                />
                            }
                        </Field>
                    </Grid>
                </>
            );
        default: return <div>no case for this</div>;
    }
}