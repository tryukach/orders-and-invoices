import React from 'react';

// Classes
import Database from "classes/Database";
import Bitrix24 from "classes/Bitrix24";

// Components
import Form from "core/desktop/view/form/Form";
import Field from "core/desktop/view/form/Field";
import DynamicSelect from "core/desktop/view/form/fields/select/DynamicSelect";
import DealSelect from "components/mobile/ui/DealSelect";
import Input from "core/desktop/view/form/fields/Input";
import FileLoader from "core/mobile/view/form/fields/file/FileLoader";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Button from "core/desktop/view/Button";
import Icon from "core/desktop/view/Icon";
import FormHelperText from "@material-ui/core/FormHelperText";

// Constants
import {E_ORDERS} from "root/constants";

// Api functions
import {formatMoney} from "core/Api";
import {setPage} from "actions/app";


function InvoiceForm(props) {
    const {id, onSubmit, onSuccess, onError, initialValues = {}, invoiceType = 1, type} = props;
    const [placement, {ID: dealId}] = Bitrix24.getPlacementInfo();

    return (
        <Form
            id={id}
            settings={{
                labelWidth: 175,
                fieldGrid: 3,
                fieldWidth: 250
            }}
            onSubmit={onSubmit}
            onSuccess={onSuccess}
            onError={onError}
        >
            {(form, settings, isSubmitting) => {
                return (
                    <>
                        <Grid container spacing={2}>
                            {invoiceType === 1 ?
                                <Grid item xs={12}>
                                    <Field
                                        name={'order'}
                                        label={'Заявка'}
                                        isRequired={true}
                                        defaultValue={initialValues.order || ''}
                                    >
                                        {(inputProps, fieldProps) => {
                                            const {error} = inputProps;
                                            return (
                                                <>
                                                    <DynamicSelect
                                                        {...inputProps}
                                                        {...fieldProps}
                                                        loadPackage={(filterParams = {}, packageNumber) => {
                                                            return Bitrix24.loadPackage('entity.item.get', {
                                                                ENTITY: E_ORDERS,
                                                                filter: {
                                                                    PROPERTY_DEAL_ID: placement === 'CRM_DEAL_DETAIL_TAB' ? dealId : null,
                                                                    ...filterParams
                                                                }
                                                            }, packageNumber);
                                                        }}
                                                        getSearchItems={phrase => {
                                                            return Database.load(E_ORDERS, {filter: {"%NAME": phrase}});
                                                        }}
                                                        getText={ order => `Заявка № ${order.ID}` }
                                                        settings={{search: true}}
                                                    />
                                                    {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
                                                </>

                                            )
                                        }}
                                    </Field>
                                </Grid> :
                                (placement !== "CRM_DEAL_DETAIL_TAB" && (
                                    <Grid item xs={12}>
                                        <Field
                                            name={'object'}
                                            label={'Объект'}
                                            isRequired={true}
                                            defaultValue={initialValues.dealId || ''}
                                        >
                                            {(inputProps, fieldProps) =>
                                                <DealSelect
                                                    {...inputProps}
                                                    {...fieldProps}
                                                    settings={{search: true}}
                                                />
                                            }
                                        </Field>
                                    </Grid>
                                ))
                            }
                            <Grid item xs={12}>
                                <Field
                                    name={'files'}
                                    label={'Файлы счета'}
                                    defaultValue={initialValues.files || []}
                                >
                                    {(inputProps, fieldProps) =>
                                        <FileLoader
                                            {...inputProps}
                                            {...fieldProps}
                                            settings={{
                                                multiple: true
                                            }}
                                            disabled={isSubmitting}
                                        />
                                    }
                                </Field>
                            </Grid>
                            <Grid item xs={12}>
                                <Field
                                    name={'date'}
                                    label={'Дата счета'}
                                    defaultValue={initialValues.date || ''}
                                >
                                    {inputProps => <Input {...inputProps} type={'date'}/>}
                                </Field>
                            </Grid>
                            <Grid item xs={12}>
                                <Field
                                    name={'invoiceNumber'}
                                    label={'Номер счета'}
                                    defaultValue={initialValues.invoiceNumber || ''}
                                >
                                    {inputProps => <Input {...inputProps}/>}
                                </Field>
                            </Grid>
                            <Grid item xs={12}>
                                <Field
                                    name={'invoiceSum'}
                                    label={'Сумма'}
                                    isRequired={true}
                                    defaultValue={initialValues.invoiceSum || ''}
                                    format={formatMoney}
                                >
                                    {inputProps => {
                                        const {error} = inputProps;
                                        return (
                                            <>
                                                <Input {...inputProps}/>
                                                {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
                                            </>
                                        )
                                    }}
                                </Field>
                            </Grid>
                            <Grid item xs={12}>
                                <Field
                                    name={'paymentPurpose'}
                                    label={'Назначение платежа'}
                                    defaultValue={initialValues.paymentPurpose || ''}
                                >
                                    {inputProps => <Input {...inputProps}/>}
                                </Field>
                            </Grid>
                        </Grid>
                        <Box mt={4}>
                            <div className={'form-buttons'}>
                                <Button
                                    type={'reset'}
                                    variant={'outlined'}
                                    color={'primary'}
                                    disabled={isSubmitting}
                                ><Icon>refresh</Icon></Button>
                                <Button
                                    type={'submit'}
                                    isLoading={isSubmitting}
                                    className={'btn-success'}
                                ><Icon>save</Icon></Button>
                                {type === 'create' && (
                                    <Box mt={1}>
                                        <Button
                                            color={'secondary'}
                                            isLoading={isSubmitting}
                                            className={'btn-link'}
                                            onClick={() => {
                                                form.submit(
                                                    fields => onSubmit(fields, 2),
                                                    onSuccess,
                                                    onError
                                                );
                                            }}
                                        ><Icon>save</Icon><pre>  +  </pre>согласовать</Button>
                                    </Box>
                                )}
                            </div>
                        </Box>
                    </>
                )
            }}
        </Form>
    )
}
export default InvoiceForm;

export function getDealId(fields, invoiceType) {
    const [placement, {ID: dealId}] = Bitrix24.getPlacementInfo();

    if (placement === 'CRM_DEAL_DETAIL_TAB') {
        return Promise.resolve(dealId);
    }

    if (invoiceType === 1) {
        const orderId = fields['order'].value;
        return Database.loadOrder(orderId)
            .then( order => Promise.resolve(order.PROPERTY_VALUES.DEAL_ID) )
    }

    return Promise.resolve(fields['object'].value);
}