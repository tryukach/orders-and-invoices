import React, {useEffect, useState, useContext} from 'react';
import moment from "moment";
import {connect} from "react-redux";
import {orderStatuses} from "root/static";
import {SnackContext} from "root/contexts";

// Classes
import Order from "classes/Order";
import Bitrix24 from "classes/Bitrix24";
import Data from "classes/Data";
import Database from "classes/Database";

// Constants
import {DATE_TIME_FORMAT} from "root/constants";

// Api functions
import {numberToMoney, callMany} from "core/Api";

// Components
import Typography from "@material-ui/core/Typography";
import Button, {LoadButton} from "core/desktop/view/Button";
import Table from "components/mobile/TableCard";
import Box from "@material-ui/core/Box";
import PageLoader from "core/desktop/view/PageLoader";
import OrderFilter from "components/mobile/forms/OrderFilter";
import Icon from "core/desktop/view/Icon";

// Actions
import {setPage} from "root/actions/app";

function Main(props) {
    const {users, currentUser, currentRole, initialValues = {}} = props;
    const [placement, {ID: dealId}] = Bitrix24.getPlacementInfo();

    const snackManager = useContext(SnackContext);

    const [data, setData] = useState({});
    const {orders, invoices, deals} = data;

    const [status, setStatus] = useState('loading');

    useEffect(() => {
        loadData({
            ">=DATE_CREATE": initialValues['fromDate'] ?
                moment(initialValues['fromDate']).startOf("day").format():
                moment().startOf("month").startOf('day').format(),
            "<=DATE_CREATE": initialValues['toDate'] ?
                moment(initialValues['toDate']).endOf("day").format():
                moment().endOf("month").endOf('day').format(),
            "PROPERTY_DEAL_ID": placement === "CRM_DEAL_DETAIL_TAB" ? dealId : (initialValues['object'] ||  null),
            "PROPERTY_STATUS_ID": initialValues['status'] || null,
            "PROPERTY_CREATOR_ID": initialValues['creator'] || null,
            "PROPERTY_RESPONSIBLE_ID": initialValues['responsible'] || null,
        }).then( data => {
            setData(data);
            setStatus('ready');
        })
    }, []);

    if (status === 'loading') {
        return <PageLoader/>;
    }

    return (
        <>
            <OrderFilter
                initialValues={initialValues}
                placement={placement}
                onSubmit={fields => {
                    const params = {
                        ">=DATE_CREATE": moment(fields['fromDate'].value).startOf("day").format(),
                        "<=DATE_CREATE": moment(fields['toDate'].value).endOf("day").format(),
                        "PROPERTY_DEAL_ID": placement === "CRM_DEAL_DETAIL_TAB" ? dealId : fields['object'].value,
                        "PROPERTY_STATUS_ID": fields['status'] ? fields['status'].value : null,
                        "PROPERTY_CREATOR_ID": fields['creator'] ? fields['creator'].value : null,
                        "PROPERTY_RESPONSIBLE_ID": fields['responsible'] ? fields['responsible'].value: null,
                    };

                    return loadData(params).then( data => setData(data) );
                }}
            />
            <Box mt={4}>
                <div className={'title-with-button'}>
                    <Typography variant={'h2'}>Заявки на закупку</Typography>
                    {([3,4].indexOf(currentRole) === -1 || Bitrix24.isAdmin()) && (
                        <Button
                            size={'small'}
                            onClick={() => setPage(4)}
                            color={'secondary'}
                        ><Icon>add</Icon></Button>
                    )}
                </div>
                <Table
                    id={'orders'}
                    items={orders}
                    settings={{
                        orderBy: 2,
                        order: 'desc',
                        notFound: 'Заявок нет',
                        perPage: 5,
                        perPageOptions: [5,10,20],
                        gridSize: {
                            xs: 12,
                            sm: 6,
                            lg: 3
                        }
                    }}
                    columns={[
                        {id: 1, text: '№ заявки'},
                        {id: 2, text: 'Дата создания'},
                        {id: 3, text: 'Объект', isHidden: placement === "CRM_DEAL_DETAIL_TAB"},
                        {id: 4, text: 'Описание заявки'},
                        {id: 5, text: 'Создатель'},
                        {id: 6, text: 'Ответственный'},
                        {id: 7, text: 'Статус', measure: 'руб.'},
                        {id: 8, text: 'Счетов', measure: 'шт.'},
                        {id: 9, text: 'Сумма заявки', measure: 'руб.'},
                        {id: 10, text: 'Действие'},
                    ]}
                    getCellValue={(columnId, order, updateRow) => {
                        return getCellValue(columnId, order, updateRow, {
                            users,
                            deals,
                            invoices,
                            currentUser,
                            currentRole
                        })
                    }}
                />
            </Box>
        </>
    );
}
export default connect( state => ({
    users: state.staticData.users,
    deals: state.staticData.deals,
    currentUser: state.app.currentUser,
    currentRole: state.app.currentRole,
    initialValues: state.ui['orderFilter'] || {}
}) )(Main);

function getCellValue(id, order, updateRow, data) {
    const {invoices = [], users, currentUser, currentRole, deals} = data;

    const invoicesByOrder = invoices.filter( i => +i.PROPERTY_VALUES.ORDER_ID === +order.ID);
    const sum = invoicesByOrder.reduce((acc, invoice) => {
        return acc + parseFloat(invoice.PROPERTY_VALUES.SUM + "");
    }, 0);

    const isEditable = currentUser === +order.PROPERTY_VALUES.CREATOR_ID && +order.PROPERTY_VALUES.STATUS_ID !== 2;

    switch (id) {
        case 1: return {
            display: (
                <Box display={'flex'} justifyContent={'space-between'} alignItems={'center'}>
                    <a onClick={() => setPage(6, {orderId: order.ID})} title={'просмотр заявки'}>{order.ID}</a>
                    {isEditable && <Icon onClick={() => setPage(5, {orderId: order.ID})}>edit</Icon>}
                </Box>
            ),
            source: +order.ID
        };
        case 2:
            const date = moment(order['DATE_CREATE']);
            return {display: date.format(DATE_TIME_FORMAT), source: date.valueOf()};
        case 3:
            const deal = deals.byId[order.PROPERTY_VALUES.DEAL_ID];
            return Bitrix24.getDealName( deal );
        case 5: return Data.getUserDisplayName(users.byId[order.PROPERTY_VALUES.CREATOR_ID]);
        case 6: return Data.getUserDisplayName(users.byId[order.PROPERTY_VALUES.RESPONSIBLE_ID]);
        case 7:
            let color = "#54a3d8";
            const statusTitle = orderStatuses.byId[order.PROPERTY_VALUES.STATUS_ID].TITLE;

            switch (+order.PROPERTY_VALUES.STATUS_ID) {
                case 1: color = "#a99d42"; break;
                case 2: color = "#5ca968"; break;
                case 3: color = "#d85353"; break;
            }
            return {
                display: <span className={'highlight'} style={{background: color}}>{statusTitle}</span>,
                source: statusTitle
            };
        case 8: return invoicesByOrder.length;
        case 9: return {display: numberToMoney(sum), source: sum};
        case 4: return order.DETAIL_TEXT;
        case 10:
            if ((+order.PROPERTY_VALUES.CREATOR_ID === +currentUser || currentRole === 1) &&
                +order.PROPERTY_VALUES.STATUS_ID !== 2 && Order.isComplete(order.ID, invoicesByOrder)) {
                return (
                    <CompleteOrderButton
                        orderId={order.ID}
                        updateRow={updateRow}
                    />
                );
            }
            return '-';
    }
}

function CompleteOrderButton(props) {
    const {orderId, updateRow} = props;
    const snackManager = useContext(SnackContext);

    return (
        <LoadButton
            size={'small'}
            className={'btn-success'}
            onClick={() => {
                return Order.complete(orderId)
                    .then( () => {
                        updateRow({PROPERTY_VALUES: {STATUS_ID: 2}});
                        snackManager.showMessage(`Заявка № ${orderId} завершена`, {variant: 'success'});
                        return Promise.resolve();
                    })
            }}
        >Завершить</LoadButton>
    );
}

function loadData(params) {
    return callMany({
        orders: {handler: () => Database.loadOrders(params)},
        deals: {
            handler: ({orders}) => {
                const dealIds = orders.map( o => o.PROPERTY_VALUES.DEAL_ID );
                return Bitrix24.getDealsByIds(dealIds);
            }
        },
        invoices: {
            handler: ({orders}) => {
                const ids = orders.map( o => o.ID );
                return Database.loadInvoicesByOrder(ids);
            }
        }
    })
}