import React, {useState, useEffect} from 'react';
import {connect} from "react-redux";
import {invoiceStatuses} from "root/static";

// Components
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Back from "core/desktop/view/Back";
import PageLoader from "core/desktop/view/PageLoader";
import Grid from "@material-ui/core/Grid";
import {renderFiles} from "core/mobile/view/form/fields/file/FileLoader";
import StaticField from "components/mobile/ui/StaticField";
import PaymentsTable from "components/mobile/ui/PaymentsTable";
import ApproveForm from "components/mobile/forms/ApproveInvoiceForm";
import OrderDetailContent from "components/mobile/OrderDetailContent";
import Message from "core/desktop/view/Message";
import ErrorBoundary from "core/ErrorBoundary";

// Api functions
import {callMany, formatMoney} from "core/Api";
import {getStaticFields, getDynamicData} from "components/desktop/forms/ApproveInvoiceForm";

// Classes
import Database from "classes/Database";
import Data from "classes/Data";
import Bitrix24 from "classes/Bitrix24";

function ApproveInvoice(props) {
    const {invoiceId, prevPage} = props;

    const [status, setStatus] = useState('loading');
    const [invoice, setInvoice] = useState(null);
    const [data, setData] = useState({});

    useEffect(() => {
        callMany({
            invoice: {handler: () => Database.loadInvoice(invoiceId)},
            files: {handler: () => Database.getInvoiceFiles(invoiceId)},
            payments: {handler: () => Database.getInvoicePayments(invoiceId)},
            deal: {handler: ({invoice}) => Bitrix24.getDeal(invoice.PROPERTY_VALUES.DEAL_ID)}
        }).then( response => {
            const {invoice, ...data} = response;

            setInvoice(invoice);
            setData(data);
            setStatus('ready');
        })
    }, []);

    if (status === 'loading') {
        return <PageLoader/>;
    }

    if (!invoice) {
        return <Message type={'warning'}>Счета № {invoiceId} не существует</Message>
    }

    const {files = [], payments = [], deal} = data;
    const statusId = +invoice.PROPERTY_VALUES.STATUS_ID;
    const invoiceTypeId = +invoice.PROPERTY_VALUES.INVOICE_TYPE_ID;
    const useOrder = statusId === 2 && invoiceTypeId === 1;
    const orderId = invoice.PROPERTY_VALUES.ORDER_ID;

    const lastChanges = getStaticFields(statusId, invoice);

    return (
        <div>
            <Grid container spacing={2}>
                <Grid item xs={12} lg={useOrder ? 5 : 12}>
                    <Box mb={2}>
                        <Back to={prevPage}>Счет № {invoiceId}</Back>
                    </Box>
                    <Grid container spacing={1}>
                        <StaticField label={'Статус'}>{invoiceStatuses.byId[statusId].TITLE}</StaticField>
                        {invoiceTypeId === 1 &&  <StaticField label={'Номер заявки'}>№ {invoice.PROPERTY_VALUES.ORDER_ID}</StaticField>}
                        <StaticField label={'Объект'}>{Bitrix24.getDealName(deal)}</StaticField>
                        <StaticField label={'Файлы счета'}>{files.length === 0 ? '-' : <Box mt={1.25}>{renderFiles(files)}</Box>}</StaticField>
                        <StaticField label={'Дата счета'}>{Data.extractDate(invoice.PROPERTY_VALUES.INVOICE_DATE) || '-'}</StaticField>
                        <StaticField label={'Номер счета'}>{invoice.PROPERTY_VALUES.INVOICE_NUMBER || '-'}</StaticField>
                        <StaticField label={'Сумма счета'}>{formatMoney(invoice.PROPERTY_VALUES.SUM)}</StaticField>
                        <StaticField label={'Назначение платежа'}>{invoice.PROPERTY_VALUES.PAYMENT_PURPOSE || '-'}</StaticField>
                        {getDynamicData(statusId, invoice)}
                    </Grid>
                    {payments.length > 0 && <Box my={4}><PaymentsTable payments={payments}/></Box>}
                    {lastChanges && (
                        <Box my={4}>
                            <Typography variant={'h4'}>Последние изменения</Typography>
                            <Box mt={2}>
                                <Grid container spacing={1}>{lastChanges}</Grid>
                            </Box>
                        </Box>
                    )}
                    <Box my={4}>
                        <ErrorBoundary title={'Ошибка в форме'}><ApproveForm invoice={invoice}/></ErrorBoundary>
                    </Box>
                </Grid>
                {useOrder && (
                    <Grid item xs={12} lg={7}>
                        <Box mb={2}>
                            <Typography variant={'h3'}>Заявка № {orderId}</Typography>
                        </Box>
                        <OrderDetailContent orderId={orderId}/>
                    </Grid>
                )}
            </Grid>
        </div>
    )
}
export default connect( state => ({
    invoiceId: state.app.pageOptions.invoiceId,
    prevPage: state.app.prevPage
}))(ApproveInvoice);