import React, {useContext} from 'react';
import {connect} from "react-redux";
import {SnackContext} from "root/contexts";

// Classes
import Order from "classes/Order";

// Components
import Box from "@material-ui/core/Box";
import Back from "core/desktop/view/Back";

import OrderForm from "components/mobile/forms/OrderForm";

// Actions
import {setPage} from "actions/app";

function CreateOrder(props) {
    const {currentUser, prevPage} = props;
    const snackManager = useContext(SnackContext);

    return (
        <div>
            <Box mb={2}>
                <Back to={prevPage}>Создание новой заявки</Back>
            </Box>
            <OrderForm
                id={'create-order'}
                type={'create'}
                onSubmit={ fields => {
                    return Order.create(fields, {creatorId: currentUser});
                }}
                onSuccess={() => {
                    snackManager.showMessage('Заявка добавлена', {variant: 'success'});
                    setPage(1);
                }}
                onError={error => {
                    console.log(error);
                    snackManager.showMessage('Произошла ошибка', {variant: 'error'});
                }}
            />
        </div>

    )
}
export default connect(state => ({
    currentUser: state.app.currentUser,
    prevPage: state.app.prevPage
}))(CreateOrder);