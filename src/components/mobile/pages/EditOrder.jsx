import React, {useContext, useEffect, useState} from 'react';
import {connect} from "react-redux";
import {SnackContext} from "root/contexts";

// Classes
import Order from "classes/Order";
import Database from "classes/Database";
import Data from "classes/Data";

// Components
import Box from "@material-ui/core/Box";
import Back from "core/desktop/view/Back";
import OrderForm from "components/mobile/forms/OrderForm";
import PageLoader from "core/desktop/view/PageLoader";

// Actions
import {setPage} from "actions/app";
import {callMany} from "core/Api";
import uuid from "uuid";

function EditOrder(props) {
    const {orderId, prevPage} = props;
    const snackManager = useContext(SnackContext);

    const [status, setStatus] = useState('loading');
    const [initialValues, setInitialValues] = useState({});

    useEffect(() => {
        callMany({
            order: {handler: () => Database.loadOrder(orderId)},
            orderData: {handler: () => Database.getOrderRelatedData(orderId)},
        }).then( response => {
            const {order, orderData} = response;
            const {files, products} = orderData;

            const initialValues = {
                dealId: order.PROPERTY_VALUES.DEAL_ID,
                desc: order.DETAIL_TEXT,
                comment: order.PROPERTY_VALUES.COMMENT,
                products: Data.extractProducts(products),
                attachments: Data.extractFiles(files)
            };

            setInitialValues(initialValues);
            setStatus('ready');
        })
    }, []);

    if (status === 'loading') {
        return <PageLoader/>;
    }

    return (
        <div>
            <Box mb={4}>
                <Back to={prevPage}>Заявка № {orderId}</Back>
            </Box>
            <OrderForm
                id={'edit-order'}
                type={'edit'}
                onSubmit={ fields => {
                    return Order.edit(fields, orderId);
                }}
                onSuccess={() => {
                    snackManager.showMessage('Заявка сохранена', {variant: 'success'});
                    setPage(1);
                }}
                onError={() => {
                    snackManager.showMessage('Произошла ошибка', {variant: 'error'});
                }}
                initialValues={initialValues}
            />
        </div>

    )
}
export default connect(state => ({
    prevPage: state.app.prevPage,
    orderId: state.app.pageOptions.orderId
}))(EditOrder);