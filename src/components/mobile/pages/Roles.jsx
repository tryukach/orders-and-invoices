import React, {useContext, useEffect, useState} from 'react';
import {roleList} from "root/static";
import {SnackContext} from "root/contexts";
import {connect} from "react-redux";

// Components
import Form from "core/desktop/view/form/Form";
import TwoColumnField from "core/desktop/view/form/TwoColumnField";
import UserSelect from "core/desktop/view/form/fields/select/UserSelect";
import Grid from "@material-ui/core/Grid";
import Icon from "core/desktop/view/Icon";
import Button from "core/desktop/view/Button";
import PageLoader from "core/desktop/view/PageLoader";

// Constants
import {E_USERS_ROLES} from "root/constants";

// Classes
import Database from "classes/Database";
import Data from "classes/Data";

// Api functions
import {callMany} from "core/Api";

// Actions
import {setCurrentRole} from "actions/app";

function Roles(props) {
    const {currentUser} = props;
    const snackManager = useContext(SnackContext);

    const [status, setStatus] = useState('loading');
    const [initialValues, setInitialValues] = useState({});

    useEffect(() => {
        Database.loadRoles()
            .then( roles => {
                setInitialValues(Data.groupIdsBy(roles, 'ROLE_ID', 'USER_ID'));
                setStatus('ready');
            });
    }, []);


    if (status === 'loading') {
        return <PageLoader/>;
    }


    return (
        <Form
            id={'roles'}
            settings={{
                labelWidth: 200,
                fieldGrid: 3,
                fieldWidth: 300,
                resetAfterSubmit: false
            }}
            onSubmit={ fields => {
                return Database.loadRoles()
                    .then( roles => {
                        const userIdsByRole = Data.groupIdsBy(roles, 'ROLE_ID', 'USER_ID');

                        const idsToDelete = roles.filter( role => {
                            const roleId = role.PROPERTY_VALUES.ROLE_ID;
                            const userId = role.PROPERTY_VALUES.USER_ID;
                            const userIds = fields[roleId].value || [];

                            return userIds.indexOf(userId) === - 1;
                        }).map( role => role.ID );

                        return callMany({
                            deleteRoles: {handler: () => Database.deleteByIds(E_USERS_ROLES, idsToDelete) },
                            saveRoles: {handler: () => Database.saveRoles(fields, userIdsByRole)}
                        });
                    });
            }}
            onSuccess={() => {
                Database.getUserRole(currentUser).then( roleId => setCurrentRole(roleId) );
                snackManager.showMessage('Роли успешно сохранены', {variant: 'success'});
            }}
            onError={ error => {
                console.log(error);
                snackManager.showMessage('Произошла ошибка', {variant: 'error'})
            }}
        >
            {(form, settings, isSubmitting) => {
                const {labelWidth, fieldWidth} = settings;
                return (
                    <Grid container spacing={2}>
                        {roleList.map( role =>
                            <TwoColumnField
                                key={role.ID}
                                label={role.TITLE}
                                name={role.ID}
                                defaultValue={initialValues[role.ID] || []}
                                isRequired={true}
                                fieldWidth={fieldWidth}
                                labelWidth={labelWidth}
                            >
                                {(inputProps, fieldProps) =>
                                    <UserSelect
                                        {...inputProps}
                                        {...fieldProps}
                                        settings={{
                                            multiple: role.MULTIPLE === "Y",
                                            search: true,
                                            startAdornment: <Icon>account_circle</Icon>
                                        }}
                                    />
                                }
                            </TwoColumnField>
                        )}
                        <Grid item>
                            <div className={'form-buttons'}>
                                <Button
                                    type={'submit'}
                                    isLoading={isSubmitting}
                                    className={'btn-success'}
                                >Сохранить</Button>
                            </div>
                        </Grid>
                    </Grid>
                )}}
        </Form>
    )
}
export default connect(state => ({currentUser: state.app.currentUser}))(Roles);