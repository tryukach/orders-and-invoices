import React, {useContext, useState, useEffect} from 'react';
import {connect} from "react-redux";
import {invoiceStatuses, paymentStatuses} from "root/static";
import moment from "moment";
import {SnackContext} from "root/contexts";

// Components
import Typography from "@material-ui/core/Typography";
import MultiButton from "core/desktop/view/MultiButton";
import Table from "components/mobile/TableCard";
import Button from "core/desktop/view/Button";
import InvoiceFilter from "components/mobile/forms/InvoiceFilter";
import Box from "@material-ui/core/Box";
import PageLoader from "core/desktop/view/PageLoader";
import Icon from "core/desktop/view/Icon";

// Classes
import Bitrix24 from "classes/Bitrix24";
import Data from "classes/Data";
import Database from "classes/Database";
import Invoice from "classes/Invoice";

// Actions
import {setPage} from "actions/app";

// Api functions
import {callMany, numberToMoney} from "core/Api";

// Constants
import {DATE_TIME_FORMAT, E_INVOICES} from "root/constants";

function InvoiceList(props) {
    const {currentRole, users, currentUser, initialValues = {}} = props;
    const [placement, {ID: dealId}] = Bitrix24.getPlacementInfo();

    const [data, setData] = useState({});
    const [status, setStatus] = useState('loading');

    const {invoices, deals} = data;

    useEffect(() => {
        loadData({
            ">=DATE_CREATE": initialValues['fromDate'] ?
                moment(initialValues['fromDate']).startOf("day").format():
                moment().startOf("month").startOf('day').format(),
            "<=DATE_CREATE": initialValues['toDate'] ?
                moment(initialValues['toDate']).endOf("day").format():
                moment().endOf("month").endOf('day').format(),
            "PROPERTY_DEAL_ID": placement === "CRM_DEAL_DETAIL_TAB" ? dealId : (initialValues['object'] ||  null),
            "PROPERTY_STATUS_ID": initialValues['status'] || null,
            "PROPERTY_CREATOR_ID": initialValues['creator'] || null,
        }).then( data => {
            setData(data);
            setStatus('ready');
        })
    }, []);

    if (status === 'loading') {
        return <PageLoader/>;
    }

    return (
        <>
            <InvoiceFilter
                initialValues={initialValues}
                onSubmit={ fields => {
                    const params = {
                        ">=DATE_CREATE": moment(fields['fromDate'].value).startOf("day").format(),
                        "<=DATE_CREATE": moment(fields['toDate'].value).endOf("day").format(),
                        "PROPERTY_DEAL_ID": placement === "CRM_DEAL_DETAIL_TAB" ? dealId : fields['object'].value,
                        "PROPERTY_STATUS_ID": fields['status'] ? fields['status'].value : null,
                        "PROPERTY_CREATOR_ID": fields['creator'] ? fields['creator'].value : null
                    };
                    return loadData(params).then( data => setData(data) );
                }}
            />
            <Box mt={4}>
                <div className={'title-with-button'}>
                    <Typography variant={'h2'}>Счета</Typography>
                    <MultiButton
                        size={'small'}
                        actions={[
                            {title: 'с заявкой', onClick: () => setPage(7, {invoiceType: 1})},
                            {title: 'без заявки', onClick: () => setPage(7, {invoiceType: 2})},
                        ]}
                        color={'secondary'}
                    ><Icon>add</Icon></MultiButton>
                </div>
                <Table
                    id={'invoices'}
                    items={invoices}
                    settings={{
                        orderBy: 3,
                        order: 'desc',
                        notFound: 'Счетов нет',
                        perPage: 5,
                        perPageOptions: [5,10,20],
                        gridSize: {
                            xs: 12,
                            sm: 6,
                            lg: 3
                        }
                    }}
                    columns={[
                        {id: 1, text: '№ счета'},
                        {id: 2, text: '№ заявки'},
                        {id: 3, text: 'Дата создания'},
                        {id: 4, text: 'Объект', isHidden: placement === 'CRM_DEAL_DETAIL_TAB'},
                        {id: 5, text: 'Создатель'},
                        {id: 6, text: 'Назначение платежа'},
                        {id: 7, text: 'Статус счета'},
                        {id: 8, text: 'Сумма счета', measure: 'руб.', subText: 'Оплачено / Всего'},
                        {id: 9, text: 'Статус оплаты'},
                        {id: 10, text: 'Действие'},
                    ]}
                    getCellValue={(id, invoice, updateRow) => {
                        const data = {
                            users,
                            currentUser,
                            currentRole,
                            deals,
                        };
                        return getCellValue(id, invoice, updateRow, data)
                    }}
                />
            </Box>
        </>

    )
}
export default connect(state => ({
    users: state.staticData.users,
    currentRole: state.app.currentRole,
    currentUser: state.app.currentUser,
    initialValues: state.ui['invoiceFilter'] || {}
}))(InvoiceList);

function getCellValue(id, invoice, updateRow, data) {
    const {users, currentUser, currentRole, deals} = data;

    const statusId = +invoice.PROPERTY_VALUES.STATUS_ID;
    const orderId = +invoice.PROPERTY_VALUES.ORDER_ID;
    const invoiceId = +invoice.ID;

    const isEditable = (+invoice.PROPERTY_VALUES.CREATOR_ID === currentUser || Bitrix24.isAdmin()) &&
        [1,10].indexOf(+invoice.PROPERTY_VALUES.STATUS_ID) !== -1;

    switch (id) {
        case 1: return {
            display: (
                <Box display={'flex'} justifyContent={'space-between'} alignItems={'center'}>
                    <a onClick={() => setPage(9, {invoiceId})} title={'просмотр счета'}>{invoiceId}</a>
                    {isEditable && <Icon onClick={() => setPage(8, {invoiceId})}>edit</Icon>}
                </Box>
            ),
            source: invoiceId
        };
        case 2: return +invoice.PROPERTY_VALUES.INVOICE_TYPE_ID === 1 ? {
            display: <a onClick={() => setPage(6, {orderId})} title={'просмотр заявки'}>{orderId}</a>,
            source: orderId
        } : '-';
        case 3:
            const date = moment(invoice['DATE_CREATE']);
            return {display: date.format(DATE_TIME_FORMAT), source: date.valueOf()};
        case 4:
            const deal = deals.byId[invoice.PROPERTY_VALUES.DEAL_ID];
            return Bitrix24.getDealName( deal );
        case 5: return Data.getUserDisplayName(users.byId[invoice.PROPERTY_VALUES.CREATOR_ID]);
        case 6: return invoice.PROPERTY_VALUES.PAYMENT_PURPOSE;
        case 7:
            let color = "#54a3d8";
            switch (statusId) {
                case 2: color = "#4ac9dc"; break;
                case 3: color = "#5dd0d4"; break;
                case 4: color = "#4cbabd"; break;
                case 5: color = "#58b99d"; break;
                case 6: color = "#5578d6"; break;
                case 7: color = "#a99d42"; break;
                case 8: color = "#b9904f"; break;
                case 9: color = "#5ca968"; break;
                case 10: color = "#d85353"; break;
            }
            return {
                display: <span className={'highlight'} style={{background: color}}>{invoiceStatuses.byId[statusId].TITLE}</span>,
                source: invoiceStatuses.byId[statusId].TITLE
            };
        case 8:
            return {
                display: `${numberToMoney(invoice.PROPERTY_VALUES.PAYED_SUM)} / ${numberToMoney(invoice.PROPERTY_VALUES.SUM)}`,
                source: parseFloat(invoice.PROPERTY_VALUES.SUM + "")
            };
        case 9: return paymentStatuses.byId[invoice.PROPERTY_VALUES.PAYED_STATUS].TITLE;
        case 10:
            const roleId = Data.getRoleIdByInvoiceStatus(statusId);
            const isAuthorAction = currentUser === +invoice.PROPERTY_VALUES.CREATOR_ID && (statusId < 2 || statusId > 4 && statusId !== 7);

            return (roleId === currentRole || isAuthorAction) && statusId !== 9 ?
                <InvoiceApproveButton
                    invoice={invoice}
                    currentRole={currentRole}
                    updateTableRow={updateRow}
                />: "-";
    }
}

function InvoiceApproveButton(props) {
    const {invoice, currentRole, updateTableRow} = props;
    const snackManager = useContext(SnackContext);

    const statusId = +invoice.PROPERTY_VALUES.STATUS_ID;
    const invoiceType = +invoice.PROPERTY_VALUES.INVOICE_TYPE_ID;

    const [isLoading, setLoading] = useState(false);

    if (statusId === 1 || statusId === 10) {
        const highRole = [1,2].indexOf(currentRole) !== -1;
        const nextStatus = (invoiceType === 2 && highRole) || highRole ? 3 : 2;

        return (
            <Button
                size={"small"}
                color={'secondary'}
                isLoading={isLoading}
                onClick={ () => {
                    setLoading(true);
                    return Invoice.createTaskByStage(invoice, nextStatus)
                        .then( taskId => {
                            return Database.updateItem(E_INVOICES, {
                                ID: invoice.ID,
                                PROPERTY_VALUES: {
                                    STATUS_ID: nextStatus,
                                    TASK_ID: taskId
                                }
                            })
                        }).then( () => {
                            updateTableRow({
                                PROPERTY_VALUES: {
                                    STATUS_ID: nextStatus
                                }
                            });
                            snackManager.showMessage(`Счет № ${invoice.ID} отправлен на согласование`, {variant: 'success'});
                        }).catch( error => {
                            console.log(error);
                            snackManager.showMessage('Произошла ошибка', {variant: 'error'})
                        }).finally( () => {
                            setLoading(false);
                        });
                }}
            >Согласовать</Button>
        );
    }

    let buttonTitle = "Утвердить";
    switch (statusId) {
        case 4: buttonTitle = "Оплатить"; break;
        case 5: buttonTitle = "Запланировать"; break;
        case 6: buttonTitle = "Проконтролировать"; break;
        case 7: buttonTitle = "Собрать документы"; break;
        case 8: buttonTitle = "Получить документы"; break;
    }

    return (
        <Button
            color={'secondary'}
            size={"small"}
            onClick={() => setPage(10, {invoiceId: invoice.ID})}
        >{buttonTitle}</Button>
    );
}

function loadData(params) {
    return callMany({
        invoices: {handler: () => Database.loadInvoices(params)},
        deals: {
            handler: ({invoices}) => {
                const dealIds = invoices.map( i => i.PROPERTY_VALUES.DEAL_ID );
                return Bitrix24.getDealsByIds(dealIds);
            }
        }
    })
}