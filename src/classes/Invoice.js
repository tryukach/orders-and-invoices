import moment from "moment";

// Classes
import Bitrix24 from "./Bitrix24";
import Database from "./Database";
import Data from "classes/Data";

// Api functions
import {callMany, callStepByStep, moneyToNumber} from "core/Api";

// Constants
import {
    E_ATTACHMENTS,
    E_INVOICE_PAYMENTS,
    E_INVOICES, U_INVOICE_STATUS_ID,
} from "root/constants";

class Invoice {

    static setStatus(invoiceId, statusId) {
        return Database.updateItem(E_INVOICES, {
            ID: invoiceId,
            PROPERTY_VALUES: {
                STATUS_ID: statusId
            }
        })
    }

    static create(fields, {invoiceType, currentUser, statusId, dealId}) {

        const invoiceNumber = fields['invoiceNumber'].value;
        const params = {
            NAME: `счет № ${invoiceNumber}`,
            DATE_ACTIVE_FROM: moment().format(),
            PROPERTY_VALUES: {
                INVOICE_TYPE_ID: invoiceType,
                INVOICE_NUMBER: invoiceNumber,
                INVOICE_DATE: fields['date'].value ? moment( fields['date'].value ).format() : "",
                CREATOR_ID: currentUser,
                DEAL_ID: dealId,
                ORDER_ID: invoiceType === 1 ? fields['order'].value : null,
                SUM: moneyToNumber(fields['invoiceSum'].value),
                PAYMENT_PURPOSE: fields['paymentPurpose'].value,
                STATUS_ID: statusId,
                PAYED_STATUS: 1,
                PAYED_SUM: 0
            }
        };

        return Database.addItem(E_INVOICES, params)
            .then( ID => Database.saveFiles(ID, 'invoice', fields['files'].value) )
            .then( () => Promise.resolve(statusId) );
    }

    static edit(fields, invoiceId, {invoiceType, dealId}) {
        const invoiceNumber = fields['invoiceNumber'].value;

        const params = {
            ID: invoiceId,
            NAME: `счет № ${invoiceNumber}`,
            PROPERTY_VALUES: {
                INVOICE_NUMBER: invoiceNumber,
                INVOICE_DATE: fields['date'].value ? moment( fields['date'].value ).format() : "",
                DEAL_ID: dealId,
                ORDER_ID: invoiceType === 1 ? fields['order'].value : null,
                SUM: moneyToNumber(fields['invoiceSum'].value),
                PAYMENT_PURPOSE: fields['paymentPurpose'].value,
            }
        };

        return Database.updateItem(E_INVOICES, params)
            .then( () => Database.updateFiles(invoiceId, 'invoice', fields['files']) )
    }

    static update(id, params) {
        return Database.updateItem(E_INVOICES, {
            ID: id,
            ...params
        });
    }

    static delete(id) {
        return callMany({
            deleteInvoice: {handler: () => Database.deleteItem(E_INVOICES, id)},
            files: {handler: () => Database.getInvoiceFiles(id)},
            payments: {handler: () => Database.getInvoicePayments(id)},
            paymentFiles: {
                handler: ({payments}) => {
                    const ids = payments.map( p => p.ID );
                    return ids.length > 0 ?
                        Database.getFilesBy(ids, 'ppFiles'):
                        Promise.resolve();
                }
            }
        }).then( response => {
            const {files = [], payments = [], paymentFiles = []} = response;

            return callMany({
                deleteFiles: {handler: () => Database.deleteItems(E_ATTACHMENTS, files)},
                deletePayments: {handler: () => Database.deleteItems(E_INVOICE_PAYMENTS, payments)},
                deletePaymentFiles: {handler: () => Database.deleteItems(E_ATTACHMENTS, paymentFiles)},
            });
        });
    }

    static deleteMany(ids, onSuccess, onError) {
        const promises = ids.map( id => {
            return () => {
                return Invoice.delete(id)
                    .then(
                        () => {
                            if (typeof onSuccess === 'function') {
                                onSuccess(`Счет № ${id} удален`);
                            }
                        },
                        error => {
                            console.log(error);
                            if (typeof onError === 'function') {
                                onError(`При удалении счета № ${id} произошла ошибка`);
                            }
                        },
                    )
            };
        });

        return callStepByStep(promises);
    }

    static approve(fields, invoice) {
        const statusId = +invoice.PROPERTY_VALUES.STATUS_ID;
        const isTaskCompleted = statusId !== 6 || (statusId === 6 && +fields['deliveryStatus'].value === 1);
        const nextStatusId = Invoice.getNextStatus(invoice, fields);

        if (isTaskCompleted) {
            Bitrix24.closeTask(invoice.PROPERTY_VALUES.TASK_ID);
        }

        return (
            !isTaskCompleted ?
                Promise.resolve(null):
                Invoice.createTaskByStage(invoice, nextStatusId)
        ).then( taskId => {
            const paramsByStage = Invoice.getUpdateParamsByStage(fields, invoice);

            const params = {
                ID: invoice.ID,
                PROPERTY_VALUES: {
                    STATUS_ID: nextStatusId,
                    TASK_ID: taskId || invoice.PROPERTY_VALUES.TASK_ID,
                    ...paramsByStage
                }
            };
            return Database.updateItem(E_INVOICES, params);
        })
            .then( () => Invoice.saveRelationDataByStage(invoice, fields) )
            .then( () => Invoice.onApprove(invoice, nextStatusId) )
            .then( () => Promise.resolve(nextStatusId) )
    }

    static getRestPaymentSum(invoice) {
        const payedSum = parseFloat(invoice.PROPERTY_VALUES.PAYED_SUM);
        const invoiceSum = parseFloat(invoice.PROPERTY_VALUES.SUM);
        return payedSum > 0 ? invoiceSum - payedSum : invoiceSum;
    }

    static getUpdateParamsByStage(fields, invoice) {

        switch (+invoice.PROPERTY_VALUES.STATUS_ID) {
            case 2: return {E_COMMENT: fields['commentE'].value};
            case 3:
                const currentPaymentSum = fields['paymentSum'] ?
                    moneyToNumber(fields['paymentSum'].value):
                    Invoice.getRestPaymentSum(invoice);

                return {
                    D_COMMENT: fields['commentD'].value,
                    PAYMENT_TYPE_ID: fields['paymentType'].value || 1,
                    CURRENT_PAYMENT_SUM: currentPaymentSum
                };
            case 4:
                const totalPayedSum = Invoice.getTotalPayedSum(invoice, fields['ppSum']);
                return {
                    PAYED_SUM: totalPayedSum.toFixed(2),
                    PAYED_STATUS: totalPayedSum >= parseFloat(invoice.PROPERTY_VALUES.SUM + "") ? 3 : totalPayedSum > 0 ? 2 : 1,
                };
            case 5: {
                const deliveryDate = fields['deliveryDate'].value;
                return {DELIVERY_DATE: deliveryDate ? moment(deliveryDate).format() : ""};
            }
            case 6:{
                const deliveryStatus = +fields['deliveryStatus'].value;
                if (deliveryStatus === 1) {
                    return {DELIVERY_STATUS_ID: deliveryStatus}
                }
                return {
                    DELIVERY_DATE: moment(fields['deliveryDate'].value).format(),
                    DELIVERY_STATUS_ID: deliveryStatus,
                };
            }
            case 7:
                const documentsReceiveDate = fields['documentsReceiveDate'].value;
                return {
                    DOCUMENTS_RECEIVE_DATE: moment(documentsReceiveDate).format(),
                    DOCUMENTS_RECEIVE_STATUS_ID: fields['documentsReceiveStatus'].value,
                };
        }
    }

    static getNextStatus(invoice, fields) {
        const statusId = +invoice.PROPERTY_VALUES.STATUS_ID;
        const isTaskNotCompleted = statusId === 6 && fields['deliveryStatus'].value !== 1;
        const totalPayedSum = Invoice.getTotalPayedSum(invoice, fields['ppSum']);
        const invoiceSum = parseFloat(invoice.PROPERTY_VALUES.SUM);

        debugger;

        if (statusId === 4 && totalPayedSum < invoiceSum) return 3;
        return isTaskNotCompleted ? statusId : statusId === 7 ? 9 : statusId + 1;
    }

    static getTotalPayedSum(invoice, ppSumField) {
        const ppSum = ppSumField === undefined ? 0 : moneyToNumber(ppSumField.value);
        return parseFloat(invoice.PROPERTY_VALUES.PAYED_SUM) + ppSum;
    }

    static saveRelationDataByStage(invoice, fields) {

        switch (+invoice.PROPERTY_VALUES.STATUS_ID) {
            case 4: {
                const params = {
                    NAME: "Оплата по счету",
                    PROPERTY_VALUES: {
                        INVOICE_ID: invoice.ID,
                        PP_SUM: moneyToNumber(fields['ppSum'].value),
                        PAYMENT_DATE: fields['paymentDate'].value
                    }
                };

                return callMany({
                    paymentId: {handler: () => Database.savePayment(params)},
                    saveFiles: {handler: ({paymentId}) => Database.saveFiles(paymentId, 'ppFiles', fields['ppFiles'].value)},
                });
            }
            case 7: return Database.saveFiles(invoice.ID, 'closedDocuments', fields['closedDocuments'].value);
            default: return Promise.resolve();
        }
    }

    static onApprove(invoice, nextStatus) {
        return Bitrix24.getAppUrl()
            .then( appUrl => {
                if (+invoice.PROPERTY_VALUES.STATUS_ID === 4) {
                    const url = appUrl + `/?action=invoice_show&id=${invoice.ID}`;
                    return Bitrix24.notifyUser(invoice.PROPERTY_VALUES.CREATOR_ID,
                        `<a href='${url}' target="_blank">Счет № ${invoice.ID}</a> ${nextStatus === 3 ? "частично" : "полностью"} оплачен. Заберите платежку.`);
                }
                return Promise.resolve();
            })
    }

    static getLabelsByStage(invoice, status) {
        let  labelsByStage;
        const ID = invoice.ID;

        switch (status) {
            case 2: labelsByStage = {ActivityTitle: "Проверить и утвердить счет (гл. инженер)"}; break;
            case 3: labelsByStage = {ActivityTitle: "Проверить и утвердить счет (ген. директор)"}; break;
            case 4: labelsByStage = {
                ActivityTitle: "Оплата счета",
                ActivityDesc: `Оплатить и загрузить документы по счету № ${ID}`,
                actionButtonTitle: "Оплачено"
            }; break;
            case 5: labelsByStage = {
                ActivityTitle: `Планирование поставки`,
                ActivityDesc: `Заполнить дату поставки материалов по счету № ${ID}`,
                actionButtonTitle: "Запланировать"
            }; break;
            case 6: labelsByStage = {
                ActivityTitle: `Контроль поставки`,
                ActivityDesc: `Проконтролировать поставку материалов на объект`,
                actionButtonTitle: "Утвердить",
                secondaryActionButtonTitle: "Запланировать"
            }; break;
            case 7: labelsByStage = {
                ActivityTitle: `Сбор документов`,
                ActivityDesc: `Загрузить все закрывающие документы по счету № ${ID}`,
                actionButtonTitle: "Собрано"
            }; break;
            case 8: labelsByStage = {
                ActivityTitle: `Получение документов`,
                ActivityDesc: `Указать дату и статус получения документов по счету № ${ID}`,
                actionButtonTitle: "Завершить счет"
            }; break;
        }

        return {
            ActivityTitle: `Утверждение счета`,
            ActivityDesc: `Утвердить счет № ${ID}`,
            actionButtonTitle: "Утвердить",
            ...labelsByStage
        };
    }

    static createTaskByStage(invoice, nextStatus) {
        if (nextStatus === 9) {
            return Promise.resolve(null);
        }

        return Promise.all([
            Bitrix24.getAppUrl(),
            Database.getUserIdsByRole(Data.getRoleIdByInvoiceStatus(nextStatus))
        ]).then( response => {
            const [appUrl, userIds = []] = response;

            const labels = Invoice.getLabelsByStage(invoice, nextStatus);
            const url = appUrl + `/?action=invoice_approve&id=${invoice.ID}`;
            const responsibleId = nextStatus > 4 && nextStatus !== 7 ?
                invoice.PROPERTY_VALUES.CREATOR_ID :
                userIds[0];

            const params = {
                DEADLINE: moment().add(3, 'h').format(),
                TITLE: labels[`ActivityTitle`],
                DESCRIPTION: labels[`ActivityDesc`] + `.[URL=${url}]Перейти к выполнению[/URL]`,
                RESPONSIBLE_ID: responsibleId,
                ACCOMPLICES: userIds.length > 1 ? userIds.slice(1) : null,
                [`UF_${U_INVOICE_STATUS_ID}`]: nextStatus,
                UF_CRM_TASK: [`D_${invoice.PROPERTY_VALUES.DEAL_ID}`],
            };
            return Bitrix24.createTask(params);
        })
    }
}
export default Invoice;
