// Classes
import App from "core/classes/App";
import Bitrix24 from "./Bitrix24";
import Data from "./Data";
import Database from "./Database";

// Api
import {storageApi, callMany, normalizeData} from "core/Api";

// Actions
import {setHintVisibility} from "actions/app";


class MyApp extends App {

    static initState(cacheState) {

        return callMany({
            info: {handler: () => Bitrix24.getInfo()},
            user: {handler: () => Bitrix24.getCurrentUser()},
            currentRole: {handler: ({user}) => Database.getUserRole(+user.ID)},
            settings: {handler: () => App.getSettings()}
        }).then( response => {
            const {info, user, settings, currentRole} = response;

            const app = {
                settings, info,
                currentUser: +user.ID,
                currentRole,
                currentDepartment: Data.getUserFirstDepartment(user),
                currentPage: 1,
                lock: false,
                message: {},
                //hints: MyApp.getHints(),
                status: "ready",
                pages: [
                    {id: 1, title: 'Заявки', icon: 'assignment'},
                    {id: 2, title: 'Счета', icon: 'receipt'},
                    {id: 3, title: 'Роли', icon: 'supervisor_account', isShow: () => Bitrix24.isAdmin()},
                    {id: 4, code: 'order_create'},
                    {id: 5, code: 'order_edit'},
                    {id: 6, code: 'order_detail'},
                    {id: 7, code: 'invoice_create'},
                    {id: 8, code: 'invoice_edit'},
                    {id: 9, code: 'invoice_detail'},
                    {id: 10, code: 'invoice_approve'},
                ],
                pageOptions: {}
            };
            return cacheState ?
                Promise.resolve(Object.assign({app}, cacheState)):
                MyApp.loadData(app)
                    .then( response => {
                        const {staticData} = response;
                        const initialState = {staticData, app};
                        return Promise.resolve(initialState);
                    });
        })
    }

    static loadData(app) {
        return Bitrix24.loadUsers().then( users => Promise.resolve(normalizeData(users)) )
            .then( users => Promise.resolve({staticData: {users}}));
    }
    static getHints() {
        return storageApi.get(
            App.generateStorageKey('hints'),
            {1: true}
        );
    }

    static reloadData() {
        return MyApp.initState();
    }

    static updateHint(id, visibility) {
        storageApi.update(App.generateStorageKey('hints'), {[id]: visibility});
        setHintVisibility(id, visibility)
    }
}
export default MyApp;
