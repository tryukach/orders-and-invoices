import Database from "core/classes/Database";

// Constants
import {E_ATTACHMENTS, E_INVOICE_PAYMENTS, E_INVOICES, E_ORDERS, E_PRODUCTS, E_USERS_ROLES} from "root/constants";
import {callMany, callStepBatch} from "core/Api";

class MyDB extends Database {
    static loadOrders(filter = {}) {
        return Database.load(E_ORDERS, {filter});
    }

    static loadInvoices(filter = {}) {
        return Database.load(E_INVOICES, {filter});
    }

    static loadElement(ENTITY, params) {
        return Database.load(ENTITY, params).then( items => {
            return Promise.resolve(items.length > 0 ? items[0] : null);
        });
    }

    static loadInvoice(id) {
        return MyDB.loadElement(E_INVOICES, {filter: {ID: id}});
    }

    static loadOrder(id) {
        return MyDB.loadElement(E_ORDERS, {filter: {ID: id}});
    }

    static loadInvoicesByOrder(ids = []) {
        if (ids.length === 0) {
            return Promise.resolve([]);
        }

        return Database.load(E_INVOICES, {
            filter: {PROPERTY_ORDER_ID: ids}
        })
    }

    static getOrderRelatedData(id) {
        return callMany({
            invoices: {handler: () => MyDB.loadInvoicesByOrder(id)},
            products: {handler: () => MyDB.getProducts(id)},
            files: {handler: () => MyDB.getFilesBy(id)}
        })
    }

    static getProducts(orderId) {
        return Database.load(E_PRODUCTS, {filter: {PROPERTY_ORDER_ID: orderId}});
    }

    static getFilesBy(entityId, code = null) {
        return Database.load(E_ATTACHMENTS, {filter: {PROPERTY_ENTITY_ID: entityId, CODE: code}});
    }

    static saveFiles(entityId, code, files = []) {
        const requests = {};

        for(let file of files) {
            requests[`addF-${file.ID}`] = [
                'entity.item.add',
                {
                    ENTITY: E_ATTACHMENTS,
                    NAME: file['name'],
                    CODE: code,
                    PROPERTY_VALUES: {
                        SRC: [file['name'], file['src'].split("base64,")[1]],
                        SIZE: file['size'],
                        TYPE: file['type'],
                        ENTITY_ID: entityId,
                    }
                }
            ];
        }
        return callStepBatch(requests);
    }

    static updateFiles(entityId, code, fileField) {
        const newFiles = fileField.value.filter( f => f.new === true );
        const {deleted = []} = fileField.meta;

        return callMany({
            addFiles: {handler: () => MyDB.saveFiles(entityId, code, newFiles)},
            deleteFiles: {handler: () => MyDB.deleteByIds(E_ATTACHMENTS, deleted)}
        });
    }

    static getInvoiceFiles(id) {
        return MyDB.getFilesBy(id, ['invoice', 'invoiceFiles']);
    }

    static getInvoicePayments(id) {
        return Database.load(E_INVOICE_PAYMENTS, {
            filter: {PROPERTY_INVOICE_ID: id}
        })
    }

    static loadRoles() {
        return MyDB.load(E_USERS_ROLES);
    }

    static saveRoles(fields, userIdsByRole = {}) {
        const requests = {};
        Object.keys(fields).forEach( roleId => {
            const field = fields[roleId];
            const userIdsFromField = field.value || [];
            const userIds = userIdsByRole[roleId] || [];

            const newUserIds = userIds.length === 0 ? userIdsFromField : userIdsFromField.filter( userId => userIds.indexOf(userId) === -1 );

            newUserIds.forEach( userId =>
                requests[`addUR-${roleId}-${userId}`] = [
                    'entity.item.add',
                    {
                        ENTITY: E_USERS_ROLES,
                        NAME: 'user role',
                        PROPERTY_VALUES: {
                            ROLE_ID: roleId,
                            USER_ID: userId
                        }
                    }
                ]
            );
        });
        return callStepBatch(requests);
    }

    static getUserRole(userId) {
        return MyDB.loadElement(E_USERS_ROLES, {filter: {PROPERTY_USER_ID: userId}})
            .then( role =>
                role ?
                    Promise.resolve(+role.PROPERTY_VALUES.ROLE_ID):
                    Promise.resolve(-1)
            );
    }

    static getUserIdsByRole(roleId) {

        if (roleId === null) {
            return Promise.resolve([]);
        }

        return MyDB.load(E_USERS_ROLES, {
            filter: {PROPERTY_ROLE_ID: roleId}
        }).then( roles => {
            const userIds = roles.map( role => role.PROPERTY_VALUES.USER_ID );
            return Promise.resolve(userIds);
        })
    }

    static savePayment(params) {
        return MyDB.addItem(E_INVOICE_PAYMENTS, params);
    }
}
export default MyDB;