import React from "react";

// Classes
import Bitrix24 from "core/classes/Bitrix24";

// Api functions
import {normalizeData, settledPromise} from "core/Api";

class MyBX24 extends Bitrix24 {

    static setUserOption(key, option) {
        BX24.userOption.set(key, JSON.stringify(option));
    }

    static getPlacementInfo() {
        const {placement, options = {}} = BX24.placement.info();
        return [placement, options];
    }

    static loadDeals(filter) {
        return MyBX24.callListMethod('crm.deal.list', {filter, select: ["*", "UF_*"]});
    }

    static getDealsByIds(ids = []) {
        if (ids.length === 0) {
            return Promise.resolve(normalizeData([]));
        }

        return MyBX24.loadDeals({ID: ids})
            .then( r => Promise.resolve(normalizeData(r.data())) );
    }

    static searchDeals(phrase) {
        return MyBX24.loadDeals({"%TITLE": phrase})
            .then( response => {
                return Promise.resolve(response.data());
            });
    }

    static getDeal(ID) {
        return settledPromise(() => MyBX24.callMethod('crm.deal.get', {ID}))
            .then( r => {
                if (r.status === 'fulfilled') {
                    return Promise.resolve(r.value.data());
                }
                return Promise.resolve(null);
            });
    }

    static getAppUrl() {
        return MyBX24.getInfo()
            .then( info => {
                return Promise.resolve(`https://${MyBX24.getDomain()}/marketplace/app/${info.ID}`);
            })
    }

    static getTask(ID) {
        return settledPromise(() => MyBX24.callMethod('tasks.task.get', {taskId: ID}) )
            .then( response => {
                if (response.status === 'fulfilled') {
                    return Promise.resolve(response.value.data().task);
                }
                return Promise.resolve(null);
            });
    }

    static closeTask(ID) {
        return settledPromise(() => MyBX24.callMethod('tasks.task.complete', {taskId: ID}));
    }

    static createTask(params) {
        return MyBX24.callMethod('tasks.task.add', {fields: params})
            .then( response => {
                const result = response.data();
                return Promise.resolve(result['task'].id);
            });
    }

    static getDealName = deal => {
        return deal ? deal.TITLE: "Объект не доступен";
    };

    static getDealLink = deal => {
        return deal ? <a href={`https://${Bitrix24.getDomain()}/crm/deal/details/${deal.ID}/`} target={"_blank"}>{deal.TITLE}</a>: "Объект не доступен";
    };
}
export default MyBX24;