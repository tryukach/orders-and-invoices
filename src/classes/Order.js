import moment from "moment";

// Classes
import Database from "./Database";
import Bitrix24 from "./Bitrix24";
import Invoice from "./Invoice";

// Constants
import {
    E_ATTACHMENTS,
    E_ORDERS,
    E_PRODUCTS,
    OBJECT_RESPONSIBLE_KEY
} from "root/constants";
import {callMany, callStepBatch, callStepByStep} from "core/Api";

class Order {

    static isComplete(orderId, invoices = []) {
        if (invoices.length === 0) {
            return false;
        }

        for(let invoice of invoices) {
            if (+invoice.PROPERTY_VALUES.STATUS_ID < 5) {
                return false;
            }
        }
        return true;
    }

    static complete(orderId) {
        return Database.updateItem(E_ORDERS, {
            ID: orderId,
            PROPERTY_VALUES: {
                STATUS_ID: 2
            }
        })
    }

    static saveProducts(orderId, products = []) {
        const requests = {};
        const filledProducts = products.filter( p => p.NAME !== '' );

        for(let product of filledProducts) {
            requests[`addProduct-${product.ID}`] = [
                'entity.item.add',
                {
                    ENTITY: E_PRODUCTS,
                    NAME: product.NAME,
                    PROPERTY_VALUES: {
                        ORDER_ID: orderId,
                        MEASURE_ID: product.MEASURE_ID,
                        COUNT: product.COUNT,
                        COMMENT: product.COMMENT
                    }
                }
            ];
        }
        return callStepBatch(requests);
    }

    static create(fields, {creatorId}) {
        const [placement, {ID}] = Bitrix24.getPlacementInfo();
        const dealId = placement !== "CRM_DEAL_DETAIL_TAB" ? fields['object'].value : ID;

        return Bitrix24.getDeal(dealId)
            .then( deal => {
                const params = {
                    NAME: `Заявка объекта ${dealId}`,
                    DETAIL_TEXT: fields['desc'].value,
                    DATE_ACTIVE_FROM: moment().format(),
                    PROPERTY_VALUES: {
                        DEAL_ID: dealId,
                        STATUS_ID: 1,
                        CREATOR_ID: creatorId,
                        RESPONSIBLE_ID: deal[OBJECT_RESPONSIBLE_KEY] || null,
                        COMMENT: fields['comment'].value
                    }
                };
                return Database.addItem(E_ORDERS, params)
            })
            .then( ID => {
                return callMany({
                    files: {handler: () => Database.saveFiles(ID, 'order', fields['attachments'].value)},
                    products: {handler: () => Order.saveProducts(ID, fields['products'].value)},
                    appUrl: {handler: () => Bitrix24.getAppUrl()},
                    createTask: {
                        handler: ({appUrl}) => {
                            const url = appUrl + `/?action=invoice_create&orderId=${ID}`;
                            return Bitrix24.callMethod('tasks.task.add', {
                                fields: {
                                    TITLE: 'Создание счетов',
                                    DESCRIPTION: `Создайте счета по заявке № ${ID}. [URL=${url}]Перейти к выполнению[/URL]`,
                                    RESPONSIBLE_ID: creatorId,
                                    UF_CRM_TASK: [`D_${dealId}`],
                                    DEADLINE: moment().add(3, 'h').format()
                                }
                            })
                        }
                    }
                }).then( () => Promise.resolve(ID) )
            })
    }

    static edit(fields, orderId) {
        const [placement, {ID}] = Bitrix24.getPlacementInfo();
        const dealId = placement !== "CRM_DEAL_DETAIL_TAB" ? fields['object'].value : ID;

        const params = {
            ID: orderId,
            DETAIL_TEXT: fields['desc'].value,
            PROPERTY_VALUES: {
                DEAL_ID: dealId,
                COMMENT: fields['comment'].value
            }
        };

        return Database.updateItem(E_ORDERS, params)
            .then( () => Database.updateFiles(orderId, 'order', fields['attachments']) )
            .then( () => Order.updateProducts(orderId, fields['products']))
    }

    static updateProducts(orderId, productField) {
        const newProducts = productField.value.filter( p => p.IS_NEW === true );
        const {deleted = []} = productField.meta;
        const updateRequests = {};

        productField.value
            .filter( p => !p.IS_NEW )
            .forEach( p => {
                updateRequests[`updateProduct-${p.ID}`] = [
                    'entity.item.update',
                    {
                        ENTITY: E_PRODUCTS,
                        ID: p.ID,
                        NAME: p.NAME,
                        PROPERTY_VALUES: {
                            MEASURE_ID: p.MEASURE_ID,
                            COUNT: p.COUNT,
                            COMMENT: p.COMMENT
                        }
                    }
                ]
            });

        return callMany({
            updateProducts: {handler: () => callStepBatch(updateRequests)},
            addProducts: {handler: () => Order.saveProducts(orderId, newProducts)},
            deleteProducts: {handler: () => Database.deleteByIds(E_PRODUCTS, deleted)}
        });
    }

    static delete(id) {
        return callMany({
            deleteOrder: {handler: () => Database.deleteItem(E_ORDERS, id)},
            orderData: {handler: () => Database.getOrderRelatedData(id)}
        }).then( response => {
            const {orderData = {}} = response;
            const {products, invoices, files} = orderData;

            return callMany({
                deleteProducts: {handler: () => Database.deleteItems(E_PRODUCTS, products)},
                deleteFiles: {handler: () => Database.deleteItems(E_ATTACHMENTS, files)},
                deleteInvoices: {
                    handler: () => {
                        const ids = invoices.map( i => i.ID );
                        return Invoice.deleteMany(ids);
                    }
                },
            })
        });
    }

    static deleteMany(ids, onSuccess, onError) {
        const promises = ids.map( id => {
            return () => {
                return Order.delete(id)
                    .then(
                        () => onSuccess(`Заявка № ${id} удалена`),
                        error => {
                            console.log(error);
                            onError(`При удалении заявки № ${id} произошла ошибка`);
                        },
                    )
            };
        });

        return callStepByStep(promises);
    }
}
export default Order;