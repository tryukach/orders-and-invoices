import moment from "moment";

// Classes
import Data from "core/classes/Data";
import {DATE_FORMAT} from "root/constants";
import {numberToMoney} from "core/Api";

class MyData extends Data {
    static extractProducts(products = []) {
        return products.map( p => {
            return {
                ID: p.ID,
                NAME: p.NAME,
                COUNT: +p.PROPERTY_VALUES.COUNT,
                MEASURE_ID: +p.PROPERTY_VALUES.MEASURE_ID,
                COMMENT: p.PROPERTY_VALUES.COMMENT
            }
        })
    }

    static extractFiles(files = []) {
        return files.map( f => {
            return {
                ID: f.ID,
                name: f.NAME,
                size: f.PROPERTY_VALUES.SIZE,
                src: f.PROPERTY_VALUES.SRC,
                type: f.PROPERTY_VALUES.TYPE
            }
        })
    }

    static extractDate(date) {
        return date ? moment(date.slice(0,19)).format(DATE_FORMAT) : false;
    }

    static getRoleIdByInvoiceStatus(invoiceStatusId) {
        switch (invoiceStatusId) {
            case 2: return 2;
            case 3: return 1;
            case 4: return 3;
            case 7: return 4;
            default: return null;
        }
    }

    static calcInvoiceSum(invoices = []) {
        return numberToMoney(invoices.reduce((acc, invoice) => acc + parseFloat(invoice.PROPERTY_VALUES.SUM.toString()), 0))
    }
}
export default MyData;
