function appReducer(state = {}, action) {
    let ns;

    switch (action.type) {
        case "APP_INIT": return action.payload;
        case "APP_SET_PAGE":
            const prevPageOptions = action.options || state['prevPageOptions'] || {};
            ns = {
                currentPage: action.page,
                prevPage: state.currentPage,
                prevPageOptions,
                pageOptions: action.options || prevPageOptions,
            }; break;
        case "APP_SET_MESSAGE": ns = {message: action.payload};break;
        case "APP_SET_HINT_VISIBILITY":
            ns = {hints: Object.assign({}, state.hints, {[action.id]: action.visibility})};
            break;
        case "APP_SET_STATUS": ns = {status: action.payload}; break;
        case "APP_SET_CURRENT_ROLE": ns = {currentRole: action.payload}; break;
        case "APP_LOCK": ns = {lock: action.payload}; break;
        case "APP_SET_ADMIN_MODE": ns = {useAdminMode: action.payload}; break;
        default: return state;
    }

    return Object.assign({}, state, ns);
}
export default appReducer;