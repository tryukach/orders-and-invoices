import {combineReducers} from "redux";

// reducers
import app from './app';
import staticData from './staticData';

export default combineReducers({
    app,
    data: () => ({}),
    staticData,
    ui: (state = {}, action) => {
        let ns;
        switch (action.type) {
            case "UI_SAVE": ns = {[action.key]: action.payload}; break;
            default: return state;
        }

        return Object.assign({}, state, ns);
    }
});
