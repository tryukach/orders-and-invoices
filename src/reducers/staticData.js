function staticDataReducer(state = {}, action) {
    
    switch (action.type) {
        case "STATIC_DATA_INIT": return action.payload;
        default: return state;
    }
}
export default staticDataReducer;