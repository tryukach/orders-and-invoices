import {createSelector} from "reselect";

// helpers
function getList(data) {
    return data.allIds.map( ID => data.byId[ID] )
}
function createListSelector(key) {
    return createSelector(
        state => state.data[key],
        getList
    );
}
function createListFilterSelector(listSelector, filterFunc) {
    return createSelector(
        listSelector,
        items => {
            return items.filter( item => filterFunc(item) );
        }
    );
}