import App from "classes/App";

export const loadState = () => {
    const stateCache = getCache();
    return stateCache && !isCacheExpired() ?
        App.initState(stateCache) : App.initState();
};

export const saveState = state => {
    try {
        const serializedState = JSON.stringify({
            staticData: state.staticData,
        });
        localStorage.setItem(App.generateStorageKey('state'), serializedState);
        if (!getCacheDate()) {
            setCacheDate();
        }
    }
    catch (err) {
        // ignore error
    }
};

function getCache() {
    try {
        const serializedState = localStorage.getItem(App.generateStorageKey('state'));
        if (serializedState === null) {
            return undefined;
        }

        return JSON.parse(serializedState);
    }
    catch (err) {
        return undefined;
    }
}

function isCacheExpired(minutes = 300) {
    const milliSeconds = minutes * 60 * 1000;
    const isExpired = Date.now() - getCacheDate() > milliSeconds;
    if (isExpired) {
        setCacheDate();
    }

    return isExpired;
}

function getCacheDate() {
    return +localStorage.getItem(App.generateStorageKey('stateSaveDate'));
}

function setCacheDate() {
    localStorage.setItem(App.generateStorageKey('stateSaveDate'), Date.now() + '');
}