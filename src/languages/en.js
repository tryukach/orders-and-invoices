const phrases = {
    tasks: "Tasks",
    multiButton: "Multi button",
    showBar: "Show bar",
    openPopup: "Open popup",
    action: "Action",
    formInPopup: "Form in popup",
    checkTest: "Check test",
    employees: "Employees",
    page: "Page",
    someMessage: "Some message"
};
export default phrases;