import corePhrases from "core/languages";
const curLang = BX24.getLang();

function getPhrases(language) {
    switch (language) {
        case "en": return require('./en').default;
        default: return require('./ru').default;
    }
}
const phrases = Object.assign({}, corePhrases, getPhrases(curLang));
export default phrases;