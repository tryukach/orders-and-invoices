const phrases = {
    tasks: "Задачи",
    multiButton: "Мульти кнопка",
    showBar: "Показать бар",
    openPopup: "Открыть попап",
    action: "Действие",
    formInPopup: "Форма в попапе",
    checkTest: "Чек тест",
    employees: "Сотрудники",
    page: "Страница",
    someMessage: "Какое-то сообщение",
};
export default phrases;