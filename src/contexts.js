import React from "react";
export const SnackContext = React.createContext(null);
export const TableContext = React.createContext(null);
export const FormContext = React.createContext(null);
export const SelectContext = React.createContext(null);