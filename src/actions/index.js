import * as appActions from "./app";
import * as uiActions from "./ui";
import * as staticDataActions from "./staticData";

export default {
    app: appActions,
    ui: uiActions,
    staticData: staticDataActions
};

export function createAction(actionType, paramsGetter) {

    function actionCreator(...params) {
        return makeAndDispatch(actionCreator.dispatch)(...params);
    }

    const makeAndDispatch = dispatch => (...params) => {
        const action = makeAction(...params);

        return dispatch ?
            dispatch(action):
            action;
    };

    // generate final action
    function makeAction(...params) {
        if (typeof paramsGetter === "function") {
            const payload = paramsGetter(...params);
            return {
                type: actionType,
                ...payload
            };
        }
        return {
            type: actionType,
            payload: params[0]
        }
    }

    // write some api for creator
    actionCreator.assignTo = store => {
        actionCreator.dispatch = store.dispatch;
        return actionCreator;
    };
    return actionCreator;
}
export function assignAll(actions, store) {

    if (Array.isArray(actions)) {
        return actions.map( action => action.assignTo(store) );
    }
    return Object.keys(actions).reduce( (assigns, actionName) => {
        const action = actions[actionName];

        return Object.assign(assigns, {
            [actionName]: typeof action === 'object' ?
                assignAll(action, store):
                action.assignTo(store)
        });
    }, {});
}