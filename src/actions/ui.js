import {createAction} from "actions";

export const saveUI = createAction('UI_SAVE', (key, payload) => ({key, payload}));