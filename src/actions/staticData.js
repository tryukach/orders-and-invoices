import {createAction} from "actions";

export const initStaticData = createAction('STATIC_DATA_INIT');