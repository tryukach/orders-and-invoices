import {createAction} from "actions";

export const setPage = createAction('APP_SET_PAGE', (page, options) => ({page, options}));
export const setCurrentRole = createAction('APP_SET_CURRENT_ROLE');
export const setMessage = createAction('APP_SET_MESSAGE');
export const setHintVisibility = createAction('APP_SET_HINT_VISIBILITY', (id, visibility) => ({id, visibility}));
export const setStatus = createAction('APP_SET_STATUS');
export const lock = createAction('APP_LOCK');
export const initApp = createAction('APP_INIT');