import {normalizeData} from "core/Api";

export const orderStatusesList = [
    {ID: 1, TITLE: "В обработке"},
    {ID: 2, TITLE: "Завершена"},
    {ID: 3, TITLE: "Отклонена"}
];
export const orderStatuses = normalizeData(orderStatusesList);

export const deliveryStatusList = [
    {ID: 1, TITLE: "Доставлено"},
    {ID: 2, TITLE: "Не доставлено"}
];
export const deliveryStatuses = normalizeData(deliveryStatusList);

export const documentStatusesList = [
    {ID: 1, TITLE: "Получены"},
    {ID: 2, TITLE: "Не получены"}
];
export const documentStatuses = normalizeData(documentStatusesList);

export const paymentStatuses = {
    byId: {
        1: {ID: 1, TITLE: "Не оплачен"},
        2: {ID: 2, TITLE: "Частично оплачен"},
        3: {ID: 3, TITLE: "Оплачен"},
    },
    allIds: [1,2, 3]
};

export const invoiceStatusesList = [
    {ID: 1, TITLE: "Новый"},
    {ID: 2, TITLE: "На согл. у гл. инж."},
    {ID: 3, TITLE: "На согл. у ген. дир."},
    {ID: 4, TITLE: "В оплате"},
    {ID: 5, TITLE: "Готовы к поставке"},
    {ID: 6, TITLE: "Контроль поставки"},
    {ID: 7, TITLE: "Сбор документов"},
    {ID: 9, TITLE: "Завершен"},
    {ID: 10, TITLE: "Отклонен"}
];
export const invoiceStatuses = normalizeData(invoiceStatusesList);

export const measuresList = [
    {ID: 1, TITLE: "шт"},
    {ID: 2, TITLE: "кг"},
    {ID: 3, TITLE: "м"}
];
export const measures = normalizeData(measuresList);

export const roleList = [
    {ID: 1, TITLE: "Директор", MULTIPLE: "Y", CODE: 'director'},
    {ID: 2, TITLE: "Инженер", MULTIPLE: "Y", CODE: 'engineer'},
    {ID: 3, TITLE: "Бухгалтер", MULTIPLE: "Y", CODE: 'accountant'},
    {ID: 4, TITLE: "Первичный бухгалтер", MULTIPLE: "Y", CODE: 'firstAccountant'},
];
export const roles = normalizeData(roleList);

export const paymentList = [
    {ID: 1, TITLE: "Полная"},
    {ID: 2, TITLE: "Частичная"}
];
export const paymentTypes = normalizeData(paymentList);