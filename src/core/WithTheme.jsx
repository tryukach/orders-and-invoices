import React from 'react';
import Theme from "core/desktop/Theme";

export default function WithTheme() {
    return Component => {
        return <Theme><Component {...props}/></Theme>;
    }
}