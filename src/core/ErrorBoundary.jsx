import React from "react";

// COMPONENTS
import Message from 'core/desktop/view/Message';
import Typography from '@material-ui/core/Typography';

class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    static getDerivedStateFromError(error) {
        console.log(error);
        return {
            hasError: true,
            message: `Произошла ошибка: ${error.message}`
        };
    }

    render() {
        const {title = ''} = this.props;

        if (this.state.hasError) {
            return (
                <div>
                    {title && <Typography>{title}</Typography>}
                    <Message type={"error"}>{this.state.message}</Message>
                </div>
            );
        }

        return this.props.children;
    }
}

export default ErrorBoundary;