import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const mainColor = "#535c69";
const defaultTheme = {
    palette: {
        type: "light",
        primary: { main: mainColor },
        secondary: { main: '#3bc8f5' }
    },
    typography: {
        useNextVariants: true,
        fontFamily: [
            '"IBMPlexSans"',
            'Arial',
            'Helvetica',
            'sans-serif',
        ].join(','),
        h1: {fontSize: "2.2em"},
        h2: {fontSize: "2em"},
        h3: {fontSize: "1.8em"},
        h4: {fontSize: "1.6em"},
        h5: {fontSize: "1.4em"},
    },
    overrides: {
        MuiTableCell: {
            head: {
                fontWeight: "bold",
                color: "inherit",
                padding: "12px 10px",
                fontSize: 14,
                backgroundColor: "#3bc8f533"
            },
            body: {
                fontSize: 12,
                backgroundColor: "#80808012"
            },
            footer: {
                fontSize: 12,
                color: 'inherit'
            },
            root: {padding: "5px 10px"}
        },
        MuiTableRow: {
            root: {height: "initial"},
            head: {height: "initial"}
        },
        MuiTablePagination: {
            selectIcon: {top: -2}
        },
        MuiPaper: {root: {overflowX: "auto"}},
        MuiTypography: {
            body2: {lineHeight: "initial"}
        },
        MuiFormControl: {
            root: {
                display: "block",
                "&.upload-image": {
                    display: "flex",
                    justifyContent: "flex-start",
                    flexDirection: "row"
                }
            }
        },
        MuiFormHelperText: {
            root: {marginTop: 3}
        },
        MuiFormLabel: {
            root: {fontSize: 14},
            asterisk: {color: "#f44336"}
        },
        MuiInputBase: {
            root: {display: "flex", fontSize: "0.9rem"},
            input: {padding: "4px 0"},
            multiline: {padding: 0},
            inputMultiline: {padding: "4px 0"}
        },
        MuiInputLabel: {
            root: {
                display: 'inline'
            },
            formControl: {
                position: "relative",
                transform: "none"
            },
            shrink: {
                transform: "none"
            }
        },
        MuiInputAdornment:{
            root: {
                height: 24,
                paddingBottom: 2
            },
            positionStart: {marginRight: 3}
        },
        MuiButton: {
            root: {
                borderRadius: 2,
                fontWeight: "bold",
                boxShadow: "none",
                color: "white"
            },
            containedSecondary: {
                color: "white",
                boxShadow: "none",
            },
            outlined: {
                color: "initial",
            },
            sizeSmall: {
                textTransform: "lowercase"
            }
        },
        MuiIconButton: {
            root: {
                color: mainColor,
                padding: 8
            }
        },
        MuiFab: {
            sizeSmall: {
                width: 25,
                height: 25,
                minHeight: 25,
                "& i": {
                    fontSize: 17
                }
            },
            sizeMedium: {
                width: 40,
                height: 40
            }
        },
        MuiListItemIcon: {
            root: {
                marginRight: 7,
                padding: 0,
                color: "white",
                opacity: 0.68
            }
        },
        MuiListItemText: {root: {marginTop: 0, marginBottom: 0}},
        MuiChip: {
            avatarChildren: {
                width: "initial",
                height: "initial"
            },
            root: {
                display: "flex",
                height: "initial",
                alignItems: "flex-start",
                padding: "5px 0",
                margin: 2,
                "& svg": {
                    fontSize: 18
                },
                "& span": {
                    whiteSpace: "normal",
                    padding: "2px 12px"
                },
                "&.with-icon": {
                    alignItems: "center",
                    padding: 0
                }
            }
        },
        MuiCheckbox: {
            root: {
                padding: 0
            }
        },
        MuiAvatar: {
            root: {
                width: 32,
                height: 32
            }
        },
        MuiMenuItem: {
            root: {
                minHeight: 'inherit'
            }
        },
        MuiToolbar: {
            root: {
                alignItems: 'stretch'
            }
        }
    },
    mixins: {
        toolbar: {
            minHeight: 56
        }
    }
};

export default function Theme(props) {
    const {children, type = "light"} = props;

    defaultTheme.palette.type = type;
    const theme = createMuiTheme(defaultTheme);

    return <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>;
}