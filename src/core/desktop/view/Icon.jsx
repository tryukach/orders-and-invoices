import React from 'react';
import cn from "classnames";

export default function Icon(props) {
    const {children, className = '', color, cursor = 'pointer', fontSize = 24, onClick} = props;

    return (
        <i
            className={cn('material-icons', className)}
            style={{color, cursor, fontSize}}
            onClick={e => {
                if (typeof onClick === 'function') {
                    onClick(e);
                }
            }}
        >{children}</i>
    )
}