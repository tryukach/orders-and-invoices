import React from "react";
import App from "classes/App";

// Menus
import HorizontalMenu from "core/desktop/view/menu/Horizontal";

// Actions
import {setPage, setStatus} from "root/actions/app";

function Menu(props) {
    const {currentPage, pages = [], template} = props;

    const Menu = getMenu(template);
    return (
        <Menu pages={pages}
              currentPage={currentPage}
              onMenuClick={page => onMenuClick(page, currentPage)}
        />
    );
}
export default Menu;

function getMenu(template) {
    switch (template) {
        default: return HorizontalMenu;
    }
}
function onMenuClick(page, currentPage) {
    if (page.id !== currentPage) {
        setPage(page.id);
    }
}