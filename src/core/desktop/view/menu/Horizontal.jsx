import React from 'react';
import cn from "classnames";

// Components
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';

export default function Horizontal(props) {
    const { pages, currentPage, onMenuClick } = props;
    return (
        <MenuList className={'menu menu-horizontal'}>
            {pages.map( page =>
                <MenuItem
                    key={page.id}
                    className={cn({'active': currentPage === page.id})}
                    onClick={() => onMenuClick(page)}
                >
                    {page.icon && <i className="material-icons">{page.icon}</i>}
                    <ListItemText className={'menu-item-text'}>{page.title}</ListItemText>
                </MenuItem>
            )}
        </MenuList>
    )
}