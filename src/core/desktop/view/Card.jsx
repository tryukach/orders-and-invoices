import React from 'react';
import {getGridSize} from "core/Api";

// Components
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";

function Card(props) {
    const {grid = 12, background = 'white'} = props;

    const size = getGridSize(grid);
    return (
        <Grid item {...size} className={'card-container'}>
            <Paper elevation={8} className={'card'} style={{background}}>
                <Box display={'flex'}>{props.children}</Box>
            </Paper>
        </Grid>
    );
}
export default Card;