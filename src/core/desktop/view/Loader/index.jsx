import React from 'react';

// Components
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

// Styles
import "./styles.less";

function Loader(props) {
    const {
        size, vertical, horizontal, position, background,
        children,
        textStyle = {color: "white", textShadow: "2px 2px 2px black"}
    } = props;

    let options;
    switch (size) {
        case "small": options = {variant: "caption", size: 20}; break;
        case "large": options = {variant: "h5", size: 40}; break;
        default: options = {variant: "body1", size: 30};

    }

    return (
        <div
            className={'loader'}
            style={{
                position,
                justifyContent: getAlignment(horizontal),
                alignItems: getAlignment(vertical),
                background: getBackground(background)
            }}
        >
            <div>
                {children ?
                    <>
                        <div style={{marginRight: size === "small" ? 10 : 20}}>
                            <CircularProgress size={options.size}/>
                        </div>
                        <Typography style={background ? textStyle : {}} variant={options.variant} className={'loader-title'}>{children}</Typography>
                    </>:
                    <CircularProgress size={options.size}/>
                }
            </div>
        </div>
    );
}
export default Loader;

function getAlignment(align) {
    switch (align) {
        case "top": case "left": return "flex-start";
        case "bottom": case "right": return "flex-end";
        default: return "center"
    }
}
function getBackground(bg) {
    switch (typeof bg) {
        case "string": return bg;
        case "boolean": return 'rgba(0,0,0,0,1)';
        default: return 'transparent';
    }
}