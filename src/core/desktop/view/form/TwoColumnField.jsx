import React from 'react';

// Components
import Label from "./Label";
import Field from "./Field";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import FormHelperText from "@material-ui/core/FormHelperText";

export default function TwoColumnField(props) {
    const {children, labelWidth, fieldWidth, label, ...formFieldProps} = props;
    const {isRequired = false} = formFieldProps;

    return (
        <Field {...formFieldProps}>
            {(inputProps, fieldProps) => {
                const {error} = inputProps;
                return (
                    <Grid container item alignItems={'flex-start'}>
                        <Grid item style={{width: labelWidth}}>
                            <Box mt={0.5}><Label isRequired={isRequired} error={!!error}>{label}</Label></Box>
                        </Grid>
                        <Grid item style={{width: fieldWidth}}>
                            {children(inputProps, fieldProps)}
                            {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
                        </Grid>
                    </Grid>
                )
            }}
        </Field>
    )
}