import React from 'react';

// Components
import InputLabel from '@material-ui/core/InputLabel';
import Help from "core/desktop/view/Help";

export default function Label(props) {
    const {help, children = <span>&nbsp;</span>, isRequired, error} = props;
    return (
        <InputLabel
            className={'field-label'}
            required={isRequired}
            error={error}
        >
            {help ?
                <Help text={help}>{children}</Help>:
                children
            }
        </InputLabel>
    );
}