import React from 'react';
import cn from "classnames";

// Styles
import "./styles.less";

// Components
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import Switch from '@material-ui/core/Switch';
import Help from "core/desktop/view/Help";
import FormHelperText from '@material-ui/core/FormHelperText';

function ToggleContainer(props) {
    const {error, settings = {}} = props;
    const {component = "checkbox"} = settings;

    const ToggleComponent = getComponent(component);
    const ToggleTemplate = getTemplate(settings.multiple);

    return (
        <div>
            <div className={cn('toggle-container', component)}>
                <ToggleTemplate
                    {...props}
                    component={ToggleComponent}
                />
            </div>
            {error && <FormHelperText component={'div'} error={!!error}>{error}</FormHelperText>}
        </div>
    );
}
export default ToggleContainer;

function getComponent(component) {
    switch (component) {
        case "switch": return Switch;
        default: return Checkbox;
    }
}

function getTemplate(multiple) {
    return multiple ? ToggleGroup : Toggle;
}

function Toggle(props) {
    const {value, onChange} = props;
    const ToggleComponent = props.component;

    return (
        <ToggleComponent
            color={"secondary"}
            checked={value}
            onChange={e => onChange(e.target.checked)}
        />
    );
}

function ToggleGroup(props) {
    const {value, settings = {}, onChange} = props;
    const {inline} = settings;
    const ToggleComponent = props.component;

    return (
        <FormGroup row={inline} className={cn('toggle-group', inline ? 'inline' : 'column')}>
            {Object.keys(value).map( ID => {
                const item = value[ID];
                const {title, help, toggle} = item;

                return (
                    <div
                        key={ID}
                        className={'toggle-item'}
                        onClick={() => {
                            const updatedItem = Object.assign({}, item, {toggle: !toggle});
                            onChange(Object.assign({}, value, {[ID]: updatedItem}));
                        }}
                    >
                        <ToggleComponent color={"primary"} checked={toggle}/>
                        {title && <label>{help ? <Help text={help}>{title}</Help>: title}</label>}
                    </div>
                )
            })}
        </FormGroup>
    );
}