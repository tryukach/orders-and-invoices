import React from 'react';

// Components
import Box from "@material-ui/core/Box";
import Button from "core/desktop/view/buttons/Button";

export default function ListField(props) {
    const {onChange, onMetaChange, value: items = [], settings = {}} = props;
    const {component: Component, capacity = 5, getInitialState} = settings;

    if (!Component) {
        throw new Error('Component required');
    }
    return (
        <divs className={'list-container'}>
            {items.map( (item, index) => {
                return (
                    <Component
                        key={item.ID}
                        values={item}
                        onChange={(key, value) => {
                            const updatedItem = Object.assign({}, item, {[key]: value});
                            const updatedFields = items
                                .slice(0, index)
                                .concat(updatedItem)
                                .concat(items.slice(index + 1));

                            onChange(updatedFields);
                        }}
                        onMetaChange={onMetaChange}
                        onDelete={() => onChange(items.del(index))}
                        showDelete={items.length > 1}
                    />
                )
            })}
            {items.length <= capacity && (
                <Box mt={items.length > 0 ? 2 : 0}>
                    <Button
                        settings={{
                            size: "small",
                            promise: false
                        }}
                        onClick={() => {
                            const initialState = getInitialState();
                            onChange(items.concat(initialState));
                        }}
                    >Добавить еще</Button>
                </Box>
            )}
        </divs>
    );
}