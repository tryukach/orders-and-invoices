import React, {useEffect, useState, useReducer} from 'react';
import {SelectContext} from "root/contexts";
import SelectState from "core/classes/states/Select";

// Styles
import "./style.less";

function SelectWrapper(SelectComponent) {

    return props => {
        const [select, setSelect] = useState(null);
        const [,updateDom] = useReducer(updateCount => updateCount + 1, 0);

        useEffect(() => {
            const select = new SelectState({
                open: false,
                searchPhrase: '',
                status: 'initializing',
                selected: []
            }, updateDom);
            setSelect(select);
        }, []);

        if (select === null) {
            return null;
        }

        return (
            <SelectContext.Provider value={select}>
                <SelectComponent {...props} />
            </SelectContext.Provider>
        );
    };
}
export default SelectWrapper;

export function getWidth(container) {
    if (container) {
        const bounds = container.getBoundingClientRect();
        return bounds.width;
    }
    return 'auto';
}