import React from 'react';

// Classes
import Bitrix24 from 'classes/Bitrix24';

// Api functions
import {getUserDisplayName} from "core/Api";

// Components
import DynamicSelect from "core/desktop/view/form/fields/select/DynamicSelect";

export default function UserSelect(props) {
    const {settings = {}, value, onChange, ...otherProps} = props;
    return (
        <DynamicSelect
            value={value}
            onChange={onChange}
            settings={settings}
            loadPackage={(filterParams, packageNumber) => {
                return Bitrix24.loadPackage("user.get", {FILTER: {ACTIVE: true, USER_TYPE: 'employee', ...filterParams}}, packageNumber)
            }}
            getText={user => getUserDisplayName(user)}
            getSearchItems={phrase => {
                return Bitrix24.callListMethod("user.search", {FILTER: {NAME: phrase, LAST_NAME: phrase, ACTIVE: true, USER_TYPE: 'employee'}})
                    .then( response => Promise.resolve(response.data()) );
            }}
            {...otherProps}
        />
    )
}