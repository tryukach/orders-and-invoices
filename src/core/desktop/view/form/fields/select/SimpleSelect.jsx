import React, {useMemo, useContext} from 'react';
import {SelectContext} from "root/contexts";

// Components
import SelectWrapper from "./SelectWrapper";
import SingleSelect from "./SingleSelect";
import MultipleSelect from "./MultipleSelect";

// Api functions
import {normalizeData} from "core/Api";

function SimpleSelect(props) {
    const {value, error, getText, items = [], settings = {}, onSelect, onChange, onInputChange} = props;
    const {multiple, search} = settings;

    const select = useContext(SelectContext);
    useMemo(() => {
        select.setItems(normalizeData(items));
        select.setStatus('ready');
    }, [items.length]);

    const displayItems = select.getSearchItems() || select.getItems();
    const SelectTemplate = getTemplate(multiple);

    return (
        <SelectTemplate
            value={value}
            setValue={value => {
                onChange(value);
                select.setSelected(value);
            }}
            items={displayItems}
            settings={settings}
            error={error}
            getText={ item => {
                return item ? getText(item) : 'undefined';
            }}
            onChange={ inputText => {
                if (search) {
                    if (typeof onInputChange === 'function') {
                        onInputChange(inputText);
                    }

                    select.setSearchPhrase(inputText);
                    select.open();

                    if (inputText.length > 1) {
                        const searchItems = items.filter( item => {
                            return getText(item).toLowerCase().includes(inputText.toLowerCase());
                        });
                        select.setSearchItems(normalizeData(searchItems));
                    }
                    if (inputText.length === 0) {
                        select.setSearchItems(null);
                    }
                }
            }}
            onSelect={ ID => {
                if (typeof onSelect === 'function') {
                    onSelect(ID);
                }
            }}
        />
    );
}
export default SelectWrapper(SimpleSelect);

function getTemplate(multiple) {
    return multiple ? MultipleSelect : SingleSelect;
}
