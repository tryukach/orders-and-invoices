import React, {useRef, useMemo, useContext} from 'react';
import phrases from "core/languages";
import {SelectContext} from "root/contexts";
import cn from "classnames";

// Components
import Input from '../Input';
import Icon from "core/desktop/view/Icon";
import SelectItems from "./SelectItems";
import Box from "@material-ui/core/Box";

function SelectBase(props) {
    const {
        items,
        value,
        displayText,
        error,
        settings,
        getText,
        onChange,
        onSelect,
        onScroll
    } = props;
    const {search, multiple, asPlainText} = settings;

    const containerEl = useRef(null);
    const select = useContext(SelectContext);

    return (
        <div className={cn('select', {'select-plain': asPlainText})}>
            <div
                className={'input-container'}
                ref={containerEl}
                style={{cursor: search ? 'text' : 'pointer'}}
                onClick={() => select.open() }
            >
                {asPlainText && <Box mr={1.5}>{displayText}</Box>}
                <MemoInput
                    value={displayText}
                    type={asPlainText ? 'hidden' : 'select'}
                    readOnly={!search}
                    placeholder={search ? phrases['typeForSearch'] : `${phrases['selectFrom']}...`}
                    disableUnderline={settings.useUnderline}
                    error={error}
                    onChange={onChange}
                    startAdornment={settings.startAdornment}
                />
                <span className={'select-icon'}>
                    <Icon settings={{useHover: false}}>{search ? "search": "arrow_drop_down"}</Icon>
                </span>
            </div>
            <SelectItems
                anchorEl={containerEl.current}
                onSelect={onSelect}
                items={items}
                getText={getText}
                multiple={multiple}
                value={value}
                onScroll={onScroll}
            />
        </div>
    );
}
export default SelectBase;

function MemoInput(props) {
    const {value, meta, error} = props;
    const {status} = useContext(SelectContext).getState();

    return useMemo(() => <Input {...props}/>, [value, meta, error, status])
}