import React from 'react';
import Bitrix24 from 'classes/Bitrix24';

import Field from "core/desktop/view/form/Field";

export default function DepartmentSelect(props) {
    const {settings = {}, title, id} = props;
    return (
        <Field
            id={id}
            type={'select-dynamic'}
            title={title}
            loadPackage={(filterParams, packageNumber) => {
                return Bitrix24.loadPackage("department.get", {filter: filterParams}, packageNumber);
            }}
            getText={d => d.NAME}
            getSearchItems={Bitrix24.searchDepartments}
            settings={settings}
        />
    )
}