import React, {useEffect, useContext} from 'react';
import {SelectContext} from "root/contexts";

// Components
import SelectWrapper from "./SelectWrapper";
import SingleSelect from "./SingleSelect";
import MultipleSelect from "./MultipleSelect";
import LockLoader from "core/desktop/view/LockLoader";

// Api functions
import {normalizeData} from "core/Api";
import phrases from "core/languages";

function DynamicSelect(props) {
    const {value, error, getText, settings = {}, onSelect, onChange, loadPackage, getSearchItems, isReset, onInputChange} = props;
    const {multiple, search} = settings;

    const select = useContext(SelectContext);
    const {searchPhrase, status} = select.getState();

    useEffect(() => select.reset(), [isReset]);
    useEffect(() => {
        const isEmpty = multiple ? value.length === 0 : value === "";
        loadPackage({}).then( response => {

            select.setItems(normalizeData(response.data()));
            select.total = response.total();

            return isEmpty ?
                Promise.resolve():
                loadPackage({ID: multiple ? value: [value]}).then( response => {
                    select.addItems(response.data());
                    return Promise.resolve();
                });
        })
            .then( () => {
                select.setSelected(value);
                select.setStatus('ready');
            });
    }, []);

    const displayItems = select.getSearchItems() || select.getItems();
    const SelectTemplate = getTemplate(multiple);

    return (
        <div style={{position: 'relative'}}>
            {status === 'initializing' && <LockLoader/>}
            <SelectTemplate
                value={value}
                setValue={ value => {
                    onChange(value);
                    select.setSelected(value);
                }}
                items={displayItems || {}}
                settings={settings}
                error={error}
                getText={ item => {
                    return !item ? 'undefined' : getText(item)
                }}
                onChange={ inputText => {
                    if (search) {
                        if (typeof onInputChange === 'function') {
                            onInputChange(inputText);
                        }
                        select.setSearchPhrase(inputText);
                        select.open();

                        if (inputText.length > 1 && status !== "loading") {
                            select.setStatus('loading');
                            getSearchItems(inputText).then( items => {
                                select.setSearchItems(normalizeData(items));
                                select.setStatus('ready');
                            });
                        }
                        if (inputText.length === 0) {
                            select.setSearchItems(null);
                        }
                    }
                }}
                onMetaChange={props['onMetaChange']}
                onSelect={ ID => {
                    const items = select.getItems();

                    if (typeof onSelect === 'function') {
                        onSelect(ID);
                    }
                    if (searchPhrase.length > 0 && !items.byId[ID]) {
                        const searchItems = select.getSearchItems();
                        select.addItem(searchItems.byId[ID]);
                    }
                }}
                onScroll={ e => {
                    if (!searchPhrase) {
                        const isOnBottom = e.target['scrollTop'] === e.target['scrollHeight'] - e.target['offsetHeight'];
                        const nextBatch = select.getBatch() + 1;

                        if (select.total > 50 && nextBatch * 50 < select.total && isOnBottom) {
                            select.setStatus('loading');
                            loadPackage({}, nextBatch).then( response => {
                                select.total = response.total();
                                select.addItems(response.data());
                                select.setBatch(nextBatch);
                                select.setStatus('ready');
                            })
                        }
                    }
                }}
            />
        </div>
    );
}
export default SelectWrapper(DynamicSelect);

function getTemplate(multiple) {
    return multiple ? MultipleSelect : SingleSelect;
}