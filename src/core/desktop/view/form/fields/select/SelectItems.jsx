import React, {useContext, useMemo} from 'react';
import {SelectContext} from "root/contexts";
import cn from "classnames";

// Api functions
import {getWidth} from "./SelectWrapper";

// Components
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import Popper from '@material-ui/core/Popper';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import LockLoader from "core/desktop/view/LockLoader";

function SelectItemsContainer(props) {
    const {
        anchorEl,
        onSelect,
        getText,
        items,
        multiple,
        value,
        onScroll
    } = props;

    const select = useContext(SelectContext);
    const {open, status} = select.getState();

    return (
        <Popper
            anchorEl={anchorEl}
            open={open}
            container={document.getElementById('popper')}
            modifiers={{
                offset: {
                    enabled: true,
                    offset: '0,10'
                },
            }}
        >
            <ClickAwayListener onClickAway={() => select.hide() }>
                <Paper elevation={4}>
                    <div
                        className={'select-items-container'}
                        onScroll={onScroll}
                    >
                        {status === 'loading' && <LockLoader/>}
                        <div className={'select-items-content'}>
                            <div>
                                <SelectItems
                                    items={items}
                                    width={getWidth(anchorEl)}
                                    onSelect={onSelect}
                                    getText={getText}
                                    multiple={multiple}
                                    value={value}
                                />
                            </div>
                        </div>
                    </div>
                </Paper>
            </ClickAwayListener>
        </Popper>
    )
}
export default SelectItemsContainer;

function SelectItems(props) {
    const {value, width, items, onSelect, getText, multiple} = props;

    return (
        <ul className={cn('select-items', {multiple})} style={{width}}>
            {items.allIds.length === 0 && <NotFound>Ничего не найдено</NotFound>}
            {items.allIds.map( ID => {
                const item = items.byId[ID];
                const text = getText(item);
                const checked = multiple && value.indexOf(ID) !== -1;

                return (
                    <MemoListItem
                        key={ID}
                        ID={ID}
                        onClick={() => onSelect(ID)}
                        checked={checked}
                        text={text}
                        multiple={multiple}
                    />
                )
            })}
        </ul>
    )
}

function MemoListItem(props) {
    const {ID, onClick, text, checked, multiple} = props;

    return useMemo(() =>
            <MenuItem value={ID} onClick={onClick}>
                {multiple && <Checkbox color={'secondary'} checked={checked} className={'checkbox'}/>}
                <ListItemText primary={text}/>
            </MenuItem>,
        [checked]
    );
}

function NotFound(props) {
    return (
        <ul>
            <li><Typography variant={'body2'}>{props.children}</Typography></li>
        </ul>
    )
}