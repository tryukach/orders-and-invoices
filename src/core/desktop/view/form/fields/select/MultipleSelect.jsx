import React, {useContext} from 'react';
import {SelectContext} from "root/contexts";

// Components
import SelectBase from "./SelectBase";
import Chip from "@material-ui/core/Chip";

// Api functions
import {select} from "core/Api";

export default function MultipleSelect(props) {
    const {setValue, onSelect, ...selectProps} = props;
    const {value, items, getText, getId} = selectProps;

    const selectClass = useContext(SelectContext);
    const {searchPhrase, status} = selectClass.getState();

    return (
        <div className={'select-multi'}>
            <SelectBase
                {...selectProps}
                displayText={searchPhrase || ''}
                onSelect={ ID => {
                    const selected = selectClass.getSelected();

                    setValue(select(selected, ID));
                    onSelect(ID);
                }}
            />
            {status === 'ready' && (
                <Chips
                    value={value}
                    items={items}
                    getText={getText}
                    getId={getId}
                    onDelete={ ID => {
                        const selected = selectClass.getSelected();
                        setValue(select(selected, ID));
                    }}
                />
            )}
        </div>

    )
}

function Chips(props) {
    const {value = [], getText, items, onDelete} = props;
    return value.length > 0 && (
        <div className={"chips"}>
            {value.map( ID => {
                const item = items.byId[ID];
                const text = getText(item);
                return (
                    <Chip
                        key={ID}
                        label={text}
                        className={"chip"}
                        onDelete={() => onDelete(ID)}
                    />
                )
            })}
        </div>
    )
}