import React, {useContext} from 'react';
import {SelectContext} from "root/contexts";

// Components
import SelectBase from "./SelectBase";

export default function SingleSelect(props) {
    const {onSelect, setValue, ...selectProps} = props;
    const {value: ID, items, getText, settings = {}} = selectProps;

    const select = useContext(SelectContext);
    const {searchPhrase, status} = select.getState();

    const isSearching = settings.search && searchPhrase.length > 0;

    let displayText = '';
    if (ID && !isSearching && status === 'ready') {
        const currentItem = items.byId[ID];
        displayText = getText(currentItem);
    }

    return (
        <SelectBase
            {...selectProps}
            displayText={isSearching ? searchPhrase : displayText}
            onSelect={ ID => {
                
                setValue(ID);
                onSelect(ID);

                select.setSearchPhrase('');
                select.setSearchItems(null);
                select.hide();
            }}
        />
    )
}