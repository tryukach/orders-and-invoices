import React, {useRef} from 'react';
import cn from "classnames";
import uuid from "uuid";
import phrases from "core/languages";

import './styles.less';

// Components
import Input from '@material-ui/core/Input';
import Button from 'core/desktop/view/Button';
import Icon from 'core/desktop/view/Icon';
import Tooltip from "core/desktop/view/Tooltip";

// Images
import pdfIcon from "images/pdf.svg";
import wordIcon from "images/word.svg";
import pptxIcon from "images/pptx.svg";
import fileIcon from "images/file.svg";
import imageIcon from "images/image.svg";
import xlsxIcon from "images/xlsx.svg";
import textIcon from "images/text.svg";

export function renderFiles(files = []) {

    return (
        <div className={'files-container'}>
            <div className={'file-items'}>
                {files.map( file => {
                    const {TYPE, SRC} = file.PROPERTY_VALUES;
                    return (
                        <div className={"file-item"} key={file.ID}>
                            <div className={'file-name'}>
                                <img src={getFileIcon(TYPE)}/>
                                <div><a href={SRC} target={"_blank"}>{file.NAME}</a></div>
                            </div>
                            <div className={'file-buttons'}>
                                <Tooltip title={'Скачать файл'}>
                                    <Icon
                                        onClick={() => window.open(`download.php?url=${SRC}`, "_self")}
                                    >cloud_download</Icon>
                                </Tooltip>
                            </div>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

function getTemplateComponent(template) {
    switch (template) {
        case "tile": return TileTemplate;
        default: return DefaultTemplate;
    }
}

function saveFile(file, props) {
    const {onChange, value} = props;
    const reader = new FileReader();

    if (file) {
        reader.readAsDataURL(file);
        reader.onload = function (e) {

            onChange(value.concat({
                ID: uuid(),
                new: true,
                name: file.name,
                size: file.size,
                src: e.target['result'],
                type: file.type
            }));
        };
    }
}

function FileLoader(props) {
    const {error, settings, value, meta, onChange, onMetaChange, disabled} = props;
    const {mime="*", multiple, template, maxSize = 5} = settings;
    const {deleted = []} = meta;

    const inputEl = useRef(null);
    const TemplateComponent = getTemplateComponent(template);
    const btnTitle = value.length > 0 ? phrases['loadMore'] : phrases['loadFile'];
    const showBtn = (value.length === 0 && !multiple) || (multiple && value.length < maxSize);

    return (
        <div className={'files-container'}>
            {value.length > 0 && (
                <div className={'file-items'}>
                    <TemplateComponent
                        {...props}
                        onDelete={(file, index) => {
                            onChange(value.del(index));
                            if (!file.new) {
                                onMetaChange({deleted: deleted.concat(file.ID)});
                            }
                        }}
                    />
                </div>
            )}
            {showBtn && (
                <Button
                    className={'upload-button'}
                    color={'secondary'}
                    onClick={() => inputEl.current.click()}
                    disabled={disabled}
                >{btnTitle}</Button>
            )}
            <Input
                type={"file"}
                value={''}
                inputRef={inputEl}
                className={"hidden"}
                inputProps={{accept: mime}}
                onChange={ e => {
                    const file = e.target.files[0];
                    saveFile(file, props);
                }}
            />
        </div>
    );
}
export default FileLoader;

function TileTemplate(props) {
    const {value: files} = props;
    return files.map( (file, index) =>
        <div className={cn("file-item", "file-tile")} key={file.ID}>
            <img src={file.src}/>
            <FileButtons file={file} onDelete={() => props.onDelete(file, index)}/>
        </div>
    )
}

function DefaultTemplate(props) {
    const {value: files, onDelete} = props;

    return files.map( (file, index) =>
        <div className={"file-item"} key={file.ID}>
            <div className={'file-name'}>
                <img src={getFileIcon(file.type)}/>
                {file.new ?
                    <div>{file.name}</div> :
                    <div><a href={file.src} target={"_blank"}>{file.name}</a></div>
                }
            </div>
            <FileButtons
                file={file}
                onDelete={() => {
                    onDelete(file, index);
                }}
            />
        </div>
    )
}

function FileButtons(props) {
    const {file, onDelete} = props;
    return (
        <div className={'file-buttons'}>
            {!file.new && (
                <Icon
                    onClick={() => window.open(`download.php?url=${file.src}`, "_self")}
                >cloud_download</Icon>
            )}
            <Button
                className={'btn-delete'}
                onClick={onDelete}
            ><Icon>close</Icon></Button>
        </div>
    )
}

function getFileIcon(fileType) {
    switch (fileType) {
        case "application/pdf": return pdfIcon;
        case "application/msword":
        case "application/vnd.openxmlformats-officedocument.wordprocessingml.document": return wordIcon;
        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": return xlsxIcon;
        case "application/vnd.openxmlformats-officedocument.presentationml.presentation": return pptxIcon;
        case "text/plain": return textIcon;
    }

    if (fileType.includes("image/")) {
        return imageIcon;
    }
    return fileIcon;
}