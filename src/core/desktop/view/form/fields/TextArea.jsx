import React, {useState} from 'react';

// Components
import Input from "./Input";

export default function TextArea(props) {
    const {maxRows = 8} = props;

    return (
        <Input
            {...props}
            multiline={true}
            rowsMax={maxRows}
        />
    )
}