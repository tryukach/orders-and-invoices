import React from "react";

// Components
import MuiInput from '@material-ui/core/Input';

export default function Input(props) {
    const {value = '', meta = {}, error, type = 'text'} = props;

    return (
        <MuiInput
            {...props}
            type={type}
            value={value}
            error={!!error}
            onChange={ e => props.onChange(e.target.value)}
        />
    );
}