import React, {useContext, useEffect, useMemo} from "react";
import {FormContext} from "root/contexts";

// Components
import Label from "./Label";

function Field(props) {
    const {name, defaultValue = '', children, format, onChange, help, isRequired = false, label, validate} = props;
    const form = useContext(FormContext);

    const formField = form.getField(name);
    const meta = formField ? formField.meta : {};
    const error = form.getFieldError(name);

    const value = formField ? formField.value : defaultValue;

    useEffect(() => {
        if (!formField) {
            form.addField(name, {
                value,
                initialValue: value,
                meta: {required: isRequired},
                validate,
            });
        }
    }, []);

    return useMemo(() =>
            <>
                {label && (
                    <Label
                        help={help}
                        error={!!error}
                        isRequired={isRequired}
                    >{label}</Label>
                )}
                {children(
                    {
                        name,
                        value,
                        onChange: value => {
                            if (typeof format === 'function') {
                                value = format(value);
                            }

                            if (typeof validate === 'function') {
                                const error = validate(value);
                                error ?
                                    form.setError(name, error):
                                    form.setError(name, undefined);
                            }

                            if (typeof onChange === "function") {
                                onChange(value);
                            }
                            form.setFieldValue(name, value);
                        },
                        error,
                        meta,
                    },
                    {
                        isReset: form.isReset,
                        onMetaChange: nextMeta => form.setFieldMeta(name, nextMeta)
                    }
                )}
            </>
        , [value, error, meta, form.isReset]);
}
export default Field;