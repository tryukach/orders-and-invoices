import React from 'react';


// Components
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import InputLabel from '@material-ui/core/InputLabel';

export default function FormGroup(props) {
    const {spacing = 2, title, size, isRequired} = props;

    return (
        <div className={'form-group'}>
            {title && (
                <div className={'form-group-title'}>
                    <Typography variant={'h4'}>
                        {title}
                        {isRequired && <InputLabel required={isRequired} className={'asterisk'}/>}
                    </Typography>
                </div>
            )}
            {props.children}
        </div>
    )
}