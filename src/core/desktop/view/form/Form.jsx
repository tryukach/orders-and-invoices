import React, {useState, useEffect, useReducer} from "react";
import {FormContext} from "root/contexts";

// Classes
import FormState from "core/classes/states/Form";

import "./styles.less";

function Form(props) {
    const {children, onError, id, settings = {}, onSuccess, onSubmit, onDismount, onReset, onInitialize} = props;

    const [,updateDom] = useReducer(updateCount => updateCount + 1, 0);
    const [form, setForm] = useState(null);

    useEffect(() => {
        const form = new FormState(id, updateDom);
        setForm(form);

        if (typeof onInitialize === 'function') {
            onInitialize(form);
        }

        if (typeof onDismount === 'function') {
            return () => onDismount(form);
        }
    }, []);

    if (form === null) {
        return null;
    }

    const state = form.getState();
    const isSubmitting = state.status === 'submitting';

    return (
        <form
            onSubmit={e => {
                e.preventDefault();
                if (typeof onSubmit !== 'function') {
                    return;
                }
                form.submit(onSubmit, onSuccess, onError, settings.resetAfterSubmit);
            }}
            onReset={() => {
                if (typeof onReset === 'function') {
                    onReset(form);
                    return;
                }
                form.reset();
            }}
        >
            <FormContext.Provider value={form}>
                {children(form, settings, isSubmitting)}
            </FormContext.Provider>
        </form>
    );
}
export default Form;