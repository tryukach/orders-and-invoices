import React, {useState, useMemo} from 'react';
import withWidth from '@material-ui/core/withWidth';
import ReactResizeDetector from 'react-resize-detector';

// Components
import Popover from '@material-ui/core/Popover';
import Backdrop from '@material-ui/core/Backdrop';
import Box from '@material-ui/core/Box';

function Popup(props) {
    const {
        open = false, children, width: breakpoint,
        size = {}, id, transformOrigin = {}, fixedWidth, onClose
    } = props;

    const [privateActions, setPrivateActions] = useState(null);
    const [resize, setResize] = useState(false);
    const containerWidth = fixedWidth || size[breakpoint] || getDefaultWidth(breakpoint);
   
    return useMemo(() => {
        const anchorEl = document.querySelector(`div[data-popup="${id}"]`);
        return (
            <>
                <Backdrop open={open} style={{zIndex: 1300}}/>
                <Popover
                    open={open}
                    onClose={onClose}
                    anchorEl={anchorEl}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right'
                    }}
                    transformOrigin={{
                        vertical: transformOrigin.vertical || 5,
                        horizontal: transformOrigin.horizontal || -15
                    }}
                    action={ actions => {
                        if (actions && actions.updatePosition && !privateActions) {
                            setPrivateActions(actions);
                            actions.updatePosition();
                        }
                    }}
                >

                    <Box
                        className={'popup-container'}
                        p={3.5}
                        width={containerWidth}
                        style={{maxWidth: "100%"}}
                    >
                        {children}
                        <ReactResizeDetector
                            handleHeight
                            onResize={(w,h) => {
                                console.log(w,h, privateActions);
                                if (privateActions && privateActions.updatePosition) {
                                    privateActions.updatePosition();
                                    setResize(!resize);
                                }
                            }}
                        />
                    </Box>
                </Popover>
            </>
        )
    }, [open, resize, privateActions]);
}
export default withWidth()(Popup);

function getDefaultWidth(breakpoint) {
    switch (breakpoint) {
        case "xl": return 600;
        case "lg": return 550;
        case "md": return 500;
        case "sm": return 450;
        case "xs": return 400;
    }
}

export function PopupButton(props) {
    const {id, children} = props;
    return <div data-popup={id} style={{display: 'inline-flex'}}>{children}</div>;
}