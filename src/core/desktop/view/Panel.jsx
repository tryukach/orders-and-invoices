import React from 'react';

// Components
import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';

function Panel(props) {
    const { open, closePanel, title="Заголовок", children, icon } = props;
    return (
        <ExpansionPanel expanded={open} onChange={() => closePanel()} className={"panel"}>
            <ExpansionPanelSummary expandIcon={<i className={'material-icons'}>expand_more</i>}>
                {icon && <span className={'panel-icon'}>{icon}</span>}
                <Typography variant={'h4'}>{title}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={'panel-detail'}>
                {children}
            </ExpansionPanelDetails>
        </ExpansionPanel>
    );
}
export default Panel;