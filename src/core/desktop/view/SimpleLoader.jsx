import React from 'react';
import Loader from "./Loader";

export default function SimpleLoader(props) {
    return (
        <Loader
            size={'small'}
            vetical={'top'}
            horizontal={'left'}
        >{props.children}</Loader>
    )
}