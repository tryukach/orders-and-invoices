import React from 'react';

// COMPONENTS
import PopoverMui from '@material-ui/core/Popover';

function Popover(props) {
    const { open, anchorEl, children, onClose,
        anchorOrigin = {vertical: "top", horizontal: "left"},
        transformOrigin = {vertical: "bottom", horizontal: "right"}
    } = props;

    return (
        <PopoverMui
            open={open}
            onClose={() => onClose()}
            anchorEl={anchorEl}
            anchorOrigin={anchorOrigin}
            transformOrigin={transformOrigin}
        >
            <div className={'popover-container'}>
                {typeof children === 'function' ? children(props) : children}
            </div>
        </PopoverMui>
    );
}
export default Popover;