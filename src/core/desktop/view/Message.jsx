import React from "react";
import classNames from "classnames";

// Components
import Button from "core/desktop/view/Button";
import Typography from "@material-ui/core/Typography";

function Message(props) {
    const {type, useButton, onClick, buttonTitle = 'ok'} = props;

    return (
        <div className={'app-message-container'}>
            <div className={classNames("app-message", type)}>
                <i className={'material-icons'}>{getIcon(type)}</i>
                <Typography variant={'body1'}>{props.children}</Typography>
            </div>
            {useButton && <Button onClick={onClick}>{buttonTitle}</Button>}
        </div>
    );
}
export default Message;

function getIcon(type) {
    switch (type) {
        case "info": return "info_outline";
        case "warning": return "warning";
        default: return "error_outline";
    }
}