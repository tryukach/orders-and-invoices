import React from "react";

// COMPONENTS
import MuiBar from '@material-ui/core/AppBar';
import Toolbar from "@material-ui/core/Toolbar";

function AppBar(props) {
    return (
        <MuiBar position={'relative'}>
            <Toolbar variant={'dense'} disableGutters={true} className={'app-bar'}>
                {props.children}
            </Toolbar>
        </MuiBar>
    );
}
export default AppBar;