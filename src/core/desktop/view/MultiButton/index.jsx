import React, {useState} from 'react';
import uuid from "uuid";
import "./styles.less";

// Components
import Button from '../Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Paper from '@material-ui/core/Paper';
import Fade from '@material-ui/core/Fade';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from "core/desktop/view/Icon";

export default function MultiButton(props) {
    const { actions = [], defaultActionIndex = 0, ...baseButtonProps} = props;
    const { size = 'medium', color = 'secondary'} = baseButtonProps;
    const [open, setOpen] = useState(false);

    const defaultAction = actions[defaultActionIndex] || {};

    return (
        <div className={'btn-multi'}>
            <Button {...baseButtonProps} color={color} onClick={defaultAction.onClick}/>
            <Button
                className={'btn-drop'}
                size={size}
                color={color}
                onClick={() => setOpen(true)}
            ><Icon>arrow_drop_down</Icon></Button>
            {open && (
                <ClickAwayListener onClickAway={() => setOpen(false)}>
                    <Fade in={true} timeout={500}>
                        <Paper square={true} className={'actions'}>
                            {actions.map( action =>
                                <MenuItem
                                    key={uuid()}
                                    onClick={() => {
                                        setOpen(false);
                                        if (typeof action.onClick === "function") {
                                            action.onClick();
                                        }
                                    }}
                                >
                                    <ListItemText
                                        primary={action.title}
                                        primaryTypographyProps={{variant: "body2"}}
                                    />
                                </MenuItem>
                            )}
                        </Paper>
                    </Fade>
                </ClickAwayListener>
            )}
        </div>
    )
}