import React from 'react';

// Components
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import MuiPopper from "@material-ui/core/Popper";
import Paper from "@material-ui/core/Paper";

export default function Popper(props) {
    const {open = false, anchorEl, offset = '0,5', onClose} = props;

    return (
        <MuiPopper
            open={open}
            anchorEl={anchorEl}
            container={document.getElementById('popper')}
            modifiers={{
                offset: {
                    enabled: true,
                    offset
                },
            }}
        >
            <ClickAwayListener onClickAway={onClose}>
                <Paper elevation={4}>
                    {props.children}
                </Paper>
            </ClickAwayListener>
        </MuiPopper>
    )
}