import React from 'react';

// Components
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

export default function Group(props) {
    const {title, children, spacing = 1, justifyContent, alignItems, ...boxProps} = props;

    return (
        <Box {...boxProps} display={boxProps.display || 'flex'}>
            {title && <Box mb={1}><Typography variant={'h4'}>{title}</Typography></Box>}
            <Grid
                container
                spacing={spacing}
                justify={justifyContent}
                alignItems={alignItems}
            >
                {children.map( (child, i) =>
                    <Grid key={i} item>{child}</Grid>
                )}
            </Grid>
        </Box>
    )
}