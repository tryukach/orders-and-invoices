import React from 'react';
import {SUPPORT_URL} from "root/constants";
import langPhrases from "core/languages";

// Components
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import Tooltip from '@material-ui/core/Tooltip';
import Hidden from '@material-ui/core/Hidden';

// Images
import LogoIcon from "images/logo.png";

function Contacts() {
    return (
        <div className={'app-contacts'}>
            <ul>
                <ContactItem title={langPhrases['contactEmail']} icon={"email"} label={"tech@egza24.ru"} href={"mailto:tech@egza24.ru"}/>
                <ContactItem title={langPhrases['contactPhone']} icon={"phone"} label={"+ 7 (978) 743-98-25"} href={"tel:+79787439825"}/>
                <ContactItem title={langPhrases['contactQuestion']} icon={"supervisor_account"} label={langPhrases['contactSupport']} href={SUPPORT_URL} blank/>
                <ContactItem title={langPhrases['contactApps']} icon={"apps"} label={langPhrases['contactAppsLabel']} href={"https://www.bitrix24.ru/apps/?partner_id=171372&search=Y"} blank/>
            </ul>
            <Hidden smDown={true}>
                <a href="https://www.bitrix24.ru/partners/?ID=171372&city%5B0%5D=1061802" target={"_blank"}>
                    <Tooltip title={langPhrases['contactAuthor']}>
                        <img className={'logo'} src={LogoIcon} alt={langPhrases['companyName']}/>
                    </Tooltip>
                </a>
            </Hidden>
        </div>
    );
}
export default Contacts;

function ContactItem(props) {
    const {title, label, href, icon, blank} = props;
    return (
        <li>
            <Tooltip title={title}>
                <a href={href} target={blank ? "_blank" : "_self"}>
                    <Chip className={'contact-item'} label={label} avatar={getContactAvatar(icon)} clickable/>
                </a>
            </Tooltip>
        </li>
    )
}

function getContactAvatar(icon) {
    return <Avatar><i className={'material-icons'}>{icon}</i></Avatar>
}