import React from 'react';

// Components
import Icon from "core/desktop/view/Icon";
import Box from "@material-ui/core/Box";

export default function(props) {
    const {text, children} = props;
    return (
        <Box
            display={'inline-flex'}
            alignItems={'center'}
            className={'help'}
        >
            <span>{children}</span>
            <Box ml={0.625}>
                <Icon
                    settings={{
                        useHover: false,
                        fontSize: 22,
                        size: "small",
                        tooltip: text,
                        tooltipPlacement: 'right',
                    }}
                >help</Icon>
            </Box>
        </Box>
    );
}