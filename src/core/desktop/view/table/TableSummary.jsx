import React from 'react';

// Components
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

// Api functions
import {filterColumns} from "core/desktop/view/table/TableWrapper";

export default function TableSummary(props) {
    const {items = [], columns, onSummary, useSelect, useNumbering,  useContextMenu} = props;
    const sortedColumns = filterColumns(columns);

    return (
        <TableRow>
            {useSelect && <TableCell variant={'footer'}/>}
            {useNumbering && <TableCell variant={'footer'}/>}
            {sortedColumns.map( column => {
                const displayValue = onSummary(column.id, items);
                return <TableCell key={column.id} variant={'footer'}>{displayValue}</TableCell>;
            })}
            {useContextMenu && <TableCell variant={'footer'}/>}
        </TableRow>
    )
}