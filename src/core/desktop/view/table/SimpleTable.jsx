import React, {useMemo, useContext} from 'react';
import {TableContext} from "root/contexts";

// Api functions
import {sortRows} from "./TableWrapper";

// Components
import TableWrapper from "./TableWrapper";
import Table from "./Table";
import Box from "@material-ui/core/Box";
import LockLoader from "core/desktop/view/LockLoader";

function SimpleTable(props) {
    const {
        settings,
        items,
        getCellValue,
        onRowDetail,
        actions = [],
        contextMenuActions = [],
        children
    } = props;

    const table = useContext(TableContext);
    const state = table.getState();

    useMemo(() => {
        const sortedItems = sortRows(items, state.order, item => getCellValue(state.orderBy, item));
        table.reset();
        table.setRows(sortedItems);
        table.render();
    }, [items.length]);

    const rows = table.getRows();
    return (
        <Box
            display={'flex'}
            width={settings.width || '100%'}
            style={{position: 'relative'}}
        >
            {children && children(table)}
            {state.status !== "ready" && <LockLoader/>}
            <Table
                rows={rows}
                getId={getId}
                title={props.title}
                settings={{
                    ...settings,
                    useContextMenu: settings.useContextMenu && contextMenuActions.length > 0,
                    useFooter: actions.length > 0 || settings.usePagination,
                    useDetail: settings.useDetail && typeof onRowDetail === 'function'
                }}
                columns={props.columns}
                getCellValue={getCellValue}
                getPageRows={ items => {
                    return settings.usePagination && typeof state.perPage !== "string" ?
                        items.slice( (state.currentPage - 1) * state.perPage, state.currentPage * state.perPage):
                        items;
                }}
                sortData={ (order, column) => {
                    const rows = table.getRows();
                    const sortedRows = sortRows(rows, order, item => getCellValue(column.id, item));

                    table.setRows(sortedRows);
                    return Promise.resolve();
                }}
                onRowDetail={onRowDetail}
                actions={actions}
                actionHandler={ action => {
                    table.setStatus('loading');
                    return action.handler(state.selected).then( () => {

                        switch (action['type']) {
                            case "delete":
                                state.selected.forEach(id => table.deleteRowById(id));
                                break;
                        }

                        table.clearTable();
                        table.render();
                        return Promise.resolve();
                    });
                }}
                contextMenuActions={contextMenuActions}
                contextMenuActionHandler={(action, index) => {
                    table.setStatus('loading');
                    return action.handler(table.getRow(index)).then( payload => {
                        switch (action['type']) {
                            case "delete": table.deleteRow(index); break;
                            case "update": table.updateRow(index, payload); break;
                        }
                        table.clearTable();
                        table.render();
                        return Promise.resolve();
                    } )
                }}
                setPage={ page => {
                    table.setPage(page);
                    table.clearTable();
                    table.render();
                }}
                getAll={() => Promise.resolve()}
                rowsCount={rows.length}
                summary={props.summary}
            />
        </Box>
    )
}
export default TableWrapper(SimpleTable);

function getId(item) {
    return item.ID || item.id;
}