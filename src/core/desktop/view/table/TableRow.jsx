import React, {useContext, useState, useMemo} from 'react';
import {TableContext} from "root/contexts";

// Api functions
import {select} from "core/Api";
import {filterColumns, getCellText} from "./TableWrapper";

// Components
import MuiTableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Checkbox from '@material-ui/core/Checkbox';
import Icon from 'core/desktop/view/Icon';
import Box from "@material-ui/core/Box";

export default function TableRow(props) {
    const {
        index,
        item,
        ID,
        useContextMenu,
        useRowHover,
        useSelect,
        useNumbering,
        useDetail,
        columns,
        getCellValue,
        onRowDetail,
        useRowContextMenu
    } = props;

    const [showDetail, setShowDetail] = useState(false);

    const table = useContext(TableContext);
    const {selected, currentPage, perPage, order, orderBy} = table.getState();
    const isRowUpdated = table.isRowUpdated(ID);

    const isRowSelected = selected.indexOf(ID) !== -1;
    const sortedColumns = filterColumns(columns);

    return useMemo( () =>
        <>
            <MuiTableRow
                hover={useRowHover}
                onDoubleClick={() => {
                    if (useDetail) {
                        setShowDetail(!showDetail);
                    }
                }}
            >
                {useSelect && (
                    <TableCell variant={'body'}>
                        <Checkbox
                            checked={isRowSelected}
                            onChange={() => {
                                table.setSelected(select(table.getSelected(), ID));
                            }}
                        />
                    </TableCell>
                )}
                {useNumbering && <TableCell variant={'body'}>{ getNumberOffset(currentPage, perPage) + index + 1 }</TableCell>}
                {sortedColumns.map( column => {
                    const cellValue = getCellValue(column.id, item, (id, props) => table.updateRow(id, props));
                    const cellText = getCellText(cellValue);
                    const key = `${ID}-${column.id}`;

                    return <TableCell key={key} variant={'body'}>{cellText}</TableCell>;
                })}
                {useContextMenu && (
                    <TableCell variant={'body'}>
                        {useRowContextMenu ?
                            <Box
                                display={'flex'}
                                alignItems={'center'}
                                justifyContent={'flex-end'}
                            >
                                <Icon
                                    onClick={ e => {
                                        table.setContextMenuOptions({
                                            open: true,
                                            anchorEl: e.target,
                                            index
                                        })
                                    }}
                                >more_horiz</Icon>
                            </Box>:
                            ''
                        }
                    </TableCell>
                )}
            </MuiTableRow>
            {showDetail && useDetail && (
                <tr>
                    <td colSpan={'100%'}>
                        <div className={'row-detail'}>{onRowDetail(item)}</div>
                    </td>
                </tr>
            )}
        </>
    , [isRowSelected, order, orderBy, isRowUpdated]);
}

// helper
function getNumberOffset(currentPage, perPage) {
    return (currentPage - 1) * perPage;
}