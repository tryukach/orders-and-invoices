import React, {useReducer, useState, useEffect} from 'react';
import langPhrases from "core/languages";
import {TableContext} from "root/contexts";

// Classes
import TableState from "core/classes/states/Table";

// Styles
import "./styles.less";

const defaultSettings = {
    notFound: langPhrases['dataNotFound'],
    usePagination: true,
    perPage: 10,
    order: 'desc',
    orderBy: 1,
    perPageOptions: [5,10,20,50,100],
    useRowHover: true,
    useNumbering: false,
    useSelect: true,
    fullWidth: 'auto',
    useDetail: false,
    useContextMenu: false,
    useEntitySort: true
};

// Components
import Help from "core/desktop/view/Help";

function TableWrapper(TableComponent) {

    return props => {

        const {id} = props;
        const settings = Object.assign({}, defaultSettings, props.settings);

        const [,updateDom] = useReducer(updateCount => updateCount + 1, 0);
        const [table, setTable] = useState(null);

        useEffect(() => {
            const table = new TableState({
                id,
                status: "ready",
                order: settings.order,
                orderBy: settings.orderBy,
                perPage: settings.perPage,
                selected: [],
                currentPage: 1,
                contextMenuOptions: {
                    open: false,
                    anchorEl: null
                },
                actionId: ''
            }, updateDom);

            setTable(table);
        }, []);

        if (table === null) {
            return null;
        }

        return (
            <TableContext.Provider value={table}>
                <TableComponent
                    {...props}
                    settings={settings}
                />
            </TableContext.Provider>
        )
    };
}
export default TableWrapper;

export function sortRows(items, order, getValue) {
    return items.sort((a, b) => {
        const aValue = getValue(a);
        const bValue = getValue(b);

        const aSortValue = typeof aValue === 'object' ? aValue.source : (aValue === '-' ? 0 : aValue);
        const bSortValue = typeof bValue === 'object' ? bValue.source : (bValue === '-' ? 0 : bValue);

        const result = aSortValue === bSortValue ? 0 : aSortValue < bSortValue ? -1 : 1;
        return result === 0 ? 0 : order === "desc" ? -result : result;
    });
}
export function sortColumns(a, b) {
    const aSort = a.sort || 100;
    const bSort = b.sort || 100;

    return bSort - aSort;
}
export function filterColumns(columns) {
    return columns.sort(sortColumns)
        .filter( column => {
            return typeof column['isHidden'] === 'function' ?
                !column.isHidden() : !column.isHidden
        })
}
export const findColumnById = (columns, id) => {
    return columns.find( column => column.id === id );
};

export function getCellHeadText(column) {
    return (
        <div className={'cell-text'}>
            <div>
                {column['help'] ?
                    <Help text={column['help']}>{column.text}</Help>:
                    column.text
                }
                {column.measure && <span className={'cell-measure'}>, {column.measure}</span>}
            </div>
            {column['subText'] && <span className={'sub-text'}>{column['subText']}</span>}
        </div>
    )
}

export function getCellText(cellValue) {

    if (isEmpty(cellValue)) {
        return '-';
    }
    return typeof cellValue === 'object' && cellValue.hasOwnProperty('display') ? cellValue['display'] : cellValue;
}

function isEmpty(cellValue) {
    return cellValue === '' || (!cellValue && cellValue !== 0);
}

export function getPageItems(perPageOptions = []) {
    return perPageOptions.map( p => ({ID: p, TITLE: p}) );
}