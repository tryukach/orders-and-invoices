import React from 'react';

// Components
import TableHead from "./Head";
import TableBody from "./Body";
import TableFooter from "./Footer";
import Typography from "@material-ui/core/Typography";
import Paper from '@material-ui/core/Paper';
import ContextMenu from "./ContextMenu";
import Box from "@material-ui/core/Box";

function Table(props) {
    const {rows, columns = [], settings, title, getPageRows, actions, contextMenuActions, rowsCount, getId} = props;
    const rowsByPage = getPageRows(rows);

    return (
        <div className={'table-container'}>
            {title && <div className={'table-title'}><Typography variant={'h4'}>{title}</Typography></div>}
            {rows.length <= 0 ?
                <Typography variant={'body1'} color={'textSecondary'}>{settings.notFound}</Typography>:
                <>
                    <Paper className={'table-wrapper'}>
                        {rowsByPage.length > 0 ?
                            <table>
                                <TableHead
                                    getId={getId}
                                    rowsByPage={rowsByPage}
                                    columns={columns}
                                    sortData={props['sortData']}
                                    useSelect={settings.useSelect}
                                    useNumbering={settings.useNumbering}
                                    useContextMenu={settings.useContextMenu}
                                />
                                <TableBody
                                    getId={getId}
                                    columns={columns}
                                    rowsByPage={rowsByPage}
                                    getCellValue={props['getCellValue']}
                                    onRowDetail={props['onRowDetail']}
                                    useContextMenu={settings.useContextMenu}
                                    useRowContextMenu={settings.useRowContextMenu}
                                    useRowHover={settings.useRowHover}
                                    useSelect={settings.useSelect}
                                    useNumbering={settings.useNumbering}
                                    useDetail={settings.useDetail}
                                    summary={props.summary}
                                />
                            </table>:
                            <Box
                                display={'flex'}
                                alignItems={'center'}
                                justifyContent={'center'}
                                style={{minHeight: 100}}
                            >
                                <Typography>Нет данных</Typography>
                            </Box>
                        }
                    </Paper>
                    {settings.useFooter && (
                        <TableFooter
                            rowsByPageLength={rowsByPage.length}
                            actions={actions}
                            doAction={props['actionHandler']}
                            setPage={props['setPage']}
                            usePagination={settings.usePagination}
                            perPageOptions={settings.perPageOptions}
                            count={rowsCount}
                            getAll={props['getAll']}
                        />
                    )}
                    {settings.useContextMenu && <ContextMenu onAction={props['contextMenuActionHandler']} actions={contextMenuActions}/>}
                </>
            }
        </div>
    )
}
export default Table;
