import React, {useMemo, useContext} from 'react';
import cn from "classnames";
import langPhrases from "core/languages";
import {TableContext} from "root/contexts";

// Components
import Button from "core/desktop/view/Button";
import SimpleSelect from "core/desktop/view/form/fields/select/SimpleSelect";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Tooltip from "core/desktop/view/Tooltip";
import Icon from "core/desktop/view/Icon";

// Api function
import {getPageItems} from "./TableWrapper";

export default function Footer(props) {
    const {
        count,
        doAction,
        rowsByPageLength,
        usePagination,
        perPageOptions,
        actions = [],
        setPage,
        getAll
    } = props;

    const table = useContext(TableContext);
    const {selected, currentPage, perPage, actionId} = table.getState();

    const pagesCount = Math.ceil(count / perPage);
    const lastPageItemIndex = currentPage * perPage;
    const to = count < lastPageItemIndex ? count : lastPageItemIndex;
    const from = lastPageItemIndex - perPage + 1;
    const useActions = selected.length > 0 && actions.length > 0;

    const pageItems = useMemo(() => getPageItems(perPageOptions), [perPageOptions.length]);

    return (
        <div className={'table-footer'}>
            <div className={cn('d-flex table-footer-primary-panel', selected.length > 0 ? 'justify-content-between'  : 'justify-content-end')}>
                {selected.length > 0 && (
                    <Box mr={4}>
                        <Typography variant={'body2'}>Отмечено: {selected.length}/{rowsByPageLength}</Typography>
                    </Box>
                )}
                {usePagination && (
                    <div className={'pagination d-flex align-items-center'}>
                        <div className={'page-switcher d-flex align-items-center'}>
                            <Typography variant={'body2'}>{langPhrases['pagePosts']}:</Typography>
                            <SimpleSelect
                                value={perPage}
                                items={pageItems}
                                settings={{
                                    useUnderline: true,
                                    asPlainText: true
                                }}
                                getText={ item => item.TITLE }
                                onChange={ ID => {
                                    const item = pageItems.find( i => i.ID === ID );
                                    const nextPerPage = item.TITLE;

                                    if (nextPerPage * currentPage > count || typeof nextPerPage === 'string') {
                                        table.setPage(1);
                                    }

                                    nextPerPage === langPhrases['all'] ?
                                        getAll().then( () => table.setPerPage(nextPerPage) ):
                                        table.setPerPage(nextPerPage);
                                }}
                            />
                        </div>
                        {typeof perPage !== "string" && (
                            <div className={'info'}>
                                <Typography variant={'body2'}>{from}-{to} из {count}</Typography>
                            </div>
                        )}
                        <div className={'page-links d-flex'}>
                            <div  className={'page-link page-prev'}>
                                <Tooltip title={langPhrases['prev']}>
                                    <Button
                                        size={'small'}
                                        color={'primary'}
                                        disabled={currentPage === 1 || perPage === langPhrases['all']}
                                        onClick={() => {
                                            if (currentPage > 1) {
                                                setPage(currentPage - 1);
                                            }
                                        }}
                                    ><Icon>navigate_before</Icon></Button>
                                </Tooltip>
                            </div>
                            <div className={'page-link page-next'}>
                                <Tooltip title={langPhrases['next']}>
                                    <Button
                                        size={'small'}
                                        color={'primary'}
                                        disabled={currentPage >= pagesCount || perPage === langPhrases['all']}
                                        onClick={() => {
                                            if (currentPage < pagesCount) {
                                                setPage(currentPage + 1);
                                            }
                                        }}
                                    ><Icon>navigate_next</Icon></Button>
                                </Tooltip>
                            </div>
                        </div>
                    </div>
                )}
            </div>
            {useActions && (
                <Box
                    display={'flex'}
                    alignItems={'center'}
                    className={'table-footer-secondary-panel'}
                >
                    <Box width={165}>
                        <SimpleSelect
                            value={actionId}
                            items={actions}
                            settings={{
                                placeholder: "Выберите действие",
                            }}
                            getText={action => action.title}
                            onChange={ id => {
                                table.setActionId(id);
                            }}
                        />
                    </Box>
                    {actionId && (
                        <Box mx={1}>
                            <Button
                                color={'secondary'}
                                size={'small'}
                                onClick={() => {
                                    const action = actions.find( action => actionId === action.id );
                                    doAction(action);
                                }}
                            >Применить</Button>
                        </Box>
                    )}
                </Box>
            )}
        </div>
    )
}
