import React from 'react';

// Components
import TableBody from "@material-ui/core/TableBody";
import TableRow from "./TableRow";
import TableSummary from "./TableSummary";

export default function Body(props) {
    const {rowsByPage = [], getId, summary, useRowContextMenu } = props;


    return (
        <TableBody>
            {rowsByPage.map( (row, index) => {
                const ID = getId(row);
                const showContextMenu = typeof useRowContextMenu === 'function' ? useRowContextMenu(row) : true;

                return (
                    <TableRow
                        key={ID}
                        index={index}
                        item={row}
                        ID={ID}
                        columns={props.columns}
                        getCellValue={props['getCellValue']}
                        onRowDetail={props['onRowDetail']}
                        useContextMenu={props.useContextMenu}
                        useRowContextMenu={showContextMenu}
                        useRowHover={props.useRowHover}
                        useSelect={props.useSelect}
                        useNumbering={props.useNumbering}
                        useDetail={props.useDetail}
                    />
                )
            })}
            {typeof summary === 'function' && (
                <TableSummary
                    items={rowsByPage}
                    columns={props.columns}
                    onSummary={summary}
                    useSelect={props.useSelect}
                    useNumbering={props.useNumbering}
                    useContextMenu={props.useContextMenu}
                />
            )}
        </TableBody>
    )
}