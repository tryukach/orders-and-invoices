import React, {useEffect, useContext} from 'react';
import {TableContext} from "root/contexts";
import langPhrases from "core/languages";

// Components
import TableWrapper from "./TableWrapper";
import Table from "./Table";
import SimpleLoader from "core/desktop/view/SimpleLoader";
import LockLoader from "core/desktop/view/LockLoader";
import Box from "@material-ui/core/Box";

// Api functions
import {findColumnById} from "./TableWrapper";

function EntityDynamicTable(props) {
    const {
        columns,
        settings,
        loadPackage,
        loadAll,
        getCellValue,
        onRowDetail,
        actions = [],
        contextMenuActions = [],
        children
    } = props;

    const table = useContext(TableContext);
    const state = table.getState();

    const isShowAll = state.perPage === langPhrases['all'];

    useEffect(() => {
        const column = findColumnById(columns, state.orderBy);
        const batch = isShowAll ? -1 : getBatch(state.perPage, state.currentPage) - 1;

        loadPackage({[column.code]: state.order}, batch)
            .then( response => {
                table.setRows(response.data());
                table.rowsCount = response.total();
                table.render();
            } );
    }, []);

    const rows = table.getRows();
    if (!rows) {
        return <SimpleLoader>{langPhrases['tableLoading']}</SimpleLoader>
    }

    return (
        <Box
            display={'flex'}
            width={settings.width || '100%'}
            style={{position: 'relative'}}
        >
            {children && children(table)}
            {state.status !== "ready" && <LockLoader/>}
            <Table
                rows={rows}
                getId={r => r.ID || r.id}
                title={props.title}
                settings={{
                    ...settings,
                    useContextMenu: settings.useContextMenu && contextMenuActions.length > 0,
                    useFooter: actions.length > 0 || settings.usePagination,
                    useDetail: settings.useDetail && typeof onRowDetail === 'function'
                }}
                columns={columns}
                getCellValue={getCellValue}
                getPageRows={ items => splitByPage(items, state, settings) }
                sortData={ (order, column) => {
                    const batch = isShowAll ? -1 : getBatch(state.perPage, state.currentPage) - 1;

                    return loadPackage({[column.code]: order}, batch)
                        .then( response => {
                            table.setRows(response.data());
                            table.rowsCount = response.total();
                            table.clearTable();
                            table.render();
                        });
                }}
                onRowDetail={onRowDetail}
                actions={actions}
                actionHandler={ action => {
                    const column = findColumnById(columns, state.orderBy);
                    const batch = isShowAll ? -1 : getBatch(state.perPage, state.currentPage) - 1;

                    table.setStatus('loading');
                    return action.handler(state.selected)
                        .then( () => loadPackage({[column.code]: state.order}, batch))
                        .then( response => {
                            table.setRows(response.data());
                            table.rowsCount = response.total();
                            table.clearTable();
                            table.render();
                            return Promise.resolve();
                        });
                }}
                contextMenuActions={contextMenuActions}
                contextMenuActionHandler={(action, index) => {
                    table.setStatus('loading');
                    return action.handler(table.getRow(index))
                        .then( payload => {

                            switch (action['type']) {
                                case "delete": table.deleteRow(index); break;
                                case "update": table.updateRow(index, payload); break;
                            }

                            table.clearTable();
                            table.render();
                            return Promise.resolve();
                        });
                }}
                setPage={ page => {
                    const {currentPage, perPage, orderBy, order} = table.getState();

                    const currentBatch = getBatch(perPage, currentPage);
                    const nextBatch = getBatch(perPage, page);

                    if (nextBatch !== currentBatch) {
                        const column = findColumnById(columns, orderBy);

                        table.setStatus('loading');
                        loadPackage({[column.code]: order}, nextBatch - 1)
                            .then( response => {
                                table.setRows(response.data());
                                table.rowsCount = response.total();
                                table.setStatus('ready');
                                table.setPage(page);
                                table.clearTable();
                                table.render();
                            });
                    }
                    else {
                        table.setPage(page);
                    }
                }}
                getAll={() => {
                    table.setStatus('loading');
                    return loadAll()
                        .then( response => {
                            table.setRows(response.data());
                            table.rowsCount = response.total();
                            table.setStatus('ready');
                            return Promise.resolve();
                        })
                }}
                rowsCount={table.rowsCount || rows.length}
            />
        </Box>
    )
}
export default TableWrapper(EntityDynamicTable);

// helpers
function splitByPage(items, state, settings) {
    const pagesByBatch = Math.ceil(50 / state.perPage);
    const currentBatch = getBatch(state.perPage, state.currentPage);

    const page = state.currentPage - (currentBatch - 1) * (50 % state.perPage === 0 ? pagesByBatch : pagesByBatch - 1);

    return settings.usePagination && typeof state.perPage !== "string" ?
        items.slice( (page - 1) * state.perPage, page * state.perPage):
        items;
}

function getBatch(perPage, currentPage) {
    return typeof perPage !== "string" ?
        Math.ceil(currentPage * perPage / 50) : 1;
}