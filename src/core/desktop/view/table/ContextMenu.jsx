import React, {useContext} from 'react';
import {TableContext} from "root/contexts";

// Components
import Popper from 'core/desktop/view/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';

export default function ContextMenu(props) {
    const {onAction, actions = []} = props;
    const table = useContext(TableContext);
    const {open, anchorEl, index} = table.getContextMenuOptions();
    return (
        <Popper
            open={open}
            anchorEl={anchorEl}
            onClose={() => onClose(table)}
        >
            {open && actions.map( action => {
                let isShow = true;
                const item = table.getRow(index);

                if (typeof action['isHidden'] === "function") {
                    isShow = !action['isHidden'](item);
                }
                else if (typeof action['isShow'] === "function") {
                    isShow = action['isShow'](item);
                }

                return isShow && (
                    <MenuItem
                        key={action.id}
                        onClick={() => {
                            onClose(table);
                            return onAction(action, index);
                        }}
                    >
                        <ListItemText
                            primary={action.title}
                            primaryTypographyProps={{
                                variant: "caption"
                            }}
                        />
                    </MenuItem>
                )
            })}
        </Popper>
    )
}

function onClose(table) {
    table.setContextMenuOptions({open: false, anchorEl: null});
}