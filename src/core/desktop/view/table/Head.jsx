import React, {useContext} from 'react';
import {TableContext} from "root/contexts";

// Api functions
import {filterColumns, getCellHeadText} from "./TableWrapper";

// Components
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Tooltip from "@material-ui/core/Tooltip";
import SortIcon from "@material-ui/core/TableSortLabel";
import Checkbox from '@material-ui/core/Checkbox';

export default function Head(props) {

    const {sortData, rowsByPage = [], useSelect, useNumbering, useContextMenu, columns, getId} = props;
    const table = useContext(TableContext);
    const state = table.getState();

    const isAllSelected = state.selected.length === rowsByPage.length;
    const orderTooltip =  state.order === 'desc' ?
        'Сортировать по возрастанию':
        'Сортировать по убыванию';

    return (
        <thead>
        <TableRow>
            {useSelect && (
                <TableCell variant={'head'}>
                    <Checkbox
                        checked={isAllSelected}
                        onChange={() => {
                            table.setSelected(isAllSelected ? [] : rowsByPage.map( r => getId(r) ));
                        }}
                    />
                </TableCell>
            )}
            {useNumbering && <TableCell variant={'head'}>№ п/п</TableCell>}
            {filterColumns(columns).map( column =>
                <TableCell
                    key={column.id}
                    variant={'head'}
                    sortDirection={state.orderBy === column.id ? state.order : false}
                    style={{
                        minWidth: column.width || 'auto',
                        width: column.width || 'auto',
                    }}
                >
                    {column['sortable'] !== false ?
                        <Tooltip title={orderTooltip}>
                            <SortIcon
                                active={state.orderBy === column.id}
                                direction={state.order}
                                hideSortIcon={true}
                                onClick={() => {
                                    const newOrder = state.order === "desc" ? "asc" : "desc";
                                    sortData(newOrder, column).then( () => {
                                        if (column.id !== state.orderBy) {
                                            table.setOrder(newOrder);
                                            table.setOrderBy(column.id);
                                        }
                                        else {
                                            table.setOrder(newOrder);
                                        }
                                        table.render();
                                    });
                                }}
                            >
                                {getCellHeadText(column)}
                            </SortIcon>
                        </Tooltip> :
                        getCellHeadText(column)
                    }

                </TableCell>
            )}
            {useContextMenu && <TableCell variant={'head'}/>}
        </TableRow>
        </thead>
    )
}