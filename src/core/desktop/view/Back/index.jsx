import React from 'react';

// Components
import Tooltip from "../Tooltip";
import Typography from "@material-ui/core/Typography";
import Icon from "../Icon";

// Actions
import {setPage} from "root/actions/app";

// Styles
import "./styles.less";

function Back(props) {
    const {tooltip = 'Вернуться назад', children, to = 1, variant = 'h3'} = props;
    return  (
        <Typography variant={variant} className={'back-page'}>
            <Tooltip title={tooltip}>
                <Icon
                    fontSize={'1.2em'}
                    className={'back with-hover'}
                    onClick={() => setPage(to)}
                >arrow_right_alt</Icon>
            </Tooltip>
            {children}
        </Typography>
    );
}
export default Back;