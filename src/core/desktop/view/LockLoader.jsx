import React from 'react';
import Loader from "./Loader";

export default function LockLoader(props) {
    const {size = 'small'} = props;
    return (
        <Loader
            size={size}
            position={'absolute'}
            background={"rgba(255,255,255,0.3)"}
            textStyle={{}}
        >{props.children}</Loader>
    )
}