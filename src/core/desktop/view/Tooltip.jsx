import React from 'react';
import Tooltip from "@material-ui/core/Tooltip";

export default function TooltipWrapper(props) {
    return <Tooltip {...props}><span>{props.children}</span></Tooltip>;
}