import React from 'react';
import Loader from "./Loader";

export default function CenteredLoader(props) {
    return (
        <Loader
            position={'absolute'}
            vetical={'center'}
            horizontal={'center'}
        >{props.children}</Loader>
    )
}