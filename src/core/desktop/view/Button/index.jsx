import React, {useState} from 'react';
import cn from "classnames";
import "./styles.less";

// Components
import BaseButton from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import Box from "@material-ui/core/Box";
import CircularProgress from "@material-ui/core/CircularProgress";

export default function Button(props) {
    const { isLoading, disabled, onClick, variant = 'contained', className = '', color = 'primary', size, template, ...baseProps } = props;
    const ButtonTemplate = getButtonComponent(template);

    return (
        <ButtonTemplate
            variant={variant}
            size={size}
            onClick={onClick}
            className={cn('btn', `btn-${variant}`, className)}
            disabled={isLoading || disabled}
            color={color}
            {...baseProps}
        >
            <Box
                display={'flex'}
                alignItems={'center'}
            >
                {isLoading && <CircularProgress size={24} className={'progress'} />}
                {props.children}
            </Box>
        </ButtonTemplate>
    )
}

export function LoadButton(props) {
    const {onClick, ...buttonProps} = props;
    const [isLoading, setLoading] = useState(false);

    return (
        <Button
            {...buttonProps}
            isLoading={isLoading}
            onClick={() => {
                setLoading(true);
                onClick().finally(() => {
                    setLoading(false);
                })
            }}
        />
    )
}

function getButtonComponent(template) {
    switch (template) {
        case "fab": return Fab;
        default: return BaseButton;
    }
}