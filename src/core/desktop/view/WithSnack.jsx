import React, {useMemo} from "react";
import { SnackbarProvider, withSnackbar } from 'notistack';
import { SnackContext } from "root/contexts";

// Components
import IconButton from '@material-ui/core/IconButton'

function WithSnack(props) {
    const SnackedWrapper = useMemo(() => withSnackbar(Wrapper), []);
    return (
        <SnackbarProvider
            autoHideDuration={props.autoHideDuration || 2500}
            anchorOrigin={props.anchorOrigin || {
                vertical: "top",
                horizontal: "right"
            }}
            maxSnack={props.maxSnack || 3}
        >
            <SnackedWrapper>{props.children}</SnackedWrapper>
        </SnackbarProvider>
    );
}
export default WithSnack;

function Wrapper(props) {
    const snackApi = {
        showMessage: (message = 'some message', options = {}) => {
            props.enqueueSnackbar(message, Object.assign({ action: key => closeAction(key, props.closeSnackbar) }, options))
        },
        closeMessage: props.closeSnackbar
    };
    return <SnackContext.Provider value={snackApi}>{props.children}</SnackContext.Provider>;
}

export function closeAction(key, close) {
    return (
        <IconButton onClick={() => close(key)}>
            <i className={'material-icons'}>close</i>
        </IconButton>
    )
}