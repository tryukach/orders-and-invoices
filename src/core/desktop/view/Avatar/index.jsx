import React from 'react';

// Components
import Tooltip from "@material-ui/core/Tooltip";
import BaseAvatar from "@material-ui/core/Avatar";

// Styles
import "./styles.less";

// Pictures
import defaultAvatar from "./user_avatar.png";

export default function AvatarContainer(props) {
    const {src, title, link, children} = props;
    const pictureSrc = src || defaultAvatar;

    return (
        <Tooltip title={title}>
            <div className={'avatar'}>
                {link ?
                    <a href={link} target={'_blank'}>
                        <Avatar src={pictureSrc} title={title}>{children}</Avatar>
                    </a>:
                    <Avatar src={pictureSrc} title={title}>{children}</Avatar>
                }
            </div>
        </Tooltip>
    )
}

function Avatar(props) {
    const {children, title, src} = props;
    return children ?
        <BaseAvatar alt={title}>{children}</BaseAvatar>:
        <BaseAvatar src={src} alt={title}/>
}