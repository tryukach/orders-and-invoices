import React from 'react';
import Loader from "./Loader";

export default function PageLoader(props) {
    return (
        <Loader
            size={'medium'}
            vetical={'top'}
            horizontal={'left'}
        >{props.children}</Loader>
    )
}