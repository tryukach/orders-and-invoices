function getPhrases(language) {
    switch (language) {
        case "en": return require('./en').default;
        default: return require('./ru').default;
    }
}
const curLang = BX24.getLang();
export default getPhrases(curLang);