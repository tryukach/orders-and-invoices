import {Map} from "immutable";
import App from "core/classes/App";

class State {

    constructor(id, initialState, updateDom) {
        const cache = State.getCacheState(id);
        this.id = id;

        this.initialState = Object.assign({}, initialState, cache);
        this.state = Map(this.initialState);
        this.updateDom = updateDom;
    }

    render() {
        this.updateDom();
    }

    getState(toJs = true) {
        return toJs ?
            this.state.toJS():
            this.state;
    }

    getInitialState() {
        return this.initialState;
    }

    set(keyPath, value) {
        this.state = Array.isArray(keyPath) ?
            this.state.setIn(keyPath, value):
            this.state.set(keyPath, value);
    }

    update(keyPath, value) {
        this.set(keyPath, value);
        this.render();
    }

    get(keyPath) {
        return Array.isArray(keyPath) ?
            this.state.getIn(keyPath):
            this.state.get(keyPath);
    }

    saveState() {
        localStorage.setItem(this.getId(), JSON.stringify(this.getState()));
    }

    static getCacheState(id) {
        const storageKey = App.generateStorageKey(id);
        const cacheState = localStorage.getItem(storageKey);
        return cacheState === null ? undefined : JSON.parse(cacheState);
    }

    removeCache() {
        localStorage.removeItem(this.getId());
    }

    getId() {
        return App.generateStorageKey(this.id);
    }
}
export default State;