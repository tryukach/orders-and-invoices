import State from "./State";
import {Map} from "immutable";

class SelectState extends State {

    constructor(options, updateDom) {
        const {id, ...initialState} = options;
        super(id, initialState, updateDom);

        this.searchItems = null;
        this.items = Map();
    }

    open() {
        if (this.get('open') !== true) {
            this.update('open', true);
        }
    }

    hide() {
        if (this.get('open') !== false) {
            this.update('open', false);
        }
    }

    setSearchItems(items) {
        this.searchItems = items;
    }

    getSearchItems() {
        return this.searchItems;
    }

    setItems(items) {
        this.items = Map(items);
    }

    getItems() {
        return this.items.toJS();
    }

    getItemId(item) {
        return item.ID || item.id;
    }

    reset() {
        this.set('searchPhrase', '');
        this.setBatch(0);
        this.searchItems = null;
        this.render();
    }

    setSearchPhrase(phrase) {
        this.update('searchPhrase', phrase);
    }

    setBatch(batch) {
        this.batch = batch;
    }

    getBatch() {
        return this.batch || 0;
    }

    setStatus(status) {
        this.update('status', status);
    }

    addItems(newItems) {
        const items = this.getItems();

        for (let item of newItems) {
            const ID = this.getItemId(item);
            if (!items.byId[ID]) {
                items.byId[ID] = item;
                items.allIds.push(ID);
            }
        }
        this.setItems(items);
    }

    addItem(item) {
        this.addItems([item]);
    }

    setSelected(selected = []) {
        this.selected = selected;
    }

    getSelected() {
        return this.selected || [];
    }
}
export default SelectState;