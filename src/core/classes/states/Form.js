import State from "./State";
import {Map} from "immutable";

class Form extends State {

    constructor(id, updateDom) {
        const initialState = {
            status: "ready",
            errors: {},
            fields: {},
            meta: {}
        };
        super(id, initialState, updateDom);
        this.initialFields = {};
    }

    submit(handler, onSuccess, onError, resetAfterSubmit = true) {
        const [errors, isValid] = this.validate();

        if (isValid) {
            this.setStatus('submitting');
            this.clearErrors();

            return handler(this.getFields())
                .then( response => {
                    this.setFormMeta('submitted', true);

                    resetAfterSubmit ?
                        this.reset():
                        this.setStatus('ready');

                    if (typeof onSuccess === "function") {
                        onSuccess(response);
                    }
                    return Promise.resolve();
                })
                .catch( err => {
                    if (typeof onError === 'function') {
                        onError(err);
                    }
                });
        }
        else {
            this.setErrors(errors);
        }
    }

    clearErrors() {
        this.set('errors', {});
    }

    validate() {
        const errors = {};
        const fields = this.getFields();
        let isValid = true;

        for (let key of Object.keys(fields)) {
            const field = fields[key];
            const {required = false} = field.meta;

            errors[key] = required && this.isFieldEmpty(field.value) ?
                'Необходимо заполнить' : (
                    typeof field.validate === 'function' ?
                        field.validate(field.value) : undefined
                );

            if (errors[key] !== undefined) {
                isValid = false;
            }
        }
        return [errors, isValid];
    }

    isFieldEmpty(value) {
        return value === '' || (Array.isArray(value) && value.length === 0);
    }

    addField(id, fieldData) {
        this.update(['fields', id], fieldData);
        this.initialFields[id] = fieldData;
    }

    setErrors(errors) {
        this.update('errors', errors);
    }

    setError(id, error) {
        this.update(['errors', id], error);
    }

    getFieldError(id) {
        return this.get(['errors', id]);
    }

    setStatus(status) {
        this.update('status', status);
    }

    setFieldMeta(id, value) {
        const meta = this.getFieldMeta(id);
        this.update(['fields', id, 'meta'], Object.assign({}, meta, value));
    }

    getFieldMeta(id) {
        return this.getField(id).meta;
    }

    setFieldValue(id, value) {
        this.update(['fields', id, 'value'], value);
    }

    getFields() {
        return this.get('fields');
    }

    getValues() {
        const fields = this.get('fields');
        const values = {};

        Object.keys(fields).forEach(key => {
            const field = fields[key];
            values[key] = field.value;
        });

        return values;
    }

    getField(id) {
        return this.get(['fields', id]);
    }

    reset(fields) {
        const initialState = this.getInitialState();
        initialState.fields = this.initialFields;

        if (fields !== undefined) {
            Object.keys(fields).forEach( key => {
                if (initialState.fields.hasOwnProperty(key)) {
                    initialState.fields[key].value = fields[key];
                }
            });
        }

        this.state = Map(initialState);
        this.isReset = !this.isReset;
        this.render();
    }

    setFormMeta(key, value) {
        this.set(['meta', key], value);
    }

    getFormMeta(key) {
        return this.get(['meta', key]);
    }

    isFormSubmitted() {
        return this.get(['meta', 'submitted']) || false;
    }
}
export default Form;