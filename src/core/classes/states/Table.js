import State from "./State";
import {updateObject} from "core/Api";
import {List} from "immutable";

class Table extends State {

    constructor(options, updateDom) {
        const {id, ...initialState} = options;
        super(id, initialState, updateDom);

        this.trackUpdatingFlags = {};
    }

    render() {
        super.render();
        this.saveState();
    }

    setStatus(status) {
        this.update('status', status);
    }

    setRows(rows) {
        this.rows = List(rows);
    }

    getRow(index) {
        return this.rows.get(index);
    }

    deleteRow(index) {
        this.rows = this.rows.delete(index);
    }

    deleteRowById(id) {
        const index = this.rows.findIndex(row => {
            const rowId = row.ID || row.id;
            return rowId === id;
        });
        this.deleteRow(index);
    }

    findRowIndex(id) {
        const rows = this.getRows();
        return rows.findIndex( r => {
            const ID = r.ID || r.id;
            return ID === id;
        });
    }

    updateRow(id, updatedProps) {
        const index = this.findRowIndex(id);
        this.rows = this.rows.update(index, row => updateObject(row, updatedProps));

        this.trackUpdatingFlags[id] = !this.trackUpdatingFlags[id];
        this.render();
    }

    getTrackUpdatingFlags() {
        return this.trackUpdatingFlags || {};
    }

    isRowUpdated(id) {
        return this.getTrackUpdatingFlags()[id];
    }

    getRows() {
        if (this.rows) {
            return this.rows.toJS();
        }
        return [];
    }

    clearTable() {
        this.set('status', 'ready');
        this.set('selected', []);
        this.clearAction();
    }

    clearAction() {
        this.set('actionId', '');
    }

    setSelected(ids = []) {
        if (ids.length === 0) {
            this.clearAction();
        }
        this.update('selected', ids);
    }

    getSelected() {
        return this.get('selected');
    }

    setContextMenuOptions(options) {
        this.update('contextMenuOptions', options);
    }

    getContextMenuOptions() {
        return this.getState().contextMenuOptions;
    }

    setOrder(order) {
        this.set('order', order);
    }

    setOrderBy(orderBy) {
        this.set('orderBy', orderBy);
    }

    setPage(page) {
        this.set('currentPage', page);
    }

    setPerPage(perPage) {
        this.update('perPage', perPage);
    }

    setActionId(actionId) {
        this.update('actionId', actionId);
    }

    reset() {
        this.setPage(1);
        this.clearTable();
    }

    saveState() {
        const currentState = this.getState();
        const saveState = {
            perPage: currentState.perPage,
            order: currentState.order,
            orderBy: currentState.orderBy
        };
        localStorage.setItem(this.getId(), JSON.stringify(saveState));
    }
}
export default Table;