class Data {
    static getFileSrc(file) {
        return [file['name'], file['src'].split("base64,")[1]];
    };

    static getUserFirstDepartment(user) {
        return Array.isArray(user['UF_DEPARTMENT']) ? +user['UF_DEPARTMENT'][0] : user['UF_DEPARTMENT'] ? +user['UF_DEPARTMENT'] : null;
    }

    static getUserDisplayName(user, variant) {
        if (!user) {
            return null;
        }

        switch (variant) {
            case "short": return `${user['LAST_NAME']} ${user['NAME'][0]}.`;
            default: return `${user['LAST_NAME']} ${user['NAME']}`;
        }
    }

    static getItemPropValue(item, prop) {
        return item.PROPERTY_VALUES[prop] || item[prop];

    }

    static groupIdsBy(items = [], byProp, idProp = "ID") {
        const result = {};
        for (let item of items) {
            const propValue = Data.getItemPropValue(item, byProp);
            if (!propValue) continue;

            if (!result.hasOwnProperty(propValue)) {
                result[propValue] = [];
            }
            result[propValue].push(Data.getItemPropValue(item, idProp));
        }
        return result;
    }

    static groupItemsBy(items = [], byProp) {
        const result = {};
        for (let item of items) {
            const propValue = Data.getItemPropValue(item, byProp);
            if (!propValue) continue;

            if (!result.hasOwnProperty(propValue)) {
                result[propValue] = [];
            }
            result[propValue].push(item);
        }
        return result;
    }
}
export default Data;