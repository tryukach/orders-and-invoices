import Bitrix24 from "core/classes/Bitrix24";
import {E_SETTINGS, APP_NAME} from "root/constants";
import {b64_md5 as md5} from "react-native-md5";

class App {
    static getSettings() {
        return Bitrix24.callListMethod('entity.item.get', {ENTITY: E_SETTINGS})
            .then( response => {
                const settings = response.data();

                let normalizedSettings = {};
                for (let setting of settings) {
                    normalizedSettings[setting.CODE] = {
                        ID: setting.ID,
                        ...setting.PROPERTY_VALUES
                    }
                }
                return Promise.resolve(normalizedSettings);
            });
    }

    static generateStorageKey(id) {
        return md5([Bitrix24.getDomain(), APP_NAME, id].join(''));
    }
}
export default App;