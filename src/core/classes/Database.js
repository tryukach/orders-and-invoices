import Bitrix24 from "core/classes/Bitrix24";
import {getDeleteRequests, callStepBatch} from "core/Api";

class Database {

    static load(entity, params, callback) {
        return Bitrix24.callListMethod("entity.item.get", {ENTITY: entity, ...params}, callback)
            .then( response => Promise.resolve(response.data()) );
    }

    static loadPackage(entity, params, packageNumber, callback) {
        return Bitrix24.loadPackage('entity.item.get', {ENTITY: entity, ...params}, packageNumber, callback);
    }

    static addItem(ENTITY, params) {
        return Bitrix24.callMethod("entity.item.add", {ENTITY, ...params})
            .then( response => Promise.resolve(response.data() + '') );
    }

    static deleteItem(ENTITY, ID) {
        return Bitrix24.callMethod('entity.item.delete', {ENTITY, ID})
            .then( response => Promise.resolve(response.data()) );
    }

    static deleteItems(entity, items = []) {
        const requests = getDeleteRequests(entity, items, item => item.ID);
        return callStepBatch(requests, 10);
    }

    static deleteByIds(entity, ids) {
        const requests = getDeleteRequests(entity, ids, item => item);
        return callStepBatch(requests, 10);
    }

    static updateItem(ENTITY, params) {
        return Bitrix24.callMethod('entity.item.update', {ENTITY, ...params})
            .then( response => Promise.resolve(response.data()) );
    }
}
export default Database;