import {
    E_SETTINGS,
    E_INVOICES,
    APP_NAME,
    U_INVOICE_STATUS_ID,
    E_ATTACHMENTS,
    E_PRODUCTS,
    E_ORDERS,
    E_USERS_ROLES,
    E_INVOICE_PAYMENTS,
    CURRENT_VERSION,
    APP_URL
} from "root/constants";
import {
    callStepBatch,
    getEntityInstallRequests,
    getAddPropsRequests,
    getAddItemsRequests, callBatch,
    settledPromise
} from "core/Api";

import Bitrix24 from "core/classes/Bitrix24";

const entities = [
    {ENTITY_CODE: E_SETTINGS, NAME: 'Настройки приложения'},
    {ENTITY_CODE: E_ORDERS, NAME: 'Заявки'},
    {ENTITY_CODE: E_INVOICES, NAME: 'Счета'},
    {ENTITY_CODE: E_ATTACHMENTS, NAME: 'Файлы'},
    {ENTITY_CODE: E_PRODUCTS, NAME: 'Товары'},
    {ENTITY_CODE: E_INVOICE_PAYMENTS, NAME: 'Оплаты счетов'},
    {ENTITY_CODE: E_USERS_ROLES, NAME: 'Роли пользователей'},
];

const entitiesProps = [
    {
        ENTITY_CODE: E_SETTINGS,
        PROPS: {
            VALUE: {NAME: "Значение"},
            SRC: {NAME: "Файл", TYPE: "F"},
            GROUP: {NAME: "Группа"},
        }
    },
    {
        ENTITY_CODE: E_ORDERS,
        PROPS: {
            DEAL_ID: {NAME: "Идентификатор сделки", TYPE: "N"},
            STATUS_ID: {NAME: "Идентификатор статуса", TYPE: "N"},
            CREATOR_ID: {NAME: "Идентификатор создателя", TYPE: "N"},
            RESPONSIBLE_ID: {NAME: "Идентификатор ответственного", TYPE: "N"},
            COMMENT: {NAME: "Комментарий"},
        }
    },
    {
        ENTITY_CODE: E_INVOICES,
        PROPS: {
            INVOICE_TYPE_ID:  {NAME: "Идентификатор типа счета", TYPE: "N"},
            INVOICE_DATE: {NAME: "Дата счета"},
            INVOICE_NUMBER: {NAME: "Номер счета", TYPE: "N"},
            STATUS_ID: {NAME: "Идентификатор статуса", TYPE: "N"},
            DEAL_ID: {NAME: "Идентификатор сделки", TYPE: "N"},
            ORDER_ID: {NAME: "Идентификатор заявки", TYPE: "N"},
            CREATOR_ID: {NAME: "Идентификатор создателя", TYPE: "N"},
            SUM: {NAME: "Сумма счета", TYPE: "N"},
            CURRENT_PAYMENT_SUM: {NAME: "Текущая сумма оплаты", TYPE: "N"},
            PAYMENT_TYPE_ID: {NAME: "Тип оплаты", TYPE: "N"},
            PAYMENT_PURPOSE: {NAME: "Назначение платежа"},
            PAYED_STATUS: {NAME: "Статус оплаты"},
            PAYED_SUM: {NAME: "Оплаченная сумма"},
            E_COMMENT: {NAME: "Комментарий инженера"},
            D_COMMENT: {NAME: "Комментарий руководителя"},
            DECLINE_REASON: {NAME: "Причина отклонения"},
            DECLINE_BY: {NAME: "Идентификатор отклонившего", TYPE: "N"},
            DELIVERY_DATE: {NAME: "Дата поставки"},
            DELIVERY_STATUS_ID: {NAME: "Идентификатор статуса поставки", TYPE: "N"},
            DOCUMENTS_RECEIVE_DATE: {NAME: "Дата получения оригиналов документов"},
            DOCUMENTS_RECEIVE_STATUS_ID: {NAME: "Идентификатор статуса получения оригиналов документов", TYPE: "N"},
            TASK_ID: {NAME: "Идентификатор задачи", TYPE: "N"}
        }
    },
    {
        ENTITY_CODE: E_ATTACHMENTS,
        PROPS: {
            SRC: {NAME: "Ссылка на файл", TYPE: "F"},
            SIZE: {NAME: "Размер файла", TYPE: "N"},
            TYPE: {NAME: "Тип файла"},
            ENTITY_ID: {NAME: "Идентификатор сущности", TYPE: "N"},
        }
    },
    {
        ENTITY_CODE: E_PRODUCTS,
        PROPS: {
            ORDER_ID: {NAME: "Идентификатор заявки", TYPE: "N"},
            GROUP_ID: {NAME: "Идентификатор группы товаров", TYPE: "N"},
            MEASURE_ID: {NAME: "Мера", TYPE: "N"},
            COUNT: {NAME: "Количество", TYPE: "N"},
            COMMENT: {NAME: "Комментарий"}
        }
    },
    {
        ENTITY_CODE: E_INVOICE_PAYMENTS,
        PROPS: {
            INVOICE_ID: {NAME: "Идентификатор счета", TYPE: "N"},
            PP_SUM: {NAME: "Сумма п/п", TYPE: "N"},
            PAYMENT_DATE: {NAME: "Дата оплаты"},
            PAYED: {NAME: "Счет оплачен"},
        }
    },
    {
        ENTITY_CODE: E_USERS_ROLES,
        PROPS: {
            USER_ID: {NAME: "Идентификатор пользователя", TYPE: "N"},
            ROLE_ID: {NAME: "Идентификатор роли", TYPE: "N"}
        }
    },
];

const entitiesItems = [
    {
        ENTITY_CODE: E_SETTINGS,
        ITEMS: [
            {NAME: "Версия приложения", CODE: "version", PROPS: {VALUE: CURRENT_VERSION, GROUP: "app"}}
        ]
    }
];


export default function install(info) {
    localStorage.clear();

    return installEntities(entities)
        .then( () => {
            const requests = getAddPropsRequests(entitiesProps);
            return callStepBatch(requests, 50);
        })
        .then( () => {
            const requests = getAddItemsRequests(entitiesItems);
            return callStepBatch(requests, 50);
        })
        .then( () => bindPlacement())
        .then( () => settledPromise(installUserField))
}

function installEntities(entities) {
    return callBatch(getEntityInstallRequests(entities), "installEntities");
}

function installUserField() {
    return Bitrix24.callMethod(
        'task.item.userfield.add',
        {
            "PARAMS": {
                'USER_TYPE_ID': 'integer',
                'FIELD_NAME': U_INVOICE_STATUS_ID,
                'XML_ID': U_INVOICE_STATUS_ID,
                'EDIT_FORM_LABEL': {
                    'ru': `Ид статуса счета [${APP_NAME}]`
                },
                'LABEL': `Ид статуса счета [${APP_NAME}]`
            }
        }
    );
}

function bindPlacement() {
    return Bitrix24.callMethod('placement.bind', {
        PLACEMENT: "CRM_DEAL_DETAIL_TAB",
        HANDLER: `${APP_URL}/deal_tab.html`,
        TITLE: "Заявки и счета",
        DESCRIPTION: "Заявки и счета"
    })
}
