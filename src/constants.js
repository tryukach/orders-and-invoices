// APP
export const APP_NAME = "Заявки Ремстрой";
export const DATE_TIME_FORMAT = "DD.MM.YYYY HH:mm";
export const DATE_FORMAT = "DD.MM.YYYY";
export const INPUT_DATE_FORMAT = "YYYY-MM-DD";
export const APP_URL = "https://portal.mstb.group/bx24_local_apps/orders_and_invoices/";
export const SUPPORT_URL = "#";
export const CURRENT_VERSION = 2.1;
export const NUMBER_DELIMITER = " ";
export const COIN_DELIMITER = ",";

// ENTITY
export const E_SETTINGS = "S1";
export const E_ORDERS = "O1";
export const E_INVOICES = "I1";
export const E_INVOICE_PAYMENTS = "IP";
export const E_USERS_ROLES = "UR";
export const E_ATTACHMENTS = "A1";
export const E_PRODUCTS = "P1";

// User Fields
export const U_INVOICE_STATUS_ID = "INVOICE_STATUS_ID";
export const U_INVOICE_STATUS_ID_LC = "InvoiceStatusId";
export const OBJECT_RESPONSIBLE_KEY = "UF_CRM_1549543152";