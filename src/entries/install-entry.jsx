import React from 'react';
import ReactDOM from 'react-dom';
import Bitrix24 from "classes/Bitrix24";
import install from "root/install";
import langPhrases from "root/languages/index";
import Theme from "core/desktop/Theme";

// COMPONENTS
import Loader from "core/desktop/view/Loader/index";

BX24.init( () => {
    const size = BX24.getScrollSize();
    const appContainer = document.getElementById('root');
    appContainer.style.minHeight = size['scrollHeight'] + "px";

    ReactDOM.render((
            <Theme>
                <Loader position={"absolute"}>
                    {langPhrases['installApp']}...
                </Loader>
            </Theme>
        ),
        document.getElementById('root')
    );

    Bitrix24.getInfo()
        .then( info => {
            info['INSTALLED'] ?
                BX24.installFinish():
                install(info).then(BX24.installFinish)
        })
});