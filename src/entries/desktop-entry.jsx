import React from 'react';
import { render } from 'react-dom';
import langPhrases from "core/languages";
import {loadState} from "root/loadStorage";

// STYLES
import 'assets/css/reset.css';
import 'assets/css/common.less';
import 'assets/css/desktop.less';

import configureStore from "root/store/configureStore";
import Root from "components/desktop/containers/Root";

// Components
import Theme from "core/desktop/Theme";
import Loader from "core/desktop/view/Loader";
import Body from "components/desktop/Body";
import AppContainer from "components/desktop/containers/AppContainer";

BX24.init(() => {
    const size = BX24.getScrollSize();
    const appContainer = document.getElementById('root');
    const initHeight = size['scrollHeight'] + "px";

    appContainer.style.minHeight = initHeight;

    render(<Theme><Loader position={"absolute"}>{langPhrases['prepareData']}...</Loader></Theme>, appContainer);
    loadState().then( state => {
        const store = configureStore(state);
        render((
            <Root
                store={store}
                useTools={false}
                initHeight={initHeight}
                renderContainer={() => (
                    <AppContainer
                        renderBody={ props => <Body {...props}/> }
                    />
                )}
            />
        ), document.getElementById('root'));
    });
});

