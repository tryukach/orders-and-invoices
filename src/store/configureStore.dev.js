import { createStore, applyMiddleware, compose } from 'redux';
import DevTools from 'core/DevTools';
import thunkMiddleware from "redux-thunk";
import actions, {assignAll} from 'root/actions';
import reducer from 'root/reducers';
import {logState} from "core/Api";
import {saveState} from "root/loadStorage";
import throttle from "lodash/throttle";

const enhancer = compose(
    applyMiddleware(thunkMiddleware),
    DevTools.instrument(),
);


export default function configureStore(initialState) {
    logState(initialState);

    const store = createStore(reducer, initialState, enhancer);
    assignAll(actions, store);

    store.subscribe(throttle(() => {
        saveState(store.getState());
    }, 1000));
    return store;
}