import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from "redux-thunk";
import actions, {assignAll} from 'root/actions';
import reducer from 'root/reducers';
import {logState} from "core/Api";
import {saveState} from "root/loadStorage";
import throttle from "lodash/throttle";

const enhancer = applyMiddleware(thunkMiddleware);

export default function configureStore(initialState) {
    logState(initialState);

    const store = createStore(reducer, initialState, enhancer);
    assignAll(actions, store);

    store.subscribe(throttle(() => {
        saveState(store.getState());
    }, 1000));

    return store;
}