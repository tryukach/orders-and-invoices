const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require('webpack');

const buildFolder = `${__dirname}/build`;
const srcFolder = `${__dirname}/src`;

module.exports = env => {

    const common = {
        mode: env,
        context: srcFolder,
        entry: {
            'desktop' : './entries/desktop-entry',
            'install' : './entries/install-entry',
            'mobile': './entries/mobile-entry'
        },
        output: {
            path: buildFolder,
            filename: 'js/[name].js',
            sourceMapFilename: "sourceMaps/[name].map"
        },
        resolve: {
            alias: {
                "reducers": `${srcFolder}/reducers`,
                "actions": `${srcFolder}/actions`,
                "components": `${srcFolder}/components`,
                "images": `${srcFolder}/assets/images`,
                "assets": `${srcFolder}/assets`,
                "core": `${srcFolder}/core`,
                "classes": `${srcFolder}/classes`,
                "root": srcFolder
            },
            extensions: ['.jsx', '.js']
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/, // определяем тип файлов
                    exclude: /(node_modules)/,  // исключаем из обработки папку node_modules
                    loader: "babel-loader",   // определяем загрузчик
                    options:{
                        presets:["@babel/env", "@babel/react"],   // используемые плагины,
                        plugins: [
                            "syntax-object-rest-spread",
                            "transform-class-properties"
                        ]
                    },

                },
                {
                    test: /\.less$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        "css-loader",
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: [require('autoprefixer')]
                            }
                        },
                        "less-loader"
                    ]
                },
                {
                    test: /\.css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        "css-loader"
                    ]
                },
                {
                    test: /fonts.*\.(eot|woff|woff2|ttf|svg)$/,
                    exclude: /(node_modules)/,
                    loader: "file-loader",
                    options: {
                        name: '[name]/[name].[ext]',
                        outputPath: '../fonts'
                    }
                },
                {
                    test: /(images|components|core).*\.(jpg|jpeg|png|svg)$/,
                    exclude: /(node_modules)/,
                    loader: "file-loader",
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'images'
                    }
                }
            ]
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: 'css/[name].css',
            }),
            new webpack.DefinePlugin({
                'process.env.NODE_ENV': JSON.stringify(env),
            })
        ],
        optimization: {
            splitChunks: {
                chunks: 'all',
                name: "common"
            },
        }
    };

    let configByMode;
    if (env === "development") {
        configByMode = {
            watch: true,
            watchOptions: {
                aggregateTimeout: 100
            }
        };
    }
    return Object.assign({}, common, configByMode);
};
